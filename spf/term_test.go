package spf

import (
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseDirectiveAll(t *testing.T) {
	tests := []struct {
		term string
		mech mechanism
	}{
		// all
		{"all", mkMech(mechAll, qualPass, "")},
		{"+all", mkMech(mechAll, qualPass, "")},
		{"-all", mkMech(mechAll, qualFail, "")},
		{"~all", mkMech(mechAll, qualSoftFail, "")},
		{"?all", mkMech(mechAll, qualNeutral, "")},
		{"?all", mkMech(mechAll, qualNeutral, "")},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectiveInclude(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// include
		{"include:_spf.whom.email", mkMech(mechInclude, qualPass, "_spf.whom.email")},
		{"include:_spf.%%.whom.email", mkMech(mechInclude, qualPass, "_spf.%.whom.email")},
		{"include:_spf.%{i}.whom.email", mkMech(mechInclude, qualPass, "_spf.10.2.101.22.whom.email")},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectiveA(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// a
		{"a", mkMech(mechA, qualPass, "")},
		{"a:_spf.whom.email", mkMech(mechA, qualPass, "_spf.whom.email")},
		{"a:_spf.whom.email/16", mechanism{
			qualPass,
			mechA,
			"_spf.whom.email",
			true,
			16,
			128,
			nil,
			nil},
		},
		{"a:_spf.whom.email//112", mechanism{
			qualPass,
			mechA,
			"_spf.whom.email",
			true,
			32,
			112,
			nil,
			nil},
		},
		{"a:_spf.whom.email/10//114", mechanism{
			qualPass,
			mechA,
			"_spf.whom.email",
			true,
			10,
			114,
			nil,
			nil},
		},

		// TODO: macros? ehh.
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectiveMX(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// mx
		{"mx", mkMech(mechMX, qualPass, "")},
		{"mx:_spf.whom.email", mkMech(mechMX, qualPass, "_spf.whom.email")},
		{"mx:_spf.whom.email/14", mechanism{
			qualPass,
			mechMX,
			"_spf.whom.email",
			true,
			14,
			128,
			nil,
			nil},
		},
		{"mx:_spf.whom.email//108", mechanism{
			qualPass,
			mechMX,
			"_spf.whom.email",
			true,
			32,
			108,
			nil,
			nil},
		},
		{"mx:_spf.whom.email/10//126", mechanism{
			qualPass,
			mechMX,
			"_spf.whom.email",
			true,
			10,
			126,
			nil,
			nil},
		},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectivePTR(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// ptr (opt ds)
		{"ptr", mkMech(mechPTR, qualPass, "")},
		{"ptr:1.0.0.127._spf.whom.email", mkMech(mechPTR, qualPass, "1.0.0.127._spf.whom.email")},
		{"ptr:%{i3r}._spf.whom.email", mkMech(mechPTR, qualPass, "101.2.10._spf.whom.email")},
		{"ptr:%{i3r}.%{v}._spf.whom.email", mkMech(mechPTR, qualPass, "101.2.10.in-addr._spf.whom.email")},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t,
			mechEqual(test.mech, mech),
			"test %d: mech not equal, want %v\ngot %v",
			i, test.mech, mech)
	}
}

func TestParseDirectiveIP4(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// ip4 (req net, opt CIDR)
		{"ip4:127.0.0.1", mechanism{
			qualPass,
			mechIP4,
			"",
			false,
			32,
			128,
			net.ParseIP("127.0.0.1"),
			nil},
		},
		{"ip4:10.2.165.187/18", mechanism{
			qualPass,
			mechIP4,
			"",
			true,
			18,
			128,
			net.ParseIP("10.2.165.187"),
			nil},
		},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectiveIP6(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	tests := []struct {
		term string
		mech mechanism
	}{
		// TODO:
		// ip6 (req net, opt CIDR)
		{"ip6:::", mechanism{
			qualPass,
			mechIP6,
			"",
			false,
			32,
			128,
			nil,
			net.ParseIP("::")},
		},
		{"ip6:2001:db8::cb01", mechanism{
			qualPass,
			mechIP6,
			"",
			false,
			32,
			128,
			nil,
			net.ParseIP("2001:db8::cb01")},
		},
		{"ip6:2001:db8::cb01/106", mechanism{
			qualPass,
			mechIP6,
			"",
			true,
			32,
			106,
			nil,
			net.ParseIP("2001:db8::cb01")},
		},
		{"ip6:FE80:0000:0000:0000:0202:B3FF:FE1E:8329", mechanism{
			qualPass,
			mechIP6,
			"",
			false,
			32,
			128,
			nil,
			net.ParseIP("FE80:0000:0000:0000:0202:B3FF:FE1E:8329")},
		},
		{"ip6:FE80::0202:B3FF:FE1E:8329", mechanism{
			qualPass,
			mechIP6,
			"",
			false,
			32,
			128,
			nil,
			net.ParseIP("FE80::0202:B3FF:FE1E:8329")},
		},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
	}
}

func TestParseDirectiveExists(t *testing.T) {
	addr := net.IPAddr{IP: net.ParseIP("10.2.101.22")}
	domain := "whom.target"
	sender := "alex@whom.ninja"
	tests := []struct {
		term string
		mech mechanism
	}{
		// exists (ds)
		{"exists:whom.email", mkMech(mechExists, qualPass, "whom.email")},
		{"exists:_spf.whom.email", mkMech(mechExists, qualPass, "_spf.whom.email")},
		{"-exists:whom.email", mkMech(mechExists, qualFail, "whom.email")},
		{"exists:%{d}", mkMech(mechExists, qualPass, domain)},
		{"exists:%{o}", mkMech(mechExists, qualPass, "whom.ninja")},
		{"exists:%{l}.whom.email", mkMech(mechExists, qualPass, "alex.whom.email")},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{ip: addr, domain: domain, sender: sender}
		mech, match, err := parseDirective(test.term, &ev)
		assert.Truef(t, match, "test %d: no match for %s", i, test.term)
		assert.Nilf(t, err, "test %d: err not nil", i)
		assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal\nwant %v\ngot %v", i, test.mech, mech)
	}
}

func TestParseDirectiveNonMatch(t *testing.T) {
	tests := []string{
		"bradley",
		"",
		":",
		"hello:",
		"exp=some.domain",
		"redirect",
		"+",
		"++",
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{}
		_, match, err := parseDirective(test, &ev)
		assert.Falsef(t, match, "test %d: false match for %s", i, test)
		assert.Nilf(t, err, "test %d: err not nil", i)
	}
}

func TestParseDirectiveBadSyntax(t *testing.T) {
	//{"a:", mkMech(mechA, qualPass, sender)},
	//{"a:/12", mkMech(mechA, qualPass, sender)},
}

func TestParseDirectiveBadMacro(t *testing.T) {

}

func TestParseDirectiveBadCIDR(t *testing.T) {
	// bad bounds for ip4, ip6 CIDRS on relevant terms
	// ip4 dual cidr, etc.
}

func TestParseDirectiveInvalidParams(t *testing.T) {
	//{"include", mkMech(mechInclude, qualPass, "")}, // needs domainspec
	// include w/ CIDR

	// ptr w/ CIDR
}

func TestParseDirective(t *testing.T) {
	tests := []struct {
		term   string
		mech   mechanism
		match  bool
		ok     bool
		result result
	}{
		// TODO: cases!
		{"all", mkMech(mechAll, qualPass, ""), true, true, resultNone},
		{"+all", mkMech(mechAll, qualPass, ""), true, true, resultNone},
		{"-all", mkMech(mechAll, qualFail, ""), true, true, resultNone},
		{"~all", mkMech(mechAll, qualSoftFail, ""), true, true, resultNone},
		{"?all", mkMech(mechAll, qualNeutral, ""), true, true, resultNone},
		{"?all", mkMech(mechAll, qualNeutral, ""), true, true, resultNone},
	}

	for i, test := range tests {
		// NOTE: eval needed for macro expansion
		ev := evaluation{}

		mech, match, err := parseDirective(test.term, &ev)
		assert.Equalf(t, test.match, match, "test %d: match not same for %s", i, test.term)

		// not a mechanism
		if !test.match {
			continue
		}

		// good parses
		if test.ok {
			assert.Nilf(t, err, "test %d: err not nil", i)
			assert.Truef(t, mechEqual(test.mech, mech), "test %d: mech not equal", i)
			continue
		}

		// error cases - bad macro or invalid args
		assert.NotNilf(t, err, "test %d: err should not be nil", i)
		serr := err.(*spfError)
		assert.Equalf(t, test.result, serr.Result, "test %d: bad error result", i)
	}
}

func TestParseModifier(t *testing.T) {
	// TODO:
}
