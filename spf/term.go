package spf

import (
	"errors"
	"net"
	"strconv"
	"strings"
)

// mechanisms
const (
	mechAll = iota
	mechInclude
	mechA
	mechMX
	mechPTR
	mechIP4
	mechIP6
	mechExists
)

// modifiers
const (
	modRedirect = iota
	modExp
	modUnknown
)

// term types
const (
	termTypeMechanism = iota
	termTypeModifier
)

type term interface {
	termType() int
	causesDNS() bool
}

type mechanism struct {
	qualifier  int
	mechType   int
	domainSpec string
	parsedCIDR bool
	cidrV4     int
	cidrV6     int
	ipV4       net.IP
	ipV6       net.IP
}

var mechanisms = map[string]int{
	"all":     mechAll,
	"include": mechInclude,
	"a":       mechA,
	"mx":      mechMX,
	"ptr":     mechPTR,
	"ip4":     mechIP4,
	"ip6":     mechIP6,
	"exists":  mechExists,
}

func (m *mechanism) termType() int {
	return termTypeMechanism
}

func (m *mechanism) causesDNS() bool {
	switch m.mechType {
	case mechInclude, mechA, mechMX, mechPTR, mechExists:
		return true
	default:
		return false
	}
}

// TODO: call expand and valid from this, then we don't need double fields
func parseDirective(term string, ev *evaluation) (mechanism, bool, error) {
	qual, nameStart := parseQualifier(term)

	// TODO: something is going wrong here w/ parsing domain
	// e.g. exists:whom.email

	// the mechanism would be entire thing, or up to ':' or '/' in possible CIDR case
	nameEnd := len(term)
	c := strings.IndexByte(term, ':')
	if c > -1 {
		nameEnd = c
	}

	sl := strings.IndexByte(term, '/')
	if sl > -1 && sl < nameEnd {
		nameEnd = sl
	}

	name := term[nameStart:nameEnd]
	m := mechanism{qualifier: qual, cidrV4: net.IPv4len * 8, cidrV6: net.IPv6len * 8}

	// do we have a domainSpec or network (post ':' section?
	var dsn string
	if c > -1 && (sl == -1 || sl > c) {
		ce := len(term)
		if sl > -1 {
			ce = sl
		}
		dsn = term[c+1 : ce]
	}

	mt, ok := mechanisms[name]
	if !ok {
		// not a mechanism
		return m, false, nil
	}

	var err error
	m.mechType = mt
	switch m.mechType {
	case mechIP4:
		m.ipV4 = net.ParseIP(dsn)
		if m.ipV4 == nil || m.ipV4.To4() == nil {
			err = newSPFError("bad ip", resultPermerror)
		} else if sl > -1 {
			// check ipv4 CIDR
			m.cidrV4, err = singleCIDR(32, term[sl:])
		}
	case mechIP6:
		m.ipV6 = net.ParseIP(dsn)
		if m.ipV6 == nil || m.ipV6.To16() == nil {
			err = newSPFError("bad ip", resultPermerror)
		} else if sl > -1 {
			// check ipv6 CIDR
			m.cidrV6, err = singleCIDR(128, term[sl:])
		}
	default:
		m.domainSpec = dsn
		if sl > -1 {
			// check dual CIDR
			m.cidrV4, m.cidrV6, err = dualCIDR(term[sl:])
		}
	}

	if err != nil {
		return m, true, err
	}

	err = m.expand(ev)
	if err != nil {
		return m, true, err
	}

	err = m.valid()
	return m, true, err
}

func singleCIDR(max int, cidr string) (int, error) {
	// ip4-cidr-length  = "/" ("0" / %x31-39 0*1DIGIT) ; value range 0-32
	// ip6-cidr-length  = "/" ("0" / %x31-39 0*2DIGIT) ; value range 0-128
	res := resultPermerror
	if len(cidr) <= 1 {
		return 0, newSPFError("bad CIDR syntax, empty", res)
	}

	if cidr[0] != '/' {
		return 0, newSPFError("bad CIDR syntax, missing '/'", res)
	}

	u, err := strconv.ParseUint(cidr[1:], 10, 8)
	if err != nil {
		return 0, newSPFError("bad CIDR value", res)
	}

	val := int(u)
	if val > max {
		return 0, newSPFError("CIDR too large", res)
	}

	if val == 0 && len(cidr) > 2 {
		return 0, newSPFError("bad CIDR syntax, should be single 0", res)
	}

	if val > 0 && cidr[1] == '0' {
		return 0, newSPFError("bad CIDR syntax, invalid leading zero", res)
	}

	return val, nil
}

func dualCIDR(cidr string) (int, int, error) {
	// dual cidr: [ipv4-cidr] ["/" ipv6-cidr]
	sl := strings.IndexByte(cidr[1:], '/')
	if sl < 0 {
		// ipv4 only, '/32' e.g., where we skipped start of v4
		v4, err := singleCIDR(32, cidr)
		return v4, 128, err
	}

	if sl == 0 {
		// ipv6 only, '//128' e.g., where sl hit the start of v6 since we skipped sep
		v6, err := singleCIDR(128, cidr[1:])
		return 32, v6, err
	}

	// TODO confirm double slash
	// both, '/32//128', e.g., where sl hit the sep since we skipped start of v4
	v4, err := singleCIDR(32, cidr[:sl])
	if err != nil {
		return 0, 0, err
	}

	v6, err := singleCIDR(128, cidr[sl+1:]) // NOTE: may be empty
	return v4, v6, err
}

func (m *mechanism) expand(ev *evaluation) error {
	// NOTE: this will expand CIDR even if it doesn't belong.
	// It is up to the valid call to decide whether the expansion makes sense.
	var err error
	switch m.mechType {
	case mechInclude, mechExists, mechA, mechMX, mechPTR:
		if m.domainSpec != "" {
			m.domainSpec, err = ev.domainSpec(m.domainSpec)
		}
	default:
	}

	return err
}

func (m *mechanism) valid() error {
	var err error
	res := resultPermerror
	switch m.mechType {
	case mechAll:
		if m.domainSpec != "" || m.parsedCIDR {
			err = newSPFError("mechanism has unexpected args", res)
		}
	case mechInclude, mechExists:
		if m.domainSpec == "" {
			err = newSPFError("mechanism requires domain spec", res)
		} else if m.parsedCIDR {
			err = newSPFError("mechanism has unexpected CIDR", res)
		}
	case mechA, mechMX:
		// nothing to do, both optional
	case mechPTR:
		// optional domain spec
		if m.parsedCIDR {
			err = newSPFError("mechanism has unexpected CIDR", res)
		}
	case mechIP4:
		if m.ipV4 == nil {
			// checked during parse, but doesn't hurt
			err = newSPFError("mechanism missing network", res)
		}
	case mechIP6:
		if m.ipV6 == nil {
			// checked during parse, but doesn't hurt
			err = newSPFError("mechanism missing network", res)
		}
	}

	return err
}

type modifier struct {
	modType int
	value   string
}

func (m *modifier) termType() int {
	return termTypeModifier
}

func (m *modifier) causesDNS() bool {
	// NOTE: exp does cause a TXT lookup, but doesn't count towards limit here
	return m.modType == modRedirect
}

func parseModifier(term string, ev *evaluation) (modifier, bool, error) {
	parts := strings.SplitN(term, "=", 2)
	mod := modifier{}
	if len(parts) > 1 {
		mod.value = parts[1]
	}

	switch parts[0] {
	case "redirect":
		mod.modType = modRedirect
	case "exp":
		mod.modType = modExp
	default:
		if len(parts) < 2 {
			// not a modifier
			return mod, false, nil
		}

		// we have an unknown modifier (ok)
		mod.modType = modUnknown
		return mod, true, nil
	}

	// redirect and exp require domain spec.
	// NOTE: even for exp, it's a domain, not the explain text
	var err error
	mod.value, err = ev.domainSpec(mod.value)
	if err != nil {
		return mod, true, err
	}

	if !mod.valid() {
		err = errors.New("invalid modifier syntax")
	}

	return mod, true, err
}

func (m *modifier) valid() bool {
	return m.value != ""
}
