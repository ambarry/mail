package spf

// qualifiers
const (
	qualPass = iota
	qualFail
	qualSoftFail
	qualNeutral
)

func parseQualifier(term string) (int, int) {
	// [ qualifier ] mechanism
	qual := qualPass

	if len(term) == 0 {
		return qual, 0
	}

	i := 1
	switch term[0] {
	case '+':
		// do nothing
	case '-':
		qual = qualFail
	case '~':
		qual = qualSoftFail
	case '?':
		qual = qualNeutral
	default:
		i = 0 // start at beginning of term, no qualifier, though default to '+'
	}

	return qual, i
}

func qualToStatus(qual int) result {
	switch qual {
	case qualPass:
		return resultPass
	case qualFail:
		return resultFail
	case qualSoftFail:
		return resultSoftFail
	default:
		return resultNeutral
	}
}
