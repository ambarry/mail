package spf

type result int

// result statuses of CheckHost
const (
	resultNone      result = iota // no valid DNS domain name to check, or no SPF record
	resultNeutral                 // we are explicitly not saying whether it's authorized
	resultPass                    // all good
	resultFail                    // failed, the client is not authorized to use the domain
	resultSoftFail                // probably not authorized
	resultTemperror               // some sort of lookup / DNS issue, try again later
	resultPermerror               // the domain's records could not be interpreted and need fixing
)
