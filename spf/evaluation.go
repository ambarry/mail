package spf

import (
	"fmt"
	"net"
	"strings"
	"time"

	"context"

	"gitlab.com/ambarry/mail/msg"
)

// SPF Validation
// See RFC 7208

// 4.6.4 DNS Lookup Limits
const (
	checkHostTimeout = 30 // total seconds for CheckHost timeout, minimum of 20
	voidLookupLimit  = 2  // limit positive DNS answer with 0 count (RCODE 0), or a Name Error answer (RCODE 3)
	dnsLimit         = 10 // max DNS queries caused by SPF eval
)

// TODO: should consider NewEvaluation().CheckHost() so tests can use same ctor func

// CheckHostSPF checks for the SPF TXT RR in DNS.
// We must check the HELO/EHLO host.
// The check SHOULD be performed during the MAIL commad when receiving,
// so as not to later have to create non-delivery notices to forged identities,
// and the policy could also change in between handling.
// <ip>: ip address of client
// <domain>: domain _providing_ authorization. This is initially the helo / ehlo,
// but may not match sender anymore after recursive calls, e.g. redirect.
// <sender>: MAIL FROM / HELO indentity we're checking to see if authorized
// <recvDomain>: Us, e.g. whom.email
func CheckHostSPF(res *net.Resolver, ip net.IPAddr, domain, recvDomain, sender string) (string, result) {
	// If domain is mailformed or lookup returns NXDOMAIN, we return None.
	// NOTE: net.lookup validates domain name for us, no sense in repeating

	// NOTE: context is for this whole operation, not just this first lookup
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*checkHostTimeout)
	defer cancel()

	ev := evaluation{
		resolver:   res,
		ip:         ip,
		domain:     domain,
		recvDomain: recvDomain,
		sender:     sender,
		helo:       domain, // initially these are the same, until recursion
		ctx:        ctx,
	}

	// TODO: may want info for a received SPF header?
	return ev.checkHost()
}

// evaluation contains all parameters needed to check the host
type evaluation struct {
	ip         net.IPAddr
	resolver   *net.Resolver
	inRedirect bool
	domain     string
	recvDomain string
	sender     string
	helo       string
	ctx        context.Context
	dnsCount   int
	voidCount  int
}

func (ev *evaluation) checkHost() (string, result) {
	// TODO: caching mechanism based on TTL if no macros, etc.
	// TODO: we may need to check and encode internationalized domain names to A-labels
	txts, err := ev.resolver.LookupTXT(ev.ctx, ev.domain)
	if err != nil {
		return "", ev.checkSPFLookupError(err)
	}

	if len(txts) == 0 {
		ev.voidCount++
		if ev.voidCount > voidLookupLimit {
			return "", resultPermerror
		}

		return "", resultNone
	}

	// Find SPF record. An error here indicates we should use provivded result.
	spf, err := ev.findSPFInTxts(txts)
	if err != nil {
		serr := err.(*spfError)
		return "", serr.Result
	}

	// now that we have spf, start to eval
	return ev.evalRecord(spf)
}

// checkSPFLookupError checks the error received during net.LookupTXT
// for the main SPF TXT record.
// Invalid domain syntax is checked internally and should return 'none'.
// Name Error (RCODE 3 / does not exist) should also immediately return 'none'.
// When in recursive mode, these 'none' results are treated as 'permerror'.
func (ev *evaluation) checkSPFLookupError(err error) result {
	res := resultNone
	if ev.inRedirect {
		res = resultPermerror
	}

	derr, ok := err.(*net.DNSError)
	if ok && derr.IsNotFound {
		ev.voidCount++
		return res
	}

	// Some other DNS error, temporary
	return resultTemperror
}

// findSPFInTxtx finds the SPF record within a set of TXT RRs.
// We must check for version start up to space or end.
// Returns the SPF txt, or an error with a result status.
func (ev *evaluation) findSPFInTxts(txts []string) (string, error) {
	const v string = "v=spf1"
	const lv int = len(v)
	var spf string
	for _, txt := range txts {
		isSPF := ev.hasSPFVersion(txt)
		if !isSPF {
			continue
		}

		if spf != "" {
			err := newSPFError("multiple SPF records found", resultPermerror)
			return "", err
		}

		spf = txt
	}

	res := resultNone
	if ev.inRedirect {
		res = resultPermerror
	}

	if spf == "" {
		return "", newSPFError("no SPF found in TXTs", res)
	}

	return spf, nil
}

func (ev *evaluation) hasSPFVersion(txt string) bool {
	// Find the SPF record. Check for version start up to space or end
	const v string = "v=spf1"
	const lv int = len(v)
	if len(txt) < lv {
		return false
	}

	if txt[:lv] != v {
		return false
	}

	if len(txt) == lv || txt[lv] == ' ' {
		// found it
		return true
	}

	return false // extra junk
}

func (ev *evaluation) parseTerms(txt string) ([]mechanism, []modifier, error) {
	// skip 'v=spf1' at start, which we know
	i := 6
	if len(txt) <= i {
		// empty record, technically ok I guess, but no matches...
		return nil, nil, nil
	}

	// split on spaces
	s := msg.ConsolidateSpaces(txt[i:])
	s = strings.Trim(s, " ")
	if s == "" {
		return nil, nil, nil
	}
	parts := strings.Split(s, " ")

	// parse terms (hacky)
	mechs := make([]mechanism, 0, len(parts))
	mods := make([]modifier, 0)
	all := false
	for _, part := range parts {
		var t term
		mech, match, err := parseDirective(part, ev)
		if err != nil {
			return nil, nil, err
		}

		if match {
			t = &mech

			// ignore any mechanisms that follow all
			if !all {
				mechs = append(mechs, mech)
				all = mech.mechType == mechAll
			}
		} else {
			mod, ok, err := parseModifier(part, ev)
			if err != nil {
				return nil, nil, err
			}

			if ok {
				// check mod
				t = &mod
				mods = append(mods, mod)
				match = true
			}
		}

		if !match {
			// invalid term or invalid syntax in SPF record
			return nil, nil, newSPFError("parse terms, invalid term or syntax", resultPermerror)
		}

		// check DNS limits
		if t.causesDNS() {
			// NOTE: as we may this in redirect recursion, must maintain limit
			ev.dnsCount++
		}

		if ev.dnsCount > dnsLimit {
			// too many terms would cause DNS load - MUST reject
			return nil, nil, newSPFError("parse terms, too many DNS lookup terms", resultPermerror)
		}
	}

	return mechs, mods, nil
}

func (ev *evaluation) evalRecord(txt string) (string, result) {
	// Evaluate each mechanism for matches.
	// NOTE: since an 'all' here matches no matter what, we satisfy the criteria
	// of not proecessing any redirect with an all.
	res := resultNone
	mechs, mods, err := ev.parseTerms(txt)
	if err != nil {
		if serr, ok := err.(*spfError); ok {
			return "", serr.Result
		}

		return "", resultPermerror
	}

	match := false
	for _, mech := range mechs {
		res, match = ev.evalMechanism(mech)
		if match {
			break
		}
	}

	// This index is in case they put an exp and redirect in the same SPF record
	exp := -1
	for i, mod := range mods {
		if !match && mod.modType == modRedirect {
			// NOTE: they don't specify use of multiple redirects, but seems like first would win...
			// Also, exp from original domain should NOT be used here
			ev.inRedirect = true
			ev.domain = mod.value
			return ev.checkHost()
		}

		if mod.modType == modExp {
			exp = i
		}
	}

	// No explanation applicable
	if !match || res != resultFail || exp < 0 {
		return "", res
	}

	return ev.getExplanation(mods[exp].value), res
}

func (ev *evaluation) getExplanation(expDomain string) string {
	// Lookup and evaluate explanation
	// TODO: context for timeout?
	txts, err := ev.resolver.LookupTXT(context.Background(), expDomain)

	// TODO: check rcode 0?
	if err != nil || len(txts) != 1 || txts[0] == "" {
		return ""
	}

	// expand as explain-string: *( macro-string / SP )
	_, expanded, err := macroString(ev, txts[0], true)
	if err != nil {
		return ""
	}

	// Prepend something like <the domain mods[exp].value> explains:
	return fmt.Sprintf("The domain %s explains: %s", expDomain, expanded)
}

func (ev *evaluation) evalMechanism(mech mechanism) (result, bool) {
	switch mech.mechType {
	case mechAll:
		return ev.all(mech)
	case mechInclude:
		return ev.include(mech)
	case mechA:
		return ev.a(mech)
	case mechMX:
		return ev.mx(mech)
	case mechPTR:
		return ev.ptr(mech)
	case mechIP4:
		return ev.ip4(mech)
	case mechIP6:
		return ev.ip6(mech)
	case mechExists:
		return ev.exists(mech)
	default:
		panic("spf - evalMechanism with unknown mechanism")
	}
}

func (ev *evaluation) all(mech mechanism) (result, bool) {
	// all matches anything
	return qualToStatus(mech.qualifier), true
}

func (ev *evaluation) include(mech mechanism) (result, bool) {
	// This is more like an if-match clause
	// We only care if we match or error, non matches and fails continue the "outer" execution
	// Assumes domain spec was already parsed.

	// recursively check the new domain
	// depending on result, we may restore domain, continue or stop
	d := ev.domain
	ev.domain = mech.domainSpec

	// RFC 7208 6.2: any exp from the included domain must NOT be used
	_, res := ev.checkHost()

	ev.domain = d
	switch res {
	case resultPass:
		// treat as match
		return qualToStatus(mech.qualifier), true
	case resultFail, resultSoftFail, resultNeutral:
		// treat as no match, continue
		return resultNone, false
	default:
		// treat error as error
		return res, true
	}
}

func (ev *evaluation) a(mech mechanism) (result, bool) {
	// Check if <ip> is one of the A or AAAA record's ip addrs.
	// optional domain spec and dual-cidr-length
	// NOTE: when domain spec not present, use domain
	d := mech.domainSpec
	if d == "" {
		d = ev.domain
	}

	// TODO: we could optimize this to only grab A or AAAA records possibly, given our ip
	addrs, err := ev.resolver.LookupIPAddr(context.Background(), d)
	// TODO: Rcode 3 or 0 handled differently...
	if err != nil {
		return resultTemperror, true // TODO: check
	}

	return ev.checkIPs(mech, addrs)
}

// checkIPs conoslidates ip comparisons that require CIDR, etc.
func (ev *evaluation) checkIPs(mech mechanism, addrs []net.IPAddr) (result, bool) {
	// apply CIDR (defaults to full)
	v4 := ev.ip.IP.To4()
	v6 := ev.ip.IP.To16()
	var ipCl []byte
	if v4 != nil {
		v4 = v4[int(mech.cidrV4):]
		ipCl = v4
	} else {
		v6 = v6[int(mech.cidrV6):]
		ipCl = v6
	}

	matched := false
	for _, addr := range addrs {
		var bytes []byte
		if v4 != nil {
			// v4
			if ov4 := addr.IP.To4(); ov4 != nil {
				bytes = ov4[int(mech.cidrV4):]
			}
		} else if ov6 := addr.IP.To16(); ov6 != nil {
			// look at ipV6 addrs only
			bytes = ov6[int(mech.cidrV6):]
		}

		if bytes == nil {
			// wrong ip type
			continue
		}

		matched := bytesEqual(ipCl, bytes)
		if matched {
			break
		}
	}

	if matched {
		return qualToStatus(mech.qualifier), true
	}

	return resultNone, false
}

func (ev *evaluation) mx(mech mechanism) (result, bool) {
	// TODO: need to apply processing limits
	// first we get addrs for MX, then ips for addrs
	d := mech.domainSpec
	if d == "" {
		d = ev.domain
	}

	mxs, err := ev.resolver.LookupMX(context.Background(), d)
	if err != nil {
		// TODO: check err type
		return resultTemperror, true
	}

	if len(mxs) > 10 {
		// DNS limits: any more than 10 lookups (per eval), we have to permerror
		return resultPermerror, true
	}

	for _, mx := range mxs {
		ipAddrs, err := ev.resolver.LookupIPAddr(context.Background(), mx.Host)
		if err != nil {
			// TODO: check err type
			return resultTemperror, false
		}
		res, match := ev.checkIPs(mech, ipAddrs)
		if match {
			// NOTE: this already applies qualifier!
			return res, true
		}
	}

	return resultNone, false
}

func (ev *evaluation) ptr(mech mechanism) (result, bool) {
	d := mech.domainSpec
	if d == "" {
		d = ev.domain
	}

	_, match := ev.ptrLookup(d, ev.ip.IP)
	if match {
		return qualToStatus(mech.qualifier), true
	}

	return resultNone, false
}

func (ev *evaluation) ip4(mech mechanism) (result, bool) {
	// TODO: I don't think these can cross, but should double-check, e.g. compare v6 to a v4
	v4 := ev.ip.IP.To4()
	if v4 == nil {
		return resultNone, false
	}

	v4 = v4[mech.cidrV4:]
	match := bytesEqual(v4, mech.ipV4[mech.cidrV4:])
	if match {
		return qualToStatus(mech.qualifier), true
	}

	return resultNone, false
}

func (ev *evaluation) ip6(mech mechanism) (result, bool) {
	// TODO: I don't think these can cross, but should double-check, e.g. compare v6 to a v4
	v6 := ev.ip.IP.To16()
	if v6 == nil {
		return resultNone, false
	}

	v6 = v6[mech.cidrV6:]
	match := bytesEqual(v6, mech.ipV6[mech.cidrV6:])
	if match {
		return qualToStatus(mech.qualifier), true
	}

	return resultNone, false
}

func (ev *evaluation) exists(mech mechanism) (result, bool) {
	// this should always lookup an A record, regardless of client connection type
	// if any A record exists, we have a match
	ipAddrs, err := ev.resolver.LookupIPAddr(context.Background(), mech.domainSpec)
	if err != nil {
		// TODO: check type 0 or 3?
		return resultTemperror, true
	}

	for _, addr := range ipAddrs {
		// must have been A record
		if addr.IP.To4() != nil {
			return qualToStatus(mech.qualifier), true
		}
	}

	return resultNone, false
}

// ptrLookup as per RFC 7208 5.5
// NOTE: PTR should NOT be used, but must be supported.
// This can also be used during expansion of the 'p' macro.
// It returns the domain, subdomain, best record, or "unknown" as
// well as a bool indicating whether domain or subdomain we're found.
func (ev *evaluation) ptrLookup(target string, ip net.IP) (string, bool) {
	// NOTE: This takes care of the reversal and correct arpa (ipv4 vs ipv6)
	addrs, err := ev.resolver.LookupAddr(context.Background(), ip.String())
	if err != nil {
		// If a DNS error occurs while doing the PTR RR lookup, then this mechanism fails to match.
		return "unknown", false
	}

	// max of 10 addrs records per PTR can be evaluated, with PTR we ignore any after 10
	if len(addrs) > 10 {
		addrs = addrs[:10]
	}

	valDomains := make([]string, 0)
	for _, addr := range addrs {
		ipAddrs, err := ev.resolver.LookupIPAddr(context.Background(), addr)
		// "If a DNS error occurs while doing an A RR lookup, then that domain name is skipped
		// and the search continues."
		if err != nil {
			continue
		}

		for _, ipAddr := range ipAddrs {
			if ipAddr.IP.Equal(ip) {
				valDomains = append(valDomains, addr)
				break
			}
		}
	}

	best := ""
	match := false
	trimDot := target[len(target)-1] != '.'
	for _, d := range valDomains {
		if trimDot {
			d = strings.TrimSuffix(d, ".") // TODO: do we need to restore? doubtful...
		}
		if target == d {
			best = d
			match = true
			break
		}

		if isSubdomain(target, d) && best == "" {
			best = d
			match = true
		}
	}

	if !match && len(valDomains) > 0 {
		best = valDomains[0]
	} else {
		best = "unknown"
	}

	return best, match
}

func bytesEqual(a, b []byte) bool {
	// assumes we've already checked lens
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func (ev *evaluation) domainSpec(ds string) (string, error) {
	// just expand the macro, the grammar is ambiguous
	// technically
	if len(ds) == 0 {
		return "", newSPFError("empty domain spec", resultPermerror)
	}

	specEnd, expanded, err := macroString(ev, ds, false)
	if err != nil {
		return "", err
	}

	// Did we end in a valid macro or top level?
	if !specEnd {
		return "", newSPFError("invalid domain end", resultPermerror)
	}

	// Can't exceed 253 chars.
	// If it does, truncate domain labels on the left side, including trailing dot, until short enough.
	for len(expanded) > 253 {
		i := strings.IndexByte(expanded, '.')
		if i < 0 || i == len(expanded)-1 {
			// either no dot, or dot is last char
			return "", newSPFError("domain spec too long", resultPermerror)
		}

		// truncate left side through domain label and dot
		expanded = expanded[i+1:]
	}

	return expanded, nil
}
