package spf

import (
	"net"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMacroExpansionV4(t *testing.T) {
	// client setup
	ip4 := net.ParseIP("192.0.2.3")
	domain := "email.example.com"
	sender := "strong-bad@email.example.com"
	tests := []struct {
		spec      string
		expansion string
	}{
		{"%{s}", "strong-bad@email.example.com"},
		{"%{o}", "email.example.com"},
		{"%{d}", "email.example.com"},
		{"%{d4}", "email.example.com"},
		{"%{d3}", "email.example.com"},
		{"%{d2}", "example.com"},
		{"%{d1}", "com"},
		{"%{dr}", "com.example.email"},
		{"%{d2r}", "example.email"},
		{"%{l}", "strong-bad"},
		{"%{l-}", "strong.bad"},
		{"%{lr}", "strong-bad"},
		{"%{lr-}", "bad.strong"},
		{"%{l1r-}", "strong"},
	}

	for i, test := range tests {
		ev := evaluation{sender: sender, domain: domain, helo: domain, ip: net.IPAddr{IP: ip4}}
		builder := strings.Builder{}

		// NOTE: we know in this case, we've already parsed the '%'
		n, err := macroExpand(&ev, &builder, test.spec[1:], false)
		assert.Nilf(t, err, "test %d: err is not nil %v", i, err)
		assert.Equalf(t, len(test.spec)-1, n, "test %d: length mismatch, want %d, got %d", i, len(test.spec)-1, n)

		expansion := builder.String()
		assert.Equalf(t, test.expansion, expansion,
			"test %d: bad expansion, want %s, got %s",
			i, test.expansion, expansion)
	}
}

// TODO: bad c///////////////////////////asestrggggggm,,, 65?

func TestMacroExpandV6(t *testing.T) {
	// ip6 := net.ParseIP("2001:db8::cb01")
	// sender := "strong-bad@email.example.com"
	// ptrDomain := "mx.example.org"
	// macro := "%{ir}.%{v}._spf.%{d2}"
	// expanded := "1.0.b.c.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0." +
	// 	"8.b.d.0.1.0.0.2.ip6._spf.example.com"
}

func TestMacroPtrLookup(t *testing.T) {
	// TODO: how to test this? dns resolver?

}

func TestExplanationExpand(t *testing.T) {
	// TODO: test extra chars, spaces, etc.
}

func TestMacroEscape(t *testing.T) {
	// TODO: test the 3 escape sequences
}

func TestMacroString(t *testing.T) {
	// TODO: test that a combination of escapes, macros, either or, are parsed
	ip4 := net.ParseIP("192.0.2.3")
	domain := "email.example.com"
	sender := "strong-bad@email.example.com"
	tests := []struct {
		spec      string
		expansion string
	}{

		{"%{ir}.%{v}._spf.%{d2}", "3.2.0.192.in-addr._spf.example.com"},
		{"%{lr-}.lp._spf.%{d2}", "bad.strong.lp._spf.example.com"},
		{"%{lr-}.lp.%{ir}.%{v}._spf.%{d2}", "bad.strong.lp.3.2.0.192.in-addr._spf.example.com"},
		{"%{ir}.%{v}.%{l1r-}.lp._spf.%{d2}", "3.2.0.192.in-addr.strong.lp._spf.example.com"},
		{"%{d2}.trusted-domains.example.net", "example.com.trusted-domains.example.net"},
	}

	for i, test := range tests {
		ev := evaluation{sender: sender, domain: domain, helo: domain, ip: net.IPAddr{IP: ip4}}
		end, expansion, err := macroString(&ev, test.spec, false)
		assert.Nilf(t, err, "test %d: err is not nil %v", i, err)
		assert.Truef(t, end, "test %d: end not detected", i)
		assert.Equalf(t, test.expansion, expansion,
			"test %d: expansion bad, wanted %s, got %s", i, test.expansion, expansion)
	}
	// TODO: additional test to make sure we consumed length and/or properly marked domain-end
}
