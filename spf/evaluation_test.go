package spf

import (
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TODO: not sure how to test the others without
// somehow mocking the DNS or else
// wrapping in testable interface.
// Is that worth it?

func TestSPFVersionCheck(t *testing.T) {
	tests := []struct {
		text string
		has  bool
	}{
		{"", false},
		{"spf", false},
		{" ", false},
		{"   ", false},
		{"v=spf1", true},
		{"v=spf1 ", true},
		{"v=spf1x", false},
		{"v=spf2", false},
		{"spf1", false},
		{"v:spf1", false},
	}

	for _, test := range tests {
		ev := evaluation{}
		has := ev.hasSPFVersion(test.text)
		assert.Equalf(t, test.has, has, "for %s, expected %v, got %v", test.text, test.has, has)
	}
}

func TestFindSPFInTxts(t *testing.T) {
	tests := []struct {
		texts      []string
		redir      bool
		ok         bool
		result     result
		validIndex int
	}{
		// No SPF found
		{[]string{}, false, false, resultNone, 0},
		{[]string{}, true, false, resultPermerror, 0},
		{[]string{""}, false, false, resultNone, 0},
		{[]string{""}, true, false, resultPermerror, 0},
		{[]string{"a", "b"}, false, false, resultNone, 0},
		{[]string{"a", "b"}, true, false, resultPermerror, 0},

		// Bad SPF
		{[]string{"v=spf1zz"}, false, false, resultNone, 0},
		{[]string{"v=spf2"}, true, false, resultPermerror, 0},

		// Multiple SPFs (always permerror)
		{[]string{"v=spf1", "v=spf1"}, false, false, resultPermerror, 0},
		{[]string{"A", "v=spf1", "v=spf1", "v=spf1"}, true, false, resultPermerror, 0},

		// Good SPF
		{[]string{"v=spf1"}, false, true, resultNone, 0},
		{[]string{"hello", "v=spf1"}, false, true, resultNone, 1},
		{[]string{"v=spf1 mx -all", "something else entirely"}, false, true, resultNone, 0},
		{[]string{"v=spf1 +mx ?a:whom.email include=_spf.whom.email -all"}, false, true, resultNone, 0},
		{[]string{"v=spf1 redirect=%{ir}.spf.whom.email"}, false, true, resultNone, 0},
	}

	for _, test := range tests {
		ev := evaluation{}
		spf, err := ev.findSPFInTxts(test.texts)
		if test.ok {
			assert.Nil(t, err)
			assert.Equalf(t, test.texts[test.validIndex], spf, "bad spf returned")

		} else {
			assert.NotNil(t, err)
			derr := err.(*spfError)
			assert.Equalf(t, test.result, derr.Result, "error result doesn't match")
		}
	}
}

type spfTermCase struct {
	spf    string
	ok     bool
	result result
	mechs  []mechanism
	mods   []modifier
}

func tcPass(spf string) *spfTermCase {
	spf = "v=spf1 " + spf
	return &spfTermCase{spf: spf, ok: true, result: resultNone}
}

func tcFail(spf string, result result) *spfTermCase {
	spf = "v=spf1 " + spf
	return &spfTermCase{spf: spf, ok: false, result: result}
}

func (c *spfTermCase) withMechs(mechs ...mechanism) *spfTermCase {
	c.mechs = mechs
	return c
}

func (c *spfTermCase) withMods(mods ...modifier) *spfTermCase {
	c.mods = mods
	return c
}

func TestParseTerms(t *testing.T) {
	// NOTE: we've guaranteed the spf version before getting here
	// TODO: test parse terms as these are failing on domain spec?
	clientAddr := net.IPAddr{IP: net.ParseIP("10.2.155.7")}
	tests := []*spfTermCase{
		tcPass(""),
		tcPass("   "),
		tcPass("mx").withMechs(mkMech(mechMX, qualPass, "")),
		tcPass(" -mx").withMechs(mkMech(mechMX, qualFail, "")),
		tcPass("~mx -all").withMechs(
			mkMech(mechMX, qualSoftFail, ""),
			mkMech(mechAll, qualFail, "")),
		tcPass("~mx exists:whom.email -all").withMechs(
			mkMech(mechMX, qualSoftFail, ""),
			mkMech(mechExists, qualPass, "whom.email"),
			mkMech(mechAll, qualFail, "")),
		tcPass("include:_spf.whom.email a -all").withMechs(
			mkMech(mechInclude, qualPass, "_spf.whom.email"),
			mkMech(mechA, qualPass, ""),
			mkMech(mechAll, qualFail, "")),
		tcPass(" a:something.whatever  include:_spf.whom.email   -all").withMechs(
			mkMech(mechA, qualPass, "something.whatever"),
			mkMech(mechInclude, qualPass, "_spf.whom.email"),
			mkMech(mechAll, qualFail, "")),
		tcPass("   redirect:_spf.whom.email   ").withMods(
			modifier{modRedirect, "_spf.whom.email"}),
		tcPass("a:super.cool.domain -mx:other.yeah ?ip4:127.0.0.1/12 "+
			"+ip6:2001:db8::ff00:42:8329 include:whom.whatever "+
			"redirect=_spf.whom.email exp=exp.whom.ninja ").withMechs(
			mkMech(mechA, qualPass, "super.cool.domain"),
			mkMech(mechMX, qualFail, "other.yeah"),
			mechanism{qualNeutral, mechIP4, "", true, 12, 128, net.ParseIP("127.0.0.1"), nil},
			mechanism{qualPass, mechIP6, "", false, 32, 128, nil, net.ParseIP("2001:db8::ff00:42:8329")},
			mkMech(mechInclude, qualPass, "whom.whatever"),
		).withMods(
			modifier{modRedirect, "_spf.whom.email"},
			modifier{modExp, "exp.whom.ninja"}),
		tcPass("ptr:%{i3r}_spf.whom.email").withMechs(
			mkMech(mechInclude, qualPass, "7.155.2._spf.whom.email")), // NOTE: no extra padding or anything

		// TODO: Bad cases (syntax or missing elements, : vs =, etc.)
		// {" x", true, resultNone, nil, nil},
		// {"  x ", true, resultNone, nil, nil},
		// {"&mx", true, resultNone, nil, nil},
		// {" --mx", true, resultNone, nil, nil},
		// {"~+mx -all", true, resultNone, nil, nil},
		// {"~mx exists=whom.email -all", true, resultNone, nil, nil},
		// {"include a -all", true, resultNone, nil, nil},
		// {" a:something.whateverinclude:_spf.whom.email   -all", true, resultNone, nil, nil},
		// {"   redirect: _spf.whom.email   ", true, resultNone, nil, nil},
		// {"a:super.cool.domain -mx:other.yeah ?ip4:127.0.1/12 +ip6:2001:db8::ff00:42:8329 include:whom.whatever +redirect=_spf.whom.email exp=exp.whom.ninja ", true, resultNone, nil, nil},
	}

	for i, test := range tests {
		// TODO: need to supply more in ev for macro expansion
		ev := evaluation{ip: clientAddr}
		mechs, mods, err := ev.parseTerms(test.spf)
		if test.ok {
			assert.Nil(t, err)
			assert.Equal(t, len(test.mechs), len(mechs), "pass %d: mech count doesn't match, exp %d, got %d", i, len(test.mechs), len(mechs))
			for j, m := range mechs {
				eq := mechEqual(test.mechs[j], m)
				if !eq {
					fmt.Println(test.mechs[j])
					fmt.Println(m)
				}
				assert.Truef(t, eq, "pass %d: mech %d doesn't match", i, j)
			}

			assert.Equalf(t, len(test.mods), len(mods), "pass %d: mod count doesn't match", i)
			for j, m := range mods {
				assert.Truef(t, modEqual(test.mods[j], m), "pass %d: mod %d doesn't match", i, j)
			}

			continue
		}

		assert.NotNil(t, err)
		serr := err.(*spfError)
		assert.Equalf(t, test.result, serr.Result, "error result doesn't match")
	}
}

func TestDNSTermLimit(t *testing.T) {
	// parse DNS terms
}

func TestEvaluateSPF(t *testing.T) {
	// TODO: some spoof dns provider instead of lookup txt directly?
}

func TestTimeout(t *testing.T) {
	// expect temperror afer 20 seconds?
}

func TestCheckIPs(t *testing.T) {

}

func TestIPV6Dotted(t *testing.T) {

}

// --- test helpers ---

func mechEqual(a, b mechanism) bool {
	return a.qualifier == b.qualifier &&
		a.mechType == b.mechType &&
		a.domainSpec == b.domainSpec &&
		a.parsedCIDR == b.parsedCIDR &&
		a.cidrV4 == b.cidrV4 &&
		a.cidrV6 == b.cidrV6 &&
		bytesNilOrEqual(a.ipV4, b.ipV4) &&
		bytesNilOrEqual(a.ipV6, b.ipV6)
}

func modEqual(a, b modifier) bool {
	return a.modType == b.modType &&
		a.value == b.value
}

func bytesNilOrEqual(a, b []byte) bool {
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	return bytesEqual(a, b)
}

func mkMech(t, qual int, ds string) mechanism {
	return mechanism{
		mechType:   t,
		domainSpec: ds,
		qualifier:  qual,
		cidrV4:     32,
		cidrV6:     128,
	}
}
