package spf

// spfError is an internal error for specifying an accompanying result
type spfError struct {
	message string
	Result  result
}

// newSPFError creates a new spfError from message and result type
func newSPFError(msg string, res result) error {
	err := spfError{message: msg, Result: res}
	return &err
}

// Error implements error interface and returns error message
func (err *spfError) Error() string {
	return err.message
}
