package spf

import (
	"encoding/hex"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ambarry/mail/ascii"
)

// TODO: this is junk

var macroDelims = []rune{
	'.',
	'-',
	'+',
	',',
	'/',
	'_',
	'=',
}

// macroString expands the macro-string construction.
// Because macro-string can have any ASCII printable chars including dots and curly
// braces, and macro-string can be empty, this grammar is junk and we can't tell ahead
// of time whether the string is valid.
// Really, we just know when we hit an illegal char, bad '{' follower, and we can
// possibly see if we ended on the proper domain-end.
// But we can't tell that just going LR.
// Good luck...
func macroString(ev *evaluation, s string, exp bool) (bool, string, error) {

	// Track last so we know if we satisfied grammar.
	// NOTE: that when this is used in some cases, we may not care...
	mac := false // last was macro-expand
	top := false // ends in top label

	domain := strings.Builder{}
	i := 0
	var j int
	var err error
	for i < len(s) {
		c := rune(s[i])
		if c == '%' {
			// macro expand
			i++
			j, err = macroExpand(ev, &domain, s[i:], exp)
			if err != nil {
				return false, "", err
			}

			// advance
			// TODO: check this! may need to return + 1
			i += j
			mac = true
		} else if isASCIIVisible(c) {
			mac = false
			domain.WriteRune(c)
			i++
			if c == '.' {
				// check for domain-end == . toplabel [.]
				top, j = readPotentialTopLabel(&domain, s[i:])
				i += j // update amount read
			}
		} else if exp && c == ' ' {
			// spaces allowed in explain string
			domain.WriteRune(c)
			i++
			mac = false
			top = false
		} else {
			return false, "", newSPFError("illegal character", resultPermerror)
		}
	}

	domainSpecEnd := mac || top
	return domainSpecEnd, domain.String(), nil
}

func readPotentialTopLabel(domain *strings.Builder, ds string) (bool, int) {
	//    toplabel         = ( *alphanum ALPHA *alphanum ) /
	//                       ( 1*alphanum "-" *( alphanum / "-" ) alphanum )
	//    alphanum         = ALPHA / DIGIT

	// note: we've read over the initial dot to get here
	// stop at next dot or macro expand or inval char, then let rest keep going
	// if we reach a dot, that'll be ok too if last char of string, otherwise this is not the toplabel
	// TODO: clean this all up
	if ds == "" {
		return false, 0
	}

	hasAlpha := false
	hasDash := false
	endDot := false
	for j, r := range ds {
		if ascii.IsAlpha(r) {
			hasAlpha = true
		} else if r == '-' {
			hasDash = true
		} else if r == '.' {
			endDot = j == len(ds)-1
			if !endDot {
				// not the end dot, therefore not top label or invalid
				return false, j
			}
		} else if !ascii.IsDigit(r) {
			// not valid top label char
			return false, j
		}

		// process it
		domain.WriteRune(r)
	}

	// can't be first pattern
	if !hasAlpha && !hasDash {
		return false, len(ds)
	}

	// can't just be a dot
	if endDot && len(ds) == 1 {
		return false, len(ds)
	}

	if !hasDash {
		// we're good
		return true, len(ds)
	}

	// can't start in dash
	if ds[0] == '-' {
		return false, len(ds)
	}

	// can't end in dash
	index := len(ds) - 1
	if endDot {
		index--
	}
	r := ds[index]
	if r == '-' {
		return false, len(ds)
	}

	return true, len(ds)
}

func macroExpand(ev *evaluation, domain *strings.Builder, ds string, exp bool) (int, error) {
	// we know first char is a '%'
	if ds == "" {
		return 0, newSPFError("macro expand, nothing following '%'", resultPermerror)
	}

	// macro or escape?
	esc := true
	switch ds[0] {
	case '%':
		domain.WriteRune('%')
	case '_':
		domain.WriteRune(' ')
	case '-':
		domain.WriteString("%20")
	case '{':
		// macro time
		esc = false
	default:
		return 0, newSPFError("macro expand, bad character folloing '%'", resultPermerror)
	}

	// found escaped char
	if esc {
		return 1, nil
	}

	// at the minimum, need a char and to close the brackets
	if len(ds) < 3 {
		return 0, newSPFError("incomplete macro", resultPermerror)
	}

	// macro expansion
	j := 1
	m := ds[j]
	expxn := ""
	switch m {
	case 's':
		// sender
		expxn = ev.sender
	case 'l':
		// local part of sender
		expxn = strings.Split(ev.sender, "@")[0]
	case 'o':
		// domain of sender
		expxn = strings.Split(ev.sender, "@")[1]
	case 'd':
		// <domain>
		expxn = ev.domain
	case 'i':
		// <ip>
		// NOTE: do NOT rely on length, go parses ipV4s into 16 bytes for whatever reason...
		v4 := ev.ip.IP.To4()
		if v4 != nil {
			// For IPv4 addresses, both the "i" and "c" macros expand to the
			// standard dotted-quad format.
			expxn = ev.ip.String()
		} else if ev.ip.IP.To16() != nil {
			// For IPv6 addresses, the "i" macro expands to a dot-format address; it
			// is intended for use in %{ir}.  The "c" macro can expand to any of the
			// hexadecimal colon-format addresses specified in Section 2.2 of
			// [RFC4291].  It is intended for humans to read.
			expxn = ipV6Dotted(ev.ip.IP)
		} else {
			return 0, newSPFError("bad client ip", resultPermerror)
		}
	case 'p':
		// the validated domain name of <ip> (do not use)
		// This returns matched domain, subdomain, any validated domain,
		// or "unknown" if no validated domains are found.
		// When performing ptr mechanism, we only accept mathches.
		// But for macro, we use whatever.
		expxn, _ = ev.ptrLookup(ev.domain, ev.ip.IP)

	case 'h':
		// HELO/EHLO domain
		expxn = ev.helo
	case 'v':
		// the string "in-addr" if <ip> is ipv4, or "ip6" if <ip> is ipv6
		if ev.ip.IP.To4() != nil {
			expxn = "in-addr"
		} else if ev.ip.IP.To16() != nil {
			expxn = "ip6"
		} else {
			return 0, newSPFError("bad client ip", resultPermerror)
		}
	case 'c':
		// exp only SMTP client IP (easily readable format)
		if !exp {
			return 0, newSPFError("c macro only supported in exp mechanism", resultPermerror)
		}
		// ipv4 - standard dotted
		// ipv6 - any of the colon-formats in RFC4291 2.2
		// (String handles ipv6 how we want already)
		expxn = ev.ip.String()

	case 'r':
		// exp only domain name of host performing the check
		if !exp {
			return 0, newSPFError("r macro only supported in exp mechanism", resultPermerror)
		}

		// Domain of us (MTA) or else "unknown" - should be fully qualified
		expxn = ev.recvDomain

	case 't':
		// exp only current timestamp (UNIX sec UTC)
		if !exp {
			return 0, newSPFError("t macro only supported in exp mechanism", resultPermerror)
		}

		expxn = fmt.Sprintf("%d", time.Now().Unix())
	default:
		return 0, newSPFError("bad macro letter", resultPermerror)
	}

	j++
	z := j
	for z < len(ds) && ascii.IsDigit(rune(ds[z])) {
		z++ // TODO: off by one chance?
	}

	dig := -1
	if z > j {
		dig, _ = strconv.Atoi(ds[j:z])
		j = z
	}

	if j >= len(ds) {
		return 0, newSPFError("incomplete macro", resultPermerror)
	}

	rev := false
	if ds[j] == 'r' {
		rev = true
		j++
	}

	if j >= len(ds) {
		return 0, newSPFError("incomplete macro", resultPermerror)
	}

	delims := "." // default to '.'
	z = j
	for z < len(ds) && isDelim(rune(ds[z])) {
		z++
	}

	if z > j {
		delims = ds[j:z]
		j = z
	}

	parts := ascii.Split(expxn, delims)
	if rev {
		// reverse the parts
		np := make([]string, len(parts))
		for i := 0; i < len(parts); i++ {
			np[i] = parts[len(parts)-1-i]
		}
		parts = np
	}

	if dig == 0 {
		// we parsed a 0 -> this is an error
		return 0, newSPFError("bad DIGIT in macro", resultPermerror)
	}

	if dig > 127 {
		// must support at least 127, though there don't actually have to be as many parts
		dig = 127
	}
	if dig > 0 && dig < len(parts) {
		// take dig _rightmost_ parts, splitting on delimeters as indicated
		parts = parts[len(parts)-dig:]
	}

	// rejoin w/ '.' no matter what and write expansion to builder
	transformed := strings.Join(parts, ".")
	_, err := domain.WriteString(transformed)
	if err != nil {
		return 0, err
	}

	// read '}'
	if j >= len(ds) {
		return 0, newSPFError("incomplete macro", resultPermerror)
	}

	if ds[j] != '}' {
		return 0, newSPFError("incomplete macro", resultPermerror)
	}

	j++
	return j, err
}

func ipV6Dotted(ip net.IP) string {
	// this means FULL address in dot format
	// so ipv4 is 32 bits = 4 octets, each of which can be up to 3 chars in text rep
	// ipv6 is 128 bits = 8 hextets, each of which is 3 hex octets or 4 chars in text rep
	// TODO: write test fdda:5cc1:23:4::1f
	// becomes f.1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.4.0.0.0.3.2.0.0.1.c.c.5.a.d.d.f (prob reverse)
	// as use is for reverse pointer records in DNS, hence %{ir}

	// 32 chars w/ 31 '.' separators = 63
	const maxLen int = 63
	hexed := make([]byte, 32)
	hex.Encode(hexed, ip)
	dotted := make([]byte, maxLen)
	for i := 0; i < len(hexed); i++ {
		n := i * 2
		dotted[n] = hexed[i]
		if n+1 < maxLen {
			dotted[n+1] = '.'
		}
	}

	return string(dotted)
}

func isSubdomain(target, candidate string) bool {
	// is candidate a subdomain of target?
	// TODO: may need to deal w/ trailing '.'
	if len(candidate) <= len(target) {
		return false
	}

	if !strings.HasSuffix(candidate, target) {
		// is example.com in mail.example.com
		return false
	}

	// hello.world (11 - 5 - 1 = 6 = )
	if candidate[len(candidate)-len(target)-1] != '.' {
		// suffix, but not subdomain, e.g. example.com vs bad-example.com
		return false
	}

	return true
}

func isDelim(r rune) bool {
	for _, d := range macroDelims {
		if d == r {
			return true
		}
	}

	return false
}

func isASCIIVisible(r rune) bool {
	// ASCII printable %x21-24 / %x26-7E
	// Does NOT include space
	return ascii.IsPrintableChar(r) && r != ' ' && r != '%'
}
