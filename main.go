package main

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/ambarry/mail/cql"
	"gitlab.com/ambarry/mail/mime"
	"gitlab.com/ambarry/mail/msg"
	"gitlab.com/ambarry/mail/smtp"
	"gitlab.com/ambarry/mail/web"
	"golang.org/x/crypto/acme/autocert"
)

func main() {
	// TODO: at this point, config file...
	var (
		host      = flag.String("host", "0.0.0.0", "host address")
		domain    = flag.String("domain", "whom.email", "greeter domain")
		webDomain = flag.String("web-domain", "test.whom.email", "additional web domain to allowlist for let's encrypt")
		smtpPort  = flag.Int("smtp", 25, "smtp port")
		webPort   = flag.Int("web", 80, "web server port")
		dkimKey   = flag.String("dkim", "", "path to DKIM private key")
		selector  = flag.String("sel", "", "DKIM selector")
		allowList = flag.String("allowlist", "", "allowlist file of acceptable email addrs")
		useTLS    = flag.Bool("tls", false, "Use TLS for SMTP, will start Let's Encrypt server")
		localTLS  = flag.Bool("local-tls", false, "Use TLS w/ localhost and self-signed cert for development")
		certs     = flag.String("certs", "certs", "certs cache dir")
		db        = flag.String("db", "fs", "database type: one of fs, cql")
	)

	flag.Parse()

	mime.RegisterTypes("mime.types")

	mdb := loadDB(*db)
	tlsMode, cfg := loadTLS(*useTLS, *localTLS, *certs, *domain, *webDomain)
	carry := startCarrier(*dkimKey, *selector, *domain, *allowList, *useTLS, mdb)

	// SMTP server
	addr := fmt.Sprintf("%s:%d", *host, *smtpPort)
	srv, err := smtp.NewServer(addr, cfg, carry)
	if err != nil {
		log.Fatalln("error starting SMTP server: ", err)
	}
	defer srv.Close()
	go func() {
		// TODO: quit channel of some sort...
		// blocking
		log.Fatalln(srv.Listen())
	}()

	// web server
	mux := web.NewMux(carry)
	var webSrv *http.Server

	if tlsMode {
		webSrv = makeTLSServer(mux, cfg)
	} else {
		webSrv = web.NewServer(*webPort, mux)
	}

	log.Println(web.Serve(webSrv, tlsMode)) // blocking
	log.Println("done!")
}

func startCarrier(dkimKey, selector, domain, allowList string, useTLS bool, mdb msg.MessageDB) *smtp.Carrier {
	stop := make(chan bool)
	key := loadDKIM(dkimKey, selector)
	carry := smtp.NewCarrier(
		smtp.WithDomain(domain),
		smtp.WithStorage(mdb),
		smtp.WithDKIM(key, selector),
		smtp.WithAllowList(loadAllowList(allowList)),
		smtp.UseTLS(useTLS))

	carry.StartProcessing(stop)
	return carry
}

func loadDB(db string) msg.MessageDB {
	var mdb msg.MessageDB
	switch db {
	case "fs":
		mdb = msg.NewFSMessageDB("message-store")
	case "cql":
		// TODO: cassandra ips
		mdb = cql.NewMessageDB("localhost")
	default:
		log.Fatalf("Unknown database provider: %s\n", db)
	}

	err := mdb.Init()
	if err != nil {
		log.Fatalf("Cannot init message DB: %v\n", err)
	}

	log.Printf("Succesfully initialized db of type: %s\n", db)

	return mdb
}

func loadTLS(useTLS, localTLS bool, certs, domain, webDomain string) (bool, *tls.Config) {
	if useTLS && localTLS {
		log.Fatalln("Cannot run with both tls and localTLS options")
	}

	// TLS and Let's Encrypt setup
	// NOTE: a tls.Config may be reused, but NOT modified
	var cfg *tls.Config
	if useTLS {
		mgr := makeLetsEncryptManager(certs, domain, webDomain)
		cfg = mgr.TLSConfig()
		//startChallengeServer(mgr) // TODO: don't believe we need, think tls-alpin-01 or whatever can handle?
	} else if localTLS {
		cfg = loadLocalTLS(certs)
	}

	return cfg != nil, cfg
}

func loadAllowList(path string) []string {
	// load allowList
	var wl []string
	if path != "" {
		// line separated
		wlBytes, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatalf("Error reading allowList file: %v\n", err)
		}

		s := string(wlBytes)
		wl = strings.Split(s, "\n")
	} else {
		// TODO: eventually remove...
		wl = append(wl, "alexanderbarry.dev@gmail.com")
	}

	return wl
}

func loadDKIM(keyPath, selector string) *rsa.PrivateKey {
	// DKIM key
	var key *rsa.PrivateKey
	if keyPath == "" {
		return key
	}

	if selector == "" {
		log.Fatalln("Selector required when using DKIM")
	}

	// create with, for example, ssh-keygen -t RSA -m pem -f <whatever>
	// to convert the public key:
	// ssh-keygen -f <whatever.pub> -e -m pem > whatever.pub.pem
	bytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		log.Fatalf("Error reading key file: %v\n", err)
	}

	// DKIM must be PKCS#1 version 1.5 as per RFC 6376 3.3.2
	pem, _ := pem.Decode(bytes)
	key, err = x509.ParsePKCS1PrivateKey(pem.Bytes)

	if err != nil {
		log.Fatalf("Error parseing key file: %v\n", err)
	}

	return key
}

func makeLetsEncryptManager(certs string, domains ...string) *autocert.Manager {
	// Let's Encrypt cert manager
	var hosts []string
	for _, d := range domains {
		if d != "" {
			hosts = append(hosts, d)
		}
	}
	mgr := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		Cache:      autocert.DirCache(certs),
		HostPolicy: autocert.HostWhitelist(hosts...), // our domains
	}

	return &mgr
}

func startChallengeServer(mgr *autocert.Manager) {
	// start challenge server on port 80
	// TODO: it is not clear we need this...
	challenge := mgr.HTTPHandler(nil)
	srv := http.Server{Addr: ":80", Handler: challenge}
	go func() {
		log.Println("Starting challenge server on port 80")
		log.Fatalln(srv.ListenAndServe())
	}()
}

func makeTLSServer(mux http.Handler, cfg *tls.Config) *http.Server {
	log.Println("Using TLS option on 443, ignoring supplied web port if any")
	tlsServer := http.Server{
		Addr:      ":443",
		TLSConfig: cfg,
		Handler:   mux,
	}
	return &tlsServer
}

func loadLocalTLS(certsCache string) *tls.Config {
	// TODO: could optimize and parse leaf, but this is local dev...
	crt := filepath.Join(certsCache, "localhost.crt")
	key := filepath.Join(certsCache, "localhost.key")
	cert, err := tls.LoadX509KeyPair(crt, key)
	if err != nil {
		log.Fatalf("Could not load local certs: %v\n", err)
	}

	var cfg = tls.Config{
		ServerName:   "localhost",
		MinVersion:   tls.VersionTLS11,
		Certificates: []tls.Certificate{cert},
	}

	return &cfg
}
