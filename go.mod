module gitlab.com/ambarry/mail

go 1.13

require (
	github.com/gocql/gocql v0.0.0-20201215165327-e49edf966d90
	github.com/google/uuid v1.1.2
	github.com/scylladb/gocqlx/v2 v2.3.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
)
