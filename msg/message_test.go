package msg

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetBody(t *testing.T) {
	tests := []struct {
		content  []string
		expected []string
	}{
		{nil, nil},
		{[]string{}, nil},
		{[]string{""}, nil},
		{[]string{"A: B   "}, nil},
		{[]string{"A:B", "C:D", ""}, nil},
		{[]string{"A:B", ""}, nil},
		{[]string{"A:B", "C:D", "", "Hello", "Yes"}, []string{"Hello", "Yes"}},
		{[]string{"", "", ""}, []string{"", ""}},
	}

	for i, test := range tests {
		msg := Message{Content: test.content}
		body := msg.Body()
		assert.Lenf(t, body, len(test.expected), "expected body length")

		for j := range test.expected {
			assert.Equalf(t, test.expected[j], body[j], "test %d, body differs at index %d", i, j)
		}
	}
}
