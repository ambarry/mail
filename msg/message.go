package msg

import (
	"regexp"
	"strings"
	"time"
)

// TODO: we should use net/mail and store as a mail.Message

// CRLF is a carriage return + line feed
const CRLF = "\r\n"

// TODO: move this somewhere more useful
// easy usage in reducing inner spaces
var spaceConslidationRegex = regexp.MustCompile("[ ]{2,}")

// ConsolidateSpaces consolidates all spaces into single spaces
func ConsolidateSpaces(s string) string {
	return spaceConslidationRegex.ReplaceAllLiteralString(s, " ")
}

// Status of messages
const (
	StatusDraft         = 0
	StatusProcessing    = 1
	StatusSuccess       = 2
	StatusTemporaryFail = 3
	StatusFail          = 4
)

// Message contains everything for sending / receving an internet message.
// This includes the message content, as per RFC 5322, and any envelope or meta
// information we need? Or keep that out?
// We can store additional data here for retry logic and error display.
// Do we need error / retry per ForPath? Probably...
// TODO: consider easier header dictionary?
type Message struct {
	Email    string    `json:"email"` // user email
	ID       string    `json:"id"`
	Sent     bool      `json:"sent"`
	Received bool      `json:"received"`
	Error    string    `json:"error"`
	Status   int       `json:"status"`
	Time     time.Time `json:"time"` // NOTE: cql wants ISO8601 string or int
	LastTry  time.Time `json:"lastTry"`

	ClientDomain string   `json:"clientDomain"`
	FromAddr     string   `json:"fromAddr"`
	ToAddrs      []string `json:"toAddrs"`
	Content      []string `json:"content"` // TODO: this? or []byte? separate headers?
}

// HeaderField contains a single header field (name:value) with possible whitespace and unfolding
// It ends in a CRLF, unless empty
type HeaderField string

// Body returns the message body, or nil
func (msg *Message) Body() []string {
	n := msg.getBodyIndex()
	if n < 0 {
		return nil
	}

	return msg.Content[n:]
}

// Header returns the header lines w/ given name.
// This lookup is case-insensitive, though will return the value as cased in the message.
// Instances are returned in top-down order.
// Note that this performs no unfolding - lines are returned as received.
// This also assumes Message is valid.
func (msg *Message) Header(name string) []HeaderField {
	name = strings.ToLower(name)
	var headers []HeaderField
	for i, line := range msg.Content {
		if line == "" {
			break // separator
		}

		j := strings.IndexByte(line, ':')
		if j < 0 {
			continue // should not be possible if Message is valid
		}

		if j < len(name) {
			continue // easy check for wrong header
		}

		// check for actual match, allowing for WSP between field name and colon
		ok := strings.ToLower(line[:len(name)]) == name
		if j > len(name) {
			for k := len(name); k < j; k++ {
				ok = isWSP(line[k])
				if !ok {
					break
				}
			}
		}

		if !ok {
			continue
		}

		// now we have it - check for any folded lines
		// remember, we've stripped the CRLF so we need to add it back
		hd := line + CRLF
		for z := i + 1; z < len(msg.Content); z++ {
			if msg.Content[z] == "" {
				break // separator
			}
			c := msg.Content[z][0]
			if isWSP(c) {
				hd += msg.Content[z] + CRLF
			} else {
				break // done finding the folded lines
			}
		}

		headers = append(headers, HeaderField(hd))
	}

	return headers
}

// PrependHeader inserts the new header at the top of the message
func (msg *Message) PrependHeader(header string) {
	header = strings.TrimSuffix(header, CRLF) // remember, our format has stripped these
	msg.Content = append(msg.Content, "")
	copy(msg.Content[1:], msg.Content)
	msg.Content[0] = header
}

func (msg *Message) getBodyIndex() int {
	n := -1
	for i, line := range msg.Content {
		if line == "" {
			if i < len(msg.Content)-1 {
				n = i + 1
			}
			break
		}
	}
	return n
}

func isWSP(c byte) bool {
	return c == ' ' || c == '\t'
}
