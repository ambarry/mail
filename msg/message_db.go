package msg

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/ambarry/mail/storage"
)

// MessageDB interface for message persistance
type MessageDB interface {
	Init() error
	Insert(msg Message) error
	Update(msg Message) error
	GetMessage(email, id string) (Message, error)
	GetAllMessages(email string) ([]Message, error)
}

// fsMessageDB is more of a sample / naive testing implementation
type fsMessageDB struct {
	storage.FSRepo
}

// NewFSMessageDB creates a new fsStorage instance
func NewFSMessageDB(dir string) MessageDB {
	fs := fsMessageDB{storage.FSRepo{Root: dir}}
	return &fs
}

// Insert stores a message.
// TODO: need to determine outgoing vs incoming, per user, etc.
// For outgoing, we should preserve so we can show "sent" mail, and notify if an error occurs.
// For incoming, just need to store (or queue to store, depending on arch) in the right place, make avail for POP or IMAP
func (fs *fsMessageDB) Insert(msg Message) error {
	if msg.ID == "" {
		return errors.New("storage id requried")
	}
	// just write the file - name can be timestamp recv UTC and a uuid, eventually user db, etc
	bytes, err := json.MarshalIndent(&msg, "", "  ") // might as well make it pretty, this is for testing
	if err != nil {
		return err
	}

	t := time.Now().UTC().Unix()
	name := fmt.Sprintf("%s_%d.json", msg.ID, t)

	// ensure subdir...
	return fs.Store(msg.Email, name, bytes)
}

// Update updates the message.
// For fs, this means overwriting.
func (fs *fsMessageDB) Update(msg Message) error {
	return fs.Insert(msg)
}

// Get gets a message file
func (fs *fsMessageDB) GetMessage(email, id string) (Message, error) {
	msg := Message{}
	b, err := fs.Get(email, id)
	if err != nil {
		return msg, err
	}

	err = json.Unmarshal(b, &msg)
	return msg, err
}

// GetAll reads all message files
// TODO: may just want a header version...
// TODO: paging
func (fs *fsMessageDB) GetAllMessages(email string) ([]Message, error) {
	blobs, err := fs.GetAll(email)
	if err != nil {
		return nil, err
	}

	msgs := make([]Message, 0, len(blobs))
	for _, b := range blobs {
		msg := Message{}
		err := json.Unmarshal(b, &msg)
		if err != nil {
			return nil, err
		}

		msgs = append(msgs, msg)
	}

	return msgs, err
}
