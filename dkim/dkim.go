package dkim

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"hash"
	"io/ioutil"
	"net"
	"regexp"
	"strings"
	"time"

	"gitlab.com/ambarry/mail/msg"
)

var (
	tagSpec        *regexp.Regexp
	hyphenatedWord *regexp.Regexp
)

func init() {
	// setup regexs for parsing
	const fws string = "(?:[ \t]*\r\n)*[ \t]+"
	const optFWS string = "(?:" + fws + ")*"
	const tagNameR string = "([a-zA-Z][a-zA-Z0-9_]*)"
	const tval string = "[\x21-\x3A\x3C-\x7E]+" // at least 1 VALCHAR

	// build regex for an individual tag spec
	spc := fmt.Sprintf("(?:[ \t]|(?:%s))+", fws)
	tagValR := fmt.Sprintf("(%s(?:%s%s)*)", tval, spc, tval)
	ts := "^" + optFWS + tagNameR + optFWS + "=" + optFWS + tagValR + optFWS + "$"
	tagSpec = regexp.MustCompile(ts)

	// hyphenated-word =  ALPHA [ *(ALPHA / DIGIT / "-") (ALPHA / DIGIT) ]
	hyphenatedWord = regexp.MustCompile("^[a-zA-Z]([[a-zA-Z0-9-]*[a-zA-Z0-9])*$")
}

// example:
// "DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;";
// "        d=gmail.com; s=20161025;";
// "        h=mime-version:from:date:message-id:subject:to;";
// "        bh=ktrUNIAV0FgZV4QaPfi6QWDXVOYl38wuHz3g4Ml1ebA=;";
// "        b=WStEvNkI6KuC3nQ7Y2aKv1T5e4PPilkegFQeNPMpdmA7wEfrLcaIBHaVrfP1DRD4Uv";
// "         o368MQPQf9ozqtDppbPTP6qiy5lOzxMGp/OtTxEDGDvttClCMMu1CbYPZJG9TUPXa5v2";
// "         67AFrRJMxRtQLbvVu0bpKhiWQ4hDYmK8HRlAOMlGmMVbs/qK6YxTXC4BxzJY55ysRWGs";
// "         fcifyftNiQbWflSivVfs74Tkv4iKYrlv0qAjBbErgwIgZw2CMaeADUjFCR4BpaXsB7Zc";
// "         N2TXZjcptIt+GmCK1t/4vsDvYO03lud/uIuF25AAaeTLSySthd5at9uuWT0i6eHT6GDD";
// "         7xHA==";

// Algorithm constants
const (
	RSA_SHA256 = "rsa-sha256"
	RSA_SHA1   = "rsa-sha1"
)

// Canonicalization types
const (
	CanonRelaxed = "relaxed"
	CanonSimple  = "simple"
)

// Key represents DKIM key informationas retrieved from DNS
type Key struct {
	Version      string   // v= RECOMMENDED, must be DKIM1 or not present
	HashAlgos    []string // h= OPTIONAL, though we want sha256
	KeyType      string   // k= OPTIONAL, rsa expected
	Notes        string   // n= OPTIONAL, not for programmatic use
	PublicKey    string   // p= REQUIRED base64 encoded
	ServiceTypes []string // s= OPTIONAL, defaults to *, could use email, more for future use
	Flags        []string // t= OPTIONAL flags, y (testing) or s (any i= in header must not use subdomain)
}

// NewKey creates a new DKIM key with defaults as specified by RFC 6376
func NewKey() Key {
	s := Key{
		Version:   "DKIM1",
		HashAlgos: []string{"sha256", "sha1"},
		KeyType:   "rsa",
	}
	return s
}

// GenerateRRString is a utility function for creating the TXT RR
func GenerateRRString(path string) (string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	block, _ := pem.Decode(data)
	if block == nil {
		return "", errors.New("empty block")
	}

	// check it
	_, err = x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		return "", err
	}

	p := base64.StdEncoding.EncodeToString(block.Bytes)
	s := "v=DKIM1; k=rsa; p=" + p
	return s, nil
}

// Signature has info needed to produce a DKIM signature
// for a Message
type Signature struct {
	Version    string          // v must be "1"
	Algo       string          // a
	CanonHead  string          // c header/body
	CanonBody  string          // c
	Signature  string          // b
	BodyHash   string          // bh
	Domain     string          // d, or the SDID
	Selector   string          // s
	Length     int             // l
	Timestamp  time.Time       // t formatted in UTC unix timestamp, e.g. seconds since 1970
	Headers    []string        // h colon separated string, case-insensitive though order matters
	PrivateKey *rsa.PrivateKey // used to sign
	PublicKey  *rsa.PublicKey  // used to verify
}

// NewSignature setups up a new DKIMSignature w/ our preferred values
func NewSignature(domain, selector string, privateKey *rsa.PrivateKey) *Signature {
	sig := Signature{
		Version:    "1",
		Algo:       RSA_SHA256,
		CanonHead:  CanonRelaxed,
		CanonBody:  CanonRelaxed,
		Domain:     domain,
		Selector:   selector,
		Headers:    []string{"from", "to", "date", "subject", "message-id"}, // TODO: include reply-to if present?
		Length:     -1,
		PrivateKey: privateKey, // -1 will indicate NoLength
	}
	return &sig
}

// Header form of Signature as required by RFC 6376
// Note there is NO trailing CRLF
func (sig *Signature) AsHeader() (string, error) {
	bld := &strings.Builder{}

	_, err := fmt.Fprintf(bld, "DKIM-Signature: v=%s; a=%s; c=%s/%s;"+msg.CRLF, sig.Version, sig.Algo, sig.CanonHead, sig.CanonBody)
	if err != nil {
		return "", err
	}

	_, err = fmt.Fprintf(bld, "    d=%s; s=%s; t=%d;"+msg.CRLF, sig.Domain, sig.Selector, sig.Timestamp.Unix())
	if err != nil {
		return "", err
	}

	_, err = fmt.Fprintf(bld, "    h=%s;"+msg.CRLF, strings.Join(sig.Headers, ":"))
	if err != nil {
		return "", err
	}

	_, err = fmt.Fprintf(bld, "    bh=%s;"+msg.CRLF, sig.BodyHash)
	if err != nil {
		return "", err
	}

	// optional length
	if sig.Length >= 0 {
		_, err = fmt.Fprintf(bld, "    l=%d;"+msg.CRLF, sig.Length)
		if err != nil {
			return "", err
		}
	}

	// Break up data hash w/ FWS to conform to 78 - leading space
	// first line is 78-6 (4 spaces + b=) = 72
	// really, we can just indexnt extra 2 spaces for other lines so chunks of 72 work
	var i int
	bld.WriteString("    b=")
	for i = 0; i < len(sig.Signature)-72; i += 72 {
		if i > 0 {
			bld.WriteString(msg.CRLF + "      ")
		}

		bld.WriteString(sig.Signature[i : i+72])
	}

	// Add remainder
	// NOTE: no trailing CRLF, this is intentional as it is supplied without trailing CRLF to signing,
	// though obviously we'll need one when prepending onto Message headers
	if i > 0 {
		bld.WriteString(msg.CRLF + "      ")
	}
	bld.WriteString(sig.Signature[i:]) // ok even if 0, just empty
	return bld.String(), nil
}

// Sign generates our DKIM signature.
// You can then access the full string through Header
func (sig *Signature) Sign(m *msg.Message) error {
	bodyHash, headerHash, cryptoHash, err := sig.checkAlgo()
	if err != nil {
		return err
	}

	body, err := sig.getCanonBody(m)
	if err != nil {
		return err
	}

	bh, err := sig.hashBody(body, bodyHash)
	if err != nil {
		return err
	}

	sig.BodyHash = bh
	sig.Timestamp = time.Now().UTC()

	canonHds, err := sig.getCanonHeaders(m)
	if err != nil {
		return err
	}

	// hash them!
	for _, hd := range canonHds {
		_, err := headerHash.Write([]byte(hd))
		if err != nil {
			return err
		}
	}

	// canon the dkim sig hash
	dkim, err := sig.AsHeader()
	if err != nil {
		return err
	}

	if sig.CanonHead == CanonRelaxed {
		dkim, err = relaxedHeader(msg.HeaderField(dkim)) // TODO: get rid of sub type alias...
		if err != nil {
			return err
		}
		dkim = dkim[:len(dkim)-2] // remove trailing CRLF that we added
	}

	// hash our dkim header too
	_, err = headerHash.Write([]byte(dkim))
	if err != nil {
		return err
	}

	// get the final data hash and sign it
	dataHash := headerHash.Sum(nil)
	bytes, err := rsa.SignPKCS1v15(rand.Reader, sig.PrivateKey, cryptoHash, dataHash)
	if err != nil {
		return err
	}

	// base64 encode the signature
	b := base64.StdEncoding.EncodeToString(bytes)
	sig.Signature = b
	return nil
}

func (sig *Signature) checkAlgo() (hash.Hash, hash.Hash, crypto.Hash, error) {
	var bodyHash hash.Hash
	var headerHash hash.Hash
	var cryptoHash crypto.Hash
	switch sig.Algo {
	case RSA_SHA256:
		bodyHash = sha256.New()
		headerHash = sha256.New()
		cryptoHash = crypto.SHA256
	case RSA_SHA1:
		bodyHash = sha1.New()
		headerHash = sha1.New()
		cryptoHash = crypto.SHA1
	default:
		return nil, nil, crypto.Hash(0), errors.New("unrecognized hash algorithm")
	}

	return bodyHash, headerHash, cryptoHash, nil
}

func (sig *Signature) getCanonHeaders(m *msg.Message) ([]string, error) {
	// NOTE: assumes headers are consistently cased in the sig field at least...
	// TODO: make sure that's true when parsing...
	canonHds := make([]string, 0, len(sig.Headers)) // no, not the camera
	hdFields := make(map[string][]msg.HeaderField)
	rel := sig.CanonHead == CanonRelaxed
	from := false
	for _, hd := range sig.Headers {
		hfs, ok := hdFields[hd]
		if !ok {
			hfs = m.Header(hd)
		}

		// Signers can include headers that don't exist yet, which must be treated as the null string
		// including all whitespace, CRLF, etc.
		if hfs == nil || len(hfs) == 0 {
			hdFields[hd] = nil
			continue
		}

		// If header appears multiple times in the list, we expect multiple in actual headers
		// We apply in bottom up order
		// Not clear if more than exist, but treating like any header that doesn't exist
		hf := hfs[len(hfs)-1]
		hdFields[hd] = hfs[:len(hfs)-1]
		var canonHd string
		var err error
		if rel {
			canonHd, err = relaxedHeader(hf)
		} else {
			// simple mode - as is
			canonHd = string(hf)
		}

		if err != nil {
			return nil, err
		}

		canonHds = append(canonHds, canonHd)
		if hd == "from" {
			from = true
		}
	}

	if !from {
		return nil, errors.New("missing from header")
	}

	return canonHds, nil
}

func (sig *Signature) getCanonBody(msg *msg.Message) (string, error) {
	var s string
	switch sig.CanonBody {
	case "relaxed":
		s = relaxedBody(msg)
	case "simple":
		s = simpleBody(msg)
	default:
		return "", errors.New("unrecognized canonicalization method")
	}

	// TODO: consider if we want to allow - basically lets you modify
	if sig.Length == 0 {
		return "", errors.New("length of 0 is not accepted")
	}

	if sig.Length < 0 {
		// our representation of not including length
		return s, nil
	}

	if sig.Length > len(s) {
		return "", errors.New("specfied length exceeds body size")
	}

	s = s[:sig.Length]
	return s, nil
}

func (sig *Signature) hashBody(body string, bodyHash hash.Hash) (string, error) {
	// hash
	_, err := bodyHash.Write([]byte(body))
	if err != nil {
		return "", err
	}

	// base64 encode
	bytes := bodyHash.Sum(nil)
	bh := base64.StdEncoding.EncodeToString(bytes)
	return bh, nil
}

func DKIMVerify(res *net.Resolver, msg msg.Message) error {
	// TODO:! parse the sig, verify
	//pub := findKey(res, )

	// NOTE; we can then enfore rsa or avoid unknown types, check 1024 chars, etc.

	return nil
}

func simpleBody(m *msg.Message) string {
	// completely empty or missing body returns just CRLF
	body := m.Body()
	if body == nil {
		return msg.CRLF
	}

	// reduce any empty lines at end to 1 (which were CRLFs over the wire)
	var j int
	for j = len(body) - 1; j >= 0; j-- {
		if body[j] != "" {
			break
		}
	}

	// empty message, all CRLFs
	if j < 0 {
		return msg.CRLF
	}

	if j == len(body)-1 {
		// insert trailing CRLF placeholder
		body = append(body, "")
	} else {
		// backup to single trailing CRLF (need extra 1 for range syntax, exclusive)
		body = body[:j+2]
	}

	s := strings.Join(body, msg.CRLF)
	return s
}

func relaxedBody(m *msg.Message) string {
	// get into "network normal" format (text is ASCII encoded, lines are separated with CRLF)
	// reduce whitespace: remove all trailing ws except CRLFs
	// reduce all WSP within a line to a single SP character (including tabs I guess)
	// a non-empty body gets CRLF added, but an empty body should return empty string
	body := m.Body()
	if body == nil {
		return ""
	}

	reduced := make([]string, 0, len(body))

	// 1. Reduce whitespace:
	// - remove trailing whitespace (remember, we've removed the CRLFs at this point)
	// - reduce innser WSP sequencues to a single SP
	for _, line := range body {
		line = strings.ReplaceAll(line, "\t", " ")
		line = strings.TrimRight(line, " ")
		line = msg.ConsolidateSpaces(line)
		reduced = append(reduced, line)
	}

	// 2. Remove empty lines at end
	// - if the body is non-empty but does not end w/ CRLF, CRLF is added.
	// - if the body is empty, keep it empty!
	// (this behavior is different from simple, which adds CRLF to empty body)
	// This understanding was verified against the hashes in RFC 6376
	var j int
	for j = len(reduced) - 1; j >= 0; j-- {
		if reduced[j] != "" {
			break
		}
	}

	if j < 0 {
		// empty!
		return ""
	}

	if j == len(reduced)-1 {
		// append CRLF placeholder
		reduced = append(reduced, "")
	} else {
		reduced = reduced[:j+2]
	}

	s := strings.Join(reduced, msg.CRLF)
	return s
}

// relaxedHeader will:
// 1. convert header name to lowercase
// 2. unfold but keep CRLF at end of val
// 3. reduce inner val WSP to single SP
// 4. remove trailing WSP
// 5. remove WSP around colon
func relaxedHeader(hd msg.HeaderField) (string, error) {
	s := strings.ReplaceAll(string(hd), "\t", " ")
	parts := strings.SplitN(s, ":", 2)
	if len(parts) < 2 {
		return "", errors.New("invalid header")
	}

	nm := strings.Trim(parts[0], " ")
	nm = strings.ToLower(nm)

	// unfold the val
	val := strings.ReplaceAll(parts[1], msg.CRLF, "")

	// val must reduce inner WSP to single SP and have ends trimmed
	val = strings.Trim(val, " ")
	val = msg.ConsolidateSpaces(val)

	// no space around colon (and we've trimmed)
	header := fmt.Sprintf("%s:%s"+msg.CRLF, nm, val)
	return header, nil
}

func findKey(res *net.Resolver, q, d, s string) (Key, error) {
	// NOTE that q is the query time, and only currently acceptable value is TXT RR, e.g. "dns/txt"
	name := fmt.Sprintf("%s._domainkey.%s", s, d)
	txts, err := res.LookupTXT(context.Background(), name)
	if err != nil {
		return Key{}, err
	}

	if len(txts) == 0 {
		return Key{}, fmt.Errorf("no TXT record found for %s", name)
	}

	for _, txt := range txts {
		// Check RRs then break up the keys according to RFC 6376
		// if v, must be first tag, and must be DKIM1, otherwise throw out the record!
		ok, sig := checkTXT(txt)
		if ok {
			return sig, nil
		}
	}

	return Key{}, fmt.Errorf("no valid DKIM key in TXT record for %s", name)
}

func checkTXT(txt string) (bool, Key) {
	// must concatenate all WS
	// whitespace around '=' doesn't matter
	// whitespace in values is signifcant
	// v is first if present and MUST be DKIM1
	// other tags are permitted and ignored
	ok := false
	key := NewKey()
	tagFields := strings.Split(txt, ";")
	if len(tagFields) == 0 {
		return ok, key
	}

	var tagSpec []string
	var tags []string
	for i, tf := range tagFields {
		// check tag parse
		ok, tagSpec = parseTag(tf)
		if !ok {
			break
		}

		// duplicate tags invalidate the entire tag list
		for _, t := range tags {
			if tagSpec[0] == t {
				ok = false
				break
			}
		}

		// v must be first tag if present
		if tagSpec[0] == "v" && i > 0 {
			ok = false
			break
		}

		// check valid value for tag
		ok = updateKey(tagSpec, &key)
		if !ok {
			break
		}

		tags = append(tags, tagSpec[0])
	}

	// make sure we parsed a public key
	// TODO: we can enforce length in next step, min 1024 (decoded?)
	ok = key.PublicKey != ""
	return ok, key
}

func parseTag(tagField string) (bool, []string) {
	matches := tagSpec.FindStringSubmatch(tagField)

	// NOTE: match includes whole text in 0
	if matches == nil || len(matches) != 3 {
		return false, nil
	}

	return true, matches[1:]
}

// updateKey updates the key based on the tag, and
// returns a bool whether we have an error and must stop.
func updateKey(tagSpec []string, key *Key) bool {
	switch tagSpec[0] {
	case "v":
		key.Version = tagSpec[1]
		return key.Version == "DKIM1"
	case "h":
		// value is a colon separated list.
		// we must support sha265 and sha1, though hyphen word may be acceptable for future algos
		algos := strings.Split(tagSpec[1], ":")
		for _, algo := range algos {
			ok, algo := trimFWS(algo)
			if !ok {
				return false
			}
			if algo != "sha265" && algo != "sha1" && !hyphenatedWord.MatchString(algo) {
				// invalid algo
				return false
			}
			key.HashAlgos = append(key.HashAlgos, algo)
		}
	case "k":
		ok, keyType := trimFWS(tagSpec[1])
		if !ok {
			return false
		}
		if keyType != "rsa" && !hyphenatedWord.MatchString(keyType) {
			return false
		}
		key.KeyType = keyType
	case "n":
		// we don't really care
		key.Notes = tagSpec[1]
	case "p":
		// this should be a base64 encoded string
		// it's allowed to have arbitrary FWS, but all CRLFs must be followed by a SP
		// TODO:
		key.PublicKey = tagSpec[1]
	case "s":
		// value is a colon separated list.
		// we expect * or email, though extensible
		svcs := strings.Split(tagSpec[1], ":")
		for _, svc := range svcs {
			ok, svc := trimFWS(svc)
			if !ok {
				return false
			}
			if svc != "*" && svc != "email" && !hyphenatedWord.MatchString(svc) {
				// invalid service
				return false
			}
			key.ServiceTypes = append(key.ServiceTypes, svc)
		}
	case "t":
		flags := strings.Split(tagSpec[1], ":")
		for _, flag := range flags {
			ok, flag := trimFWS(flag)
			if !ok {
				return false
			}
			if flag != "y" && flag != "s" && !hyphenatedWord.MatchString(flag) {
				// invalid flag
				return false
			}
			key.Flags = append(key.Flags, flag)
		}
	default:
		// unknown tags are simply ignored
		return true
	}

	return true
}

func trimFWSStart(s string) (bool, string) {
	var ok, lf, car, done bool
	ok = true
	var i int
	for i = 0; i < len(s); i++ {
		if car {
			// CR must be followed by LF
			ok = s[i] == '\n'
			car = false
			lf = true
		} else if lf {
			// LF must be followed by SP
			ok = s[i] == ' '
			lf = false
		} else {
			switch s[i] {
			case '\r':
				car = true
			case '\n':
				// unexpected LF before CR
				ok = false
			case ' ', '\t':
				// still space
			default:
				// not a space
				done = true
			}
		}

		if !ok || done {
			break
		}
	}

	if !ok {
		return ok, ""
	}

	return ok, s[i:]
}

func trimFWSEnd(s string) (bool, string) {
	var ok, lf, sp, done bool
	ok = true
	var j int
	for j = len(s) - 1; j >= 0; j-- {
		// LF must be preceeded by CR
		if lf {
			ok = s[j] == '\r'
			lf = false
		} else {
			switch s[j] {
			case '\r':
				ok = false
			case ' ', '\t':
				sp = true
			case '\n':
				// LF must be followed by at least 1 SP
				ok = sp
				sp = false
				lf = true
			default:
				done = true
			}
		}

		if !ok || done {
			break
		}
	}

	if !ok {
		return ok, ""
	}

	return ok, s[:j+1]
}

// trimFWS trims folding whitespace on either side of a string.
// It returns false if there is invalid whitespace
func trimFWS(s string) (bool, string) {
	// WSP =   SP / HTAB
	// LWSP =  *(WSP / CRLF WSP)
	// FWS =   [*WSP CRLF] 1*WSP
	ok, s := trimFWSStart(s)
	if !ok {
		return false, ""
	}

	return trimFWSEnd(s)
}
