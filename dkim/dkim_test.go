package dkim

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ambarry/mail/msg"
	"gitlab.com/ambarry/mail/testutil"
)

// TODO: test the regex parsing, then whole signature parsing

type tagFieldTest struct {
	text    string
	matches []string
}

func goodTagField(s string, matches []string) tagFieldTest {
	// for convenience, this prepends the whole text match
	allMatches := make([]string, len(matches)+1)
	allMatches[0] = s
	for i, m := range matches {
		allMatches[i+1] = m
	}
	tf := tagFieldTest{text: s, matches: allMatches}
	return tf
}

func badTagField(s string) tagFieldTest {
	tf := tagFieldTest{text: s}
	return tf
}

func TestTagField(t *testing.T) {
	tests := []tagFieldTest{
		goodTagField("test=maybe", []string{"test", "maybe"}),
		goodTagField("a= hello", []string{"a", "hello"}),
		goodTagField("b = bye-bye", []string{"b", "bye-bye"}),
		goodTagField("a=with inner spaces", []string{"a", "with inner spaces"}),
		goodTagField("juniper= the most \r\n    beautiful cat\r\n   in the world   ",
			[]string{"juniper", "the most \r\n    beautiful cat\r\n   in the world"}),

		badTagField(" spacey field=value"),
		badTagField("a=\r\nBadFWS"),
		badTagField("a=BadFWS\r\n"),
		badTagField("a=\n BadFWS"),
		badTagField("=missingfield"),
		badTagField("a="),
		badTagField("aMissingEquals"),
	}

	for _, test := range tests {
		matches := tagSpec.FindStringSubmatch(test.text)
		if test.matches == nil {
			assert.Nilf(t, matches, "expected nil match, got %v", matches)
			continue
		}

		assert.Lenf(t, len(matches), 3,
			"for %s, expected %d matches, got %d",
			test.text,
			3,
			len(matches))
		for i, m := range matches {
			tm := test.matches[i]
			assert.Equal(t, m, tm, "expected match")
		}
	}
}

func TestTrimFWS(t *testing.T) {
	tests := []struct {
		s  string
		e  string
		ok bool
	}{
		{"sha256", "sha256", true},
		{" sha256", "sha256", true},
		{"sha256    ", "sha256", true},
		{" \r\n sha256 \r\n ", "sha256", true},
		{"\rsha256", "", false},
		{" \r\nsha256", "", false},
		{" some-super-Hyphenated-future-algo  \r\n  ", "some-super-Hyphenated-future-algo", true},
		{"space inside", "space inside", true},
		{"   ", "", true},
		{"testtest\r\n ", "testtest", true},
		{"testtest\r\n", "", false},
		{"test\n ", "", false},
		{" a ", "a", true},
		{" \tsomething\t\t\t \r\n\t", "something", true},
	}

	for _, test := range tests {
		ok, a := trimFWS(test.s)
		assert.Equalf(t, test.ok, ok, "for %s, expected ok to be %v, got %v",
			testutil.EscapeNewLines(test.s), test.ok, ok)
		if test.ok {
			assert.Equalf(t, test.e, a, "expected %s, got %s", test.e, a)
		}
	}
}

func TestRelaxedHeader(t *testing.T) {
	tests := []struct {
		text     string
		expected string
		ok       bool
	}{
		{"subject:hello\r\n", "subject:hello\r\n", true},
		{"Subject: Going to the Market\r\n", "subject:Going to the Market\r\n", true},
		{"Subject: Going to    \t the Market    \t\r\n", "subject:Going to the Market\r\n", true},
		{"Message-Id   \t  :\t alpha-beta-gamma-1\r\n    charlie\r\n", "message-id:alpha-beta-gamma-1 charlie\r\n", true},
		{"Space Header: Yes\r\n", "space header:Yes\r\n", true}, // not sure this is really a valid header though...
		{"missing colon\r\n", "", false},
	}

	for _, test := range tests {
		hd, err := relaxedHeader(msg.HeaderField(test.text))
		if test.ok {
			assert.Nil(t, err)
			assert.Equalf(t, test.expected, hd, "for %s, expected %s, got %s",
				test.text, test.expected, hd)
		} else {
			assert.NotNil(t, err)
		}
	}
}

func TestSimpleBody(t *testing.T) {
	// simple just reduces the empty lines at end
	// an empty should return CRLF still
	tests := []struct {
		content  []string
		expected string
	}{
		{nil, "\r\n"},
		{[]string{}, "\r\n"},
		{[]string{""}, "\r\n"},
		{[]string{"A: B   "}, "\r\n"},
		{[]string{"A:B", "C:D", ""}, "\r\n"},
		{[]string{"A:B", ""}, "\r\n"},
		{
			[]string{"A:B", "", "Dear Alex,", "Stuff and things.", "Yours,", "", "", "Alex"},
			"Dear Alex,\r\nStuff and things.\r\nYours,\r\n\r\n\r\nAlex\r\n",
		},
		{
			[]string{"", "Hello good sir!"},
			"Hello good sir!\r\n",
		},
		{
			[]string{"", "Hello good sir!   "},
			"Hello good sir!   \r\n",
		},
		{
			[]string{"", "Hello good sir!", "   "},
			"Hello good sir!\r\n   \r\n",
		},
	}

	for i, test := range tests {
		msg := msg.Message{Content: test.content}
		b := simpleBody(&msg)

		assert.Equalf(t, test.expected, b, "test %d, expected '%s', got '%s'", i,
			testutil.EscapeNewLines(test.expected),
			testutil.EscapeNewLines(b))

		// now add some trailing ws lines
		// remember, in our format the CRLF has been stripped
		msg.Content = append(test.content, "", "", "")
		b = simpleBody(&msg)
		assert.Equalf(t, test.expected, b, "trailing test %d, expected '%s', got '%s'", i,
			testutil.EscapeNewLines(test.expected),
			testutil.EscapeNewLines(b))

	}
}

func TestRelaxedBody(t *testing.T) {
	// relaxed body returns empty string for empty body, not CRLF
	tests := []struct {
		content  []string
		expected string
	}{
		{nil, ""},
		{[]string{}, ""},
		{[]string{""}, ""},
		{[]string{"A: B   "}, ""},
		{[]string{"A:B", "C:D", ""}, ""},
		{[]string{"A:B", ""}, ""},
		{[]string{"A:B", "", ""}, ""},
		{
			[]string{"A:B", "", "Dear Alex,", "Stuff and things.", "Yours,", "", "", "Alex"},
			"Dear Alex,\r\nStuff and things.\r\nYours,\r\n\r\n\r\nAlex\r\n",
		},
		{
			[]string{"", "Hello good sir!"},
			"Hello good sir!\r\n",
		},
		{
			[]string{"", "Hello good sir!   "},
			"Hello good sir!\r\n",
		},
		{
			[]string{"", "Hello good sir!", "   "},
			"Hello good sir!\r\n", // trailing whitespace is removed, so the 2ns line is reduced out
		},
		{
			[]string{"", "Hello good sir!", "   Yes indeed.   ", "", " \t \t  ", ""},
			"Hello good sir!\r\n Yes indeed.\r\n",
		},
		{
			[]string{"", "Hello good sir!", "   Yes indeed.   ", "", " \tWhat?\t  ", ""},
			"Hello good sir!\r\n Yes indeed.\r\n\r\n What?\r\n",
		},
	}

	for i, test := range tests {
		msg := msg.Message{Content: test.content}
		b := relaxedBody(&msg)

		assert.Equalf(t, test.expected, b, "test %d, expected '%s', got '%s'", i,
			testutil.EscapeNewLines(test.expected),
			testutil.EscapeNewLines(b))

		// now add some trailing ws lines
		// remember, in our format the CRLF has been stripped
		msg.Content = append(test.content, "", "", "")
		b = relaxedBody(&msg)
		assert.Equalf(t, test.expected, b, "trailing test %d, expected '%s', got '%s'", i,
			testutil.EscapeNewLines(test.expected),
			testutil.EscapeNewLines(b))
	}
}
