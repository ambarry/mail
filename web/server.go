package web

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ambarry/mail/smtp"
)

func NewMux(carry *smtp.Carrier) *http.ServeMux {
	mux := http.NewServeMux()
	// this dir depends on where we're running from, assume top level for now
	mux.Handle("/", http.FileServer(http.Dir("./web/src")))
	mux.Handle("/email", Routes(carry))
	return mux
}

func NewServer(port int, mux *http.ServeMux) *http.Server {
	addr := fmt.Sprintf(":%d", port)
	srv := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	return &srv
}

func Serve(srv *http.Server, tls bool) error {
	p := "HTTP"
	if tls {
		p = "HTTPS"
	}
	log.Printf("Starting %s server on %s\n", p, srv.Addr)

	if tls {
		return srv.ListenAndServeTLS("", "") // key and cert coming from Let's Encrypt
	}

	return srv.ListenAndServe()
}
