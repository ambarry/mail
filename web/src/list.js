import store from './store.js'

function emailRow(msg) {
    // TODO: decide to display the meta fields or the content fields as supplied
    let hd = getHeaders(msg);
    return `<p class="emailRow" data-emailid="${msg.id}">[${hd.Date}] (${msg.status}) From: ${hd.From} | ${hd.Subject}</p>`;
}

function getHeaders(msg) {
    let hd = {};
    for (var i = 0; i < msg.content.length; i++) {
        if (msg.content[i] == "") {
            break;
        }

        // TODO: better HTML escape
        let clean = msg.content[i].replace("<", "&lt;").replace(">", "&gt;");
        var parts = clean.split(':');
        hd[parts[0]] = parts[1];
    }

    return hd;
}

// email list component, we don't need to get too fancy yet
export default class EmailList {
    constructor(sel, key) {
        let self = this;
        self.element = document.querySelector(sel);
        self.storeKey = key;
    }

    // TODO: get a list ref from a store?

    render() {
        let self = this;
        let items = store.state[self.storeKey];
        if (items.length == 0) {
            self.element.innerHTML = "";
            return;
        }

        // TODO: make child components or make pretty
        self.element.innerHTML = `
            <div>
                ${items.map(item => emailRow(item) + `<hr />`).join('')}
            </div>
        `;

        // add click for displaying / raising event to display it, maybe the id, etc.
        let rows = self.element.querySelectorAll(".emailRow");
        rows.forEach(r => r.addEventListener('click', e => {
            let eventClick = new CustomEvent('emailClicked', {
                bubbles: true,
                detail: { id: e.target.getAttribute('data-emailid') }
            });

            e.target.dispatchEvent(eventClick);
        }));
    }
}

