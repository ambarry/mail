import './api.js'
import store from './store.js'
import { getEmails, sendEmail } from './api.js';
import EmailList from './list.js';
import EmailDetail from './detail.js';

// Load up some DOM elements
const navLinks = document.querySelectorAll('.sidebar > a');
const panels = document.querySelectorAll('.sub-panel');
const form = document.querySelector('form');
const reloadBtn = document.querySelector("#reload");
const detailBody = document.querySelector("#emailDetail");

// Store state
store.state['recv'] = [];
store.state['sent'] = [];

// Wire up navigation panels
let panelMap = {};
let shown = "";
panels.forEach(pnl => {
    panelMap[pnl.getAttribute("id")] = pnl;
    pnl.setAttribute("hidden", "");
});

function navigate(id) {
    if (shown) {
        panelMap[shown].setAttribute("hidden", "");
    }

    panelMap[id].removeAttribute("hidden");
    shown = id;
}

navLinks.forEach(nl => {
    nl.addEventListener('click', evt => {
        navigate(nl.getAttribute('data-navid'));
    });
});

// Set up form submit
form.addEventListener('submit', evt => {
    evt.preventDefault();

    // get form fields
    let to = form.querySelector("#to").value.trim();
    let subject = form.querySelector("#subject").value.trim();
    let body = form.querySelector("#mail-body").value.trim();
    let password = form.querySelector("#password").value.trim();
    let incAttach = form.querySelector("#inc-attachment").checked;

    // check blanks
    if (!(to && subject && body && password)) {
        alert("All fields required!");
    } else {    
        sendEmail(to, subject, body, password, incAttach);
    }
});

// build render lists
let recvList = new EmailList('#recvList', 'recv');
let sentList = new EmailList('#sentList', 'sent');

// handle row clicks to display email
function viewDetail(e, key) {
    // TODO: build and swap the child
    let detail = new EmailDetail(detailBody, e.detail.id, key);
    detail.render();
    navigate('detail');
}

recvList.element.addEventListener('emailClicked', e => viewDetail(e, "recv"));
sentList.element.addEventListener('emailClicked', e => viewDetail(e, "sent"));

// Load emails callback
function load(emails) {
    let recv = store.state['recv'];
    let sent = store.state['sent'];
    recv.length = 0;
    sent.length = 0;
    if (emails) {        
        emails.forEach(msg => {
            if (msg.sent) {
                sent.push(msg);
            }

            // can be both in theory...
            if (msg.received) {
                recv.push(msg);
            }
        });
    }

    // update our lists (TODO: reactive)
    sentList.render();
    recvList.render();
}

// Wire up reload
reloadBtn.addEventListener('click', evt => {
    evt.preventDefault();
    getEmails(load);
});


// Set initial panel
navigate("inbox");

// Initial loading
getEmails(load);



