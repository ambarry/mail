import store from './store.js';

function emailAsText(msg) {
    let txt = msg.content.join("\n");
    return txt;
}

export default class EmailDetail {
    constructor(elm, id, key) {
        let self = this;
        self.element = elm;
        self.id = id;
        self.storeKey = key;
    }

    render() {
        let self = this;
        let items = store.state[self.storeKey];
        let msg = null;
        for (var i = 0; i < items.length; i++)
        {
            if (items[i].id == self.id) {
                msg = items[i];
                break;
            }
        }

        if (msg == null) {
            return;
        }

        self.element.innerText = emailAsText(msg);
    }
}