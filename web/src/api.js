
function getEmails(cb) {
    var req = new XMLHttpRequest();
    req.open("GET", "/email", true);
    req.onreadystatechange = function () {
        if (req.readyState == XMLHttpRequest.DONE) {
            console.log(req.response);
            cb(req.response);
        }
    };
    req.responseType = "json";
    req.send('');
}

function sendEmail(to, subject, body, password, incAttach) {
    console.log("sending email!");
    let json = JSON.stringify({
        "to": to,
        "subject": subject,
        "body": body,
        "password": password,
        "includeAttachment": incAttach,
    });

    // POST json
    var req = new XMLHttpRequest();
    req.open("POST", "/email", true);
    req.setRequestHeader("Content-type", "application/json");
    req.send(json);
}

export {
    getEmails,
    sendEmail
}