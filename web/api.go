package web

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ambarry/mail/msg"
	"gitlab.com/ambarry/mail/smtp"
)

// TODO: this should probably be it's own repo eventually, but get something simple going
// It's ok to live here, even if we have separate launch option?
// TODO: we need to make a draft of emails, and an upload route. The upload should check extension and scan, etc.

// Routes returns sub routes for the email api
func Routes(carry *smtp.Carrier) *http.ServeMux {
	mux := http.NewServeMux()
	emailHandler := handler{Carrier: carry}
	mux.Handle("/email", &emailHandler) // TODO: are we doing that right? double mux?
	return mux
}

type handler struct {
	Carrier *smtp.Carrier
}

type emailRequest struct {
	To                string
	Subject           string
	Body              string
	Password          string
	IncludeAttachment bool
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.getEmail(w, r)
	case "POST":
		h.postEmail(w, r)
	default:
		log.Println(r.Method)
		http.NotFound(w, r)
	}
}

func (h *handler) getEmail(w http.ResponseWriter, r *http.Request) {
	// TODO: users, etc.
	msgs, err := h.Carrier.MessageDB.GetAllMessages("test@whom.email")
	if err != nil {
		http.Error(w, "Unable to read storage", http.StatusInternalServerError)
	}

	// TODO: format to web message, not internal structure
	// TODO: order by time

	bytes, err := json.Marshal(msgs)
	if err != nil {
		http.Error(w, "Unable to format message", http.StatusInternalServerError)
	}

	// TODO: log or handle error?
	w.Write(bytes)
}

func (h *handler) postEmail(w http.ResponseWriter, r *http.Request) {
	// TODO: get our message body submit email for sending
	log.Println("Received post!")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, "Unable to ready body", http.StatusBadRequest)
		return
	}

	// NOTE: we don't need to close body, server will
	e := emailRequest{}
	err = json.Unmarshal(body, &e)
	if err != nil {
		log.Println(err)
		http.Error(w, "Unable to decode body", http.StatusBadRequest)
		return
	}

	if e.Password != "glorp" {
		log.Println("bad password")
		http.Error(w, "Nice try", http.StatusBadRequest)
		return
	}

	log.Println(e)

	// TODO: build a real smtp message (adding FROM, RECEIVE, to data, etc.)
	// TODO: valdate syntax for all fields
	m := msg.Message{Email: "test@whom.email"} // TODO: user account
	m.FromAddr = "test@whom.email"             // TODO: user account

	// TODO: validate syntax
	// Cc is it's own header
	// Bcc will be saved by us, set up in For paths, but NOT added to message headers,
	// just up to us to send
	m.ToAddrs = []string{e.To}

	// TODO: use PathParse here to validate!

	m.Time = time.Now().UTC()
	m.Sent = true

	// TODO: would lookup the to addr in address book, etc.

	//Date: Fri, 21 Nov 1997 09:55:06 -0600 using ref Mon Jan 2 15:04:05 -0700 MST 2006
	dt := m.Time.Local().Format("Mon, 2 Jan 2006 15:04:05 -0700") // TODO: local? const
	m.ID = uuid.New().String()

	if e.IncludeAttachment {
		// TODO: first have a test that doesn't need the full send, just make an email w/ config...
		// TODO: drafts, uploads, etc.
		// For now, hardcode a file and use in ComposeMessage for the body
		// Then save this. Then test the sending!
	}

	// TODO: DKIM-Signature
	// TODO: Received: (by us!)
	m.Content = []string{
		fmt.Sprintf("Message-Id: <%s@whom.email>", m.ID), // TODO: domain
		"Date: " + dt,
		"From: Testing <test@whom.email>",
		"Subject: " + e.Subject,
		"To: " + e.To,
		"Content-Type: " + "text/plain; charset=\"UTF-8\"", // TODO: allow html, multipart, etc.
		"",
	}

	// TODO: transform body such that all lines are CRLF (which we'll do on send)
	// TODO: this will have to be robust, assuming lf only from browser page
	// TODO: breakup body into lines of 78 characters as well
	lines := strings.Split(e.Body, "\n")
	m.Content = append(m.Content, lines...)

	// TODO: store it somewhere persistant, then dump it to the processor queue!
	// We want async: notify of error sending afterwards. Otherwise mark sent when it goes out!
	// TODO: display both
	m.Status = msg.StatusProcessing
	err = h.Carrier.MessageDB.Insert(m)
	if err != nil {
		log.Println(err)
		http.Error(w, "Unable to store message", http.StatusInternalServerError)
		return
	}

	// put on delivery queue for processing
	h.Carrier.Outgoing() <- m

	// note: OK header is automatic
	w.WriteHeader(http.StatusAccepted)

	// TODO: log or handle error?
	w.Write([]byte(m.ID))
}

// TODO: flag or just go for the upload?
// might as well test send first...
func saveAttachment(name string, bytes []byte) {

}
