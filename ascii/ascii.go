package ascii

// TODO: really a textutil pkg

func IsAlpha(c rune) bool {
	return (c >= 0x41 && c <= 0x5A) || // A-Z
		(c >= 0x61 && c <= 0x7A) // a-z
}

func IsDigit(c rune) bool {
	// 0 - 9 is ASCII 48 - 57
	return c >= 48 && c <= 57
}

func IsPrintableChar(c rune) bool {
	// really, ASCII printable char
	return c >= 32 && c <= 126
}

// Split splits s on all chars in cutset
func Split(s, cutset string) []string {
	parts := make([]string, 0)
	soFar := make([]rune, 0)
	for _, b := range s {
		cut := false
		for _, c := range cutset {
			if b == c {
				cut = true
				break
			}
		}

		if cut {
			parts = append(parts, string(soFar))
			soFar = make([]rune, 0)
		} else {
			soFar = append(soFar, b)
		}
	}

	// get last one
	parts = append(parts, string(soFar))
	return parts
}
