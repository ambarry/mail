package cql

import (
	"fmt"
	"sync"
	"time"

	"github.com/gocql/gocql"
)

const (
	WhomKeyspace = "whom"
)

var ksOnce sync.Once

// createKeyspace sets up the 'whom' keyspace.
// This will only run once, so can be called during initialization
// from an cql interface.
// TODO: opts...
func createKeyspace(name string, cluster *gocql.ClusterConfig) error {
	var err error
	ksOnce.Do(func() {
		s := fmt.Sprintf(`CREATE KEYSPACE IF NOT EXISTS %s 
		WITH replication = {
			'class': 'SimpleStrategy',
			'replication_factor': 1
		}`, name)
		c := *cluster // I guess we're making a shallow copy
		c.Keyspace = "system"
		c.Timeout = 20 * time.Second
		var session *gocql.Session
		session, err = c.CreateSession()
		if err != nil {
			return
		}

		defer session.Close()
		err = session.Query(s).Exec()
	})

	return err
}
