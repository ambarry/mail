package cql

import (
	"time"

	"gitlab.com/ambarry/mail/msg"

	"github.com/gocql/gocql"
	"github.com/scylladb/gocqlx/v2"
	"github.com/scylladb/gocqlx/v2/qb"
	"github.com/scylladb/gocqlx/v2/table"
)

// TODO: this is rough, must be kept in sync
// Can we read off or generate from struct and reflection somehow?
var messageMetadata = table.Metadata{
	Name: "messages",
	Columns: []string{
		"email",
		"id",
		"sent",
		"received",
		"error",
		"status",
		"time",
		"last_try",
		"from_addr",
		"to_addrs",
		"content",
	},
	PartKey: []string{"email"},
	SortKey: []string{"time", "id"},
}

// MessageDB stores and retrieves messagess
type MessageDB struct {
	ips     []string
	session gocqlx.Session
	table   *table.Table
}

// NewMessageDB returns instance of MessageDB
func NewMessageDB(ips ...string) *MessageDB {
	db := MessageDB{
		ips: ips,
	}
	return &db
}

// Init sets up tables
func (db *MessageDB) Init() error {
	cluster := gocql.NewCluster(db.ips...)
	cluster.ConnectTimeout = time.Second * 2

	// ensure keyspace
	createKeyspace(WhomKeyspace, cluster)

	cluster.Keyspace = WhomKeyspace

	// TODO: we may want a global gocql session instead, according to docs...
	session, err := gocqlx.WrapSession(cluster.CreateSession())
	if err != nil {
		return err
	}

	db.session = session

	// TODO: shoul the id be a uuid?
	tblStmt := `
	CREATE TABLE IF NOT EXISTS messages (
		email text,
		id text,
		sent boolean,
		received boolean,
		error text,
		status int,
		time timestamp,
		last_try timestamp,
		from_addr text,
		to_addrs list<text>,
		content list<text>,
		PRIMARY KEY (email, time, id)
	) WITH CLUSTERING ORDER BY (time DESC, id ASC)`

	err = session.ExecStmt(tblStmt) // will release
	//err = session.Query(tblStmt).Exec() // TODO: probably need to defer a release?

	if err == nil {
		db.table = table.New(messageMetadata)
	}

	// TODO: await agreement? maybe at end?

	return err
}

// Insert inserts a new message.
func (db *MessageDB) Insert(m msg.Message) error {
	q := db.session.Query(db.table.Insert()).BindStruct(m)
	return q.ExecRelease()
}

// Update updates a message.
// TODO: need to see if this will replace empty cols, etc.
func (db *MessageDB) Update(m msg.Message) error {
	q := db.session.Query(db.table.Update()).BindStruct(m)
	return q.ExecRelease()
}

// GetMessage returns a single message given the supplied email and message id
func (db *MessageDB) GetMessage(email, id string) (msg.Message, error) {
	// TODO: see how we want to do this....
	m := msg.Message{
		Email: email,
		ID:    id,
	}
	q := db.session.Query(db.table.Get()).BindStruct(m)
	err := q.GetRelease(&m)
	return m, err
}

// GetAllMessages gets all messages for an email address
// TODO: paging and limits, etc.
// TODO: attachment metadata, parse out headers ahead of time we want to be easy / queryable, subjects, etc.?
func (db *MessageDB) GetAllMessages(email string) ([]msg.Message, error) {
	var msgs []msg.Message
	q := db.session.Query(db.table.Select()).BindMap(qb.M{"email": email})
	err := q.SelectRelease(&msgs)
	return msgs, err
}
