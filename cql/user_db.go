package cql

import (
	"github.com/scylladb/gocqlx/v2/table"
)

// TODO: means we need to figure out passwords and all
// TODO: should this be a separate service?
var userMetadata = table.Metadata{
	Name:    "users",
	Columns: []string{"email", "id", "first_name", "last_name"},
	PartKey: []string{"email"},
}

type UserDB struct {
}

func NewUserDB() {

}

func (u *UserDB) Init() {

}
