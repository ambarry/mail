package smtp

const (
	SYS_STATUS              = 211
	HELP_MSG                = 214
	SRV_READY               = 220
	SRV_CLOSING             = 221
	MAIL_ACT_OK             = 250
	USER_NOT_LOCAL_FORWARD  = 251
	CANNOT_VRFY_WILL_ACCEPT = 252

	START_MAIL_INPUT = 354

	SRV_NOT_AVAIL                 = 421
	MAIL_ACT_NOT_TAKEN_TMP        = 450
	ERR_PROCESSING                = 451
	MAIL_ACT_NOT_TAKEN_INSUF_STRG = 452
	SRV_UNABLE                    = 455

	ERR_CMD_SYNTAX                      = 500 // too long, not recognized
	ERR_PARAM_SYNTAX                    = 501
	ERR_CMD_NOT_IMPL                    = 502
	ERR_BAD_CMD_SEQ                     = 503
	ERR_PARAM_NOT_IMPL                  = 504
	ERR_MAIL_ACT_NOT_TAKEN_PERM         = 550
	ERR_USER_NOT_LOCAL_TRY_FORWARD      = 551
	ERR_MAIL_ACT_ABORTED_EXCEED_STRG    = 552
	ERR_MAIL_ACT_NOT_TAKEN_MAILBOX_NAME = 553
	ERR_TRANSACTION_FAILED              = 554
	ERR_MAIL_OR_RCPT_PARAM_NOT_RGNZ     = 555
)
