package smtp

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/ambarry/mail/ascii"
)

var atextExtras = map[rune]struct{}{
	'!':  {},
	'#':  {},
	'$':  {},
	'%':  {},
	'&':  {},
	'\'': {},
	'*':  {},
	'+':  {},
	'-':  {},
	'/':  {},
	'=':  {},
	'?':  {},
	'^':  {},
	'_':  {},
	'`':  {},
	'{':  {},
	'|':  {},
	'}':  {},
	'~':  {},
}

func parsePathSection(text string, cmd pathCmd) (int, string, error) {
	lenPref := len(cmd + ":<")
	ok := len(text) >= lenPref+1                                    // include closing '>' in length check                                      // min length
	ok = ok && strings.ToLower(text[0:lenPref]) == string(cmd)+":<" // starts ok

	if !ok {
		return 0, "", errors.New("bad path syntax")
	}

	// nxt space or end
	sp := strings.IndexByte(text[lenPref:], ' ')
	if sp < 0 {
		sp = len(text)
	} else {
		sp += lenPref
	}

	ok = text[sp-1] == '>' // rev path ends before [SP <mail-parameters>]
	if !ok {
		return 0, "", errors.New("bad path syntax")
	}

	return sp, text[lenPref : sp-1], nil
}

// PathParser passes a forward or reverse path
// and validates both local part and domain or addr literal syntax.
type PathParser struct {
	Valid     bool
	AddrLit   bool
	Path      string
	LocalPart string
	Domain    string
}

// NewPathParser creates a new parser for the given path string.
// Expects the supplied path to already be inside the '<>'
func NewPathParser(path string) *PathParser {
	p := PathParser{Path: path}
	return &p
}

// ValidPath checks mail or rcpt path for validity.
// It assumes the path argument has already gone inside the '<>' bookends.
// TODO: will probably need to return parts
// Reverse-path   = Path / "<>"
// Forward-path   = Path
// Path           = "<" [ A-d-l ":" ] Mailbox ">"
func (p *PathParser) ValidPath(allowEmpty bool) (int, error) {
	// path has already gone inside the '<>'
	if p.Path == "" {
		if !allowEmpty {
			return 0, errors.New("empty path")
		}
		return 0, nil
	}

	// ignore any @domains, though accept
	i, err := p.skipAtDomains()
	if err != nil {
		return 0, err
	}

	j, err := p.parseMailbox(i)
	if err != nil {
		return 0, err
	}

	if i+j < len(p.Path) {
		// we stopped parse, but a bad character broke the rest
		return 0, errors.New("bad path syntax")
	}

	p.Valid = true
	return i + j, nil
}

// skipAtDomains reads over at domains, as they must be accepted though ignored.
// Returns the Mailbox index to start from and an error.
// A-d-l          = At-domain *( "," At-domain )
// At-domain      = "@" Domain
func (p *PathParser) skipAtDomains() (int, error) {
	i := 0
	endOk := false
	for {
		if len(p.Path) <= i {
			// reached bounds
			break
		}

		if p.Path[i] != '@' {
			// no more @domains
			break
		}

		i++
		n, err := p.parseDomain(i)
		if err != nil {
			return 0, err
		}

		i += n
		if len(p.Path) <= i {
			break
		}

		// check comma or colon for more
		if p.Path[i] == ',' {
			i++ // next @domain is expeced
		} else if p.Path[i] == ':' {
			i++
			endOk = true
			break // done @domains, mailbox is next
		} else {
			return 0, fmt.Errorf("invalid at-domain syntax, did not expect '%v'", p.Path[i])
		}
	}

	// if we parsed any, the next char must be a colon
	if i > 0 && !endOk {
		return 0, errors.New("invalid at-domain syntax, missing ':'")
	}

	return i, nil
}

// parseDomain parses a domain portion of a path.
// Domain         = sub-domain *("." sub-domain)
// sub-domain     = Let-dig [Ldh-str]
// Let-dig        = ALPHA / DIGIT
// Ldh-str        = *( ALPHA / DIGIT / "-" ) Let-dig
func (p *PathParser) parseDomain(start int) (int, error) {
	i := 0
	in := false
	h := false
	for {
		if len(p.Path) <= start+i {
			break // done string
		}

		c := rune(p.Path[start+i])
		if ascii.IsAlpha(c) || ascii.IsDigit(c) {
			in = true
			h = false
		} else if !in {
			return 0, errors.New("invalid domain syntax, must start with letter or digit")
		} else if c == '-' {
			h = true
		} else if c == '.' {
			// if prev is a -, this is an error
			if h {
				return 0, errors.New("invalid domain syntax, can't end on '-'")
			}
			in = false
		} else {
			// some other char, we're done
			break
		}

		// count it
		i++
	}

	if i == 0 {
		return 0, errors.New("empty domain")
	}

	if h {
		return 0, errors.New("invalid domain syntax, can't end on '-'")
	}

	if !in {
		return 0, errors.New("invalid domain, empty subdomain")
	}

	p.Domain = p.Path[start : start+i]
	return i, nil
}

// parseMailbox parses the mailbox portion of a path.
// Mailbox        = Local-part "@" ( Domain / address-literal )
func (p *PathParser) parseMailbox(start int) (int, error) {
	i, err := p.parseLocalPart(start)
	if err != nil {
		return 0, err
	}

	p.LocalPart = p.Path[start:i]

	// check @ and next char
	if len(p.Path) < start+i+2 {
		return 0, errors.New("mailbox length too short")
	}

	if p.Path[start+i] != '@' {
		return 0, errors.New("mailbox missing '@' symbol or bad local")
	}

	i++
	j := 0
	if p.Path[start+i] == '[' {
		// address literal
		p.AddrLit = true
		j, err = p.parseAddressLiteral(start + i)
	} else {
		j, err = p.parseDomain(start + i)
	}

	if err != nil {
		return 0, err
	}

	p.Domain = p.Path[start+i : start+i+j]

	return i + j, nil
}

// parseLocalPart parses the local part of a mailbox, e.g. before the '@'
// Local-part     = Dot-string / Quoted-string
//                ; MAY be case-sensitive
func (p *PathParser) parseLocalPart(start int) (int, error) {
	if len(p.Path) == 0 {
		return 0, errors.New("empty local part")
	}
	if p.Path[start] == '"' {
		return p.parseQuotedString(start)
	} else {
		return p.parseDotString(start)
	}
}

// parseDotStrng parses a dot string
// Dot-string     = Atom *("."  Atom)
// Atom           = 1*atext
func (p *PathParser) parseDotString(start int) (int, error) {
	i := 0
	in := false
	for i = 0; start+i < len(p.Path); i++ {
		c := p.Path[start+i]
		if isAText(rune(c)) {
			in = true
		} else if in && c == '.' {
			in = false
		} else {
			break // something else, or bad dot (carry on to next section)
		}
	}

	// must have at least 1 atext
	if i <= 0 {
		return 0, errors.New("empty or invalid dotted string")
	}

	return i, nil
}

// parseQuotesString parses a string surrounded by double quotes.
// It may be empty, and mayb include escape sequences with '\'
// Quoted-string    = DQUOTE *QcontentSMTP DQUOTE
// QcontentSMTP     = qtextSMTP / quoted-pairSMTP
// quoted-pairSMTP  = %d92 %d32-126
//                  ; i.e., backslash followed by any ASCII
//                  ; graphic (including itself) or SPace
// qtextSMTP        = %d32-33 / %d35-91 / %d93-126
//                  ; i.e., within a quoted string, any
//                  ; ASCII graphic or space is permitted
//                  ; without blackslash-quoting except
//                  ; double-quote and the backslash itself.
func (p *PathParser) parseQuotedString(start int) (int, error) {
	// a pair of quotes is the minimum quoted string
	ok := len(p.Path) >= start+2 && p.Path[start] == '"'
	if !ok {
		return 0, errors.New("invalid quoted string")
	}

	i := 1
	inQP := false
	for ok {
		// bounds check
		if len(p.Path) <= start+i {
			return 0, errors.New("unexpected end of quoted string")
		}

		c := p.Path[start+i]
		i++
		ok = ascii.IsPrintableChar(rune(c))
		if inQP {
			inQP = false
		} else if c == 34 {
			break // end quote
		} else if c == 92 {
			inQP = true // backslash
		}
	}

	if !ok {
		return 0, errors.New("invalid quoted string, bad char")
	}

	return i, nil
}

// address-literal  = "[" ( IPv4-address-literal /
// 	                IPv6-address-literal /
// 	                General-address-literal ) "]"
// 	                ; See RFC 5321 Section 4.1.3
func (p *PathParser) parseAddressLiteral(start int) (int, error) {
	if len(p.Path) < start+2 {
		return 0, errors.New("address literal too short")
	}

	if p.Path[start] != '[' {
		return 0, errors.New("address literal does not start with '['")
	}

	i := 1
	n := 0
	var err error
	if ascii.IsDigit(rune(p.Path[start+i])) { // TODO: check general addr
		n, err = p.parseIPv4(start + i)
	} else if len(p.Path) >= start+i+5 && p.Path[start+i:start+i+5] == "IPv6:" {
		n, err = p.parseIPv6(start + i)
	} else {
		n, err = p.parseGeneralAddr(start + i)
	}

	if err != nil {
		return 0, err
	}

	i += n
	if len(p.Path) <= start+i {
		return 0, errors.New("address literal stops short")
	}
	if p.Path[start+i] != ']' {
		return 0, errors.New("address literal does not end with ']'")

	}

	return i + 1, nil
}

// parseIPv4 parses an IPv4 address.
// IPv4-address-literal  = Snum 3("."  Snum)
// Snum           = 1*3DIGIT
//                ; representing a decimal integer
//                ; value in the range 0 through 255
func (p *PathParser) parseIPv4(start int) (int, error) {
	i := 0
	parts := 0
	n := 0
	for parts < 4 {
		// bounds - ran out of string, is what we have valid?
		if len(p.Path) <= start+i+n {
			if parts == 3 && n > 0 {
				i += n
				parts++
				break
			}

			// not done and ran out
			return 0, errors.New("not a valid IPv4, incomplete")
		}

		c := rune(p.Path[start+i+n])
		if ascii.IsDigit(c) {
			if n == 3 {
				return 0, errors.New("not a valid IPv4, too many digits")
			}
			n++
		} else if c == '.' {
			if n == 0 {
				return 0, errors.New("not a valid IPv4, leading '.'")
			}
			i += n
			if parts < 3 {
				// Advance past the dot, unless this is a trailing . beyond the ipv4,
				// in which the next section will error anyway
				i++
			}
			n = 0
			parts++ // may be done
		} else if parts == 3 && n > 0 {
			// done - we've parsed an IPv4, let caller deal w/ next char
			i += n
			parts++
		} else {
			// encountered a bad char and not done
			return 0, fmt.Errorf("not a valid IPv4, invalid char %s", string(c))
		}
	}

	return i, nil
}

// parseIPv6 parses an IPv6 address.
// IPv6-addr      = IPv6-full / IPv6-comp / IPv6v4-full / IPv6v4-comp
// IPv6-hex       = 1*4HEXDIG
// IPv6-full      = IPv6-hex 7(":" IPv6-hex)
// IPv6-comp      = [IPv6-hex *5(":" IPv6-hex)] "::"
//                [IPv6-hex *5(":" IPv6-hex)]
//                ; The "::" represents at least 2 16-bit groups of
//                ; zeros.  No more than 6 groups in addition to the
//                ; "::" may be present.
// IPv6v4-full    = IPv6-hex 5(":" IPv6-hex) ":" IPv4-address-literal
// IPv6v4-comp    = [IPv6-hex *3(":" IPv6-hex)] "::"
//                [IPv6-hex *3(":" IPv6-hex) ":"]
//                IPv4-address-literal
//                ; The "::" represents at least 2 16-bit groups of
//                ; zeros.  No more than 4 groups in addition to the
//                ; "::" and IPv4-address-literal may be present.
func (p *PathParser) parseIPv6(start int) (int, error) {
	// TODO: is this case sensitive?
	// "IPv6:" IPv6-addr
	return 0, errors.New("IPv6 not supported yet")
}

// parseGeneralAddr parses a general address.
// General-address-literal  = Standardized-tag ":" 1*dcontent
// Standardized-tag         = Ldh-str
// 				            ; Standardized-tag MUST be specified in a
// 				            ; Standards-Track RFC and registered with IANA
// dcontent                 = %d33-90 / %d94-126
//                          ; Printable US-ASCII excl. "[", "\", "]"
func (p *PathParser) parseGeneralAddr(start int) (int, error) {
	// As far as I can tell, this is a placeholder for future standards.
	// We don't have any tags beyond IPv6
	return 0, errors.New("general address not supported")
}

func isAText(c rune) bool {
	if ascii.IsAlpha(c) || ascii.IsDigit(c) {
		return true
	}

	_, ok := atextExtras[c]
	return ok
}
