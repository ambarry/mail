package smtp

import (
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/ambarry/mail/mime"
	"gitlab.com/ambarry/mail/msg"
)

const serverTimeout = time.Minute * 5

const (
	dataMsg     = "start mail input; end with <CRLF>.<CRLF>"
	data8BITMsg = "start 8BITMIME mail input; end with <CRLF>.<CRLF>"
)

// max lengths in bytes
const (
	MAX_LOCALPART      = 64
	MAX_DOMAIN         = 255 //
	MAX_PATH           = 256 // 501 Path too long
	MAX_CMD            = 512 // includes CRLF, 500 line too long
	MAX_REPLY          = 512
	MAX_RECOMMEND_TEXT = 80     // includes CRLF
	MAX_TEXT           = 1000   // includes CRLF
	MAX_CONTENT_SIZE   = 250000 // this is actually up to us, we should implement SIZE ext. 64k is a MIN max size. This is 25 MB.
	MAX_RCPT           = 100    // this is also a MIN max, use 452 if exceeds

)

type pathCmd string

const (
	// lowercase is important during check
	mailFromCMD = pathCmd("mail from")
	rcptToCMD   = pathCmd("rcpt to")
	specURL     = "https://tools.ietf.org/html/rfc5321"
)

const (
	tlsNone = iota
	tlsBad
	tlsGood
)

type handlerState struct {
	msg.Message
	conn      net.Conn
	writer    *bufio.Writer
	scanner   *bufio.Scanner
	carrier   *Carrier
	tlsConfig *tls.Config

	recvEhlo     bool
	processing   bool
	extSupported bool
	uses8BITMIME bool

	tlsState int
}

// NewHandler creates a new SMTP handler for a client.
func NewHandler(conn *net.TCPConn, cfg *tls.Config, carry *Carrier) *handlerState {
	// Split to CRLF only
	// TODO: should we error on non-CRLF newlines, random CRs, etc?
	h := handlerState{tlsConfig: cfg, carrier: carry}
	h.Received = true // this is for incoming messages - should this just be a different table, even for self-send?
	h.updateConn(conn)
	return &h
}

func (h *handlerState) updateConn(conn net.Conn) {
	// must change writer and reader when we switch to a TLS conn!
	h.conn = conn
	h.scanner = bufio.NewScanner(h.conn)
	h.scanner.Split(SplitCRLF)
	h.writer = bufio.NewWriter(h.conn)
}

func (h *handlerState) Close() {
	h.conn.Close()
}

func (h *handlerState) Run() error {
	// TODO: ability to bail - attempt to send 421 response code if force shut down
	// initial greeting
	err := h.greet()
	if err != nil {
		return err
	}

	// loop the commands we receive
	var cmd string
RUN:
	for {
		// TODO: ability to bail?
		if cmd, err = h.readCmd(); err != nil {
			break
		}

		// flagged for failed TLS already
		if h.tlsState == tlsBad && cmd != "QUIT" {
			err = h.badSecurity()
			if err != nil {
				break
			}
		}

		switch cmd {
		case "STARTTLS":
			err = h.startTLS()
		case "EHLO":
			fallthrough
		case "HELO":
			err = h.hello(cmd)
		case "MAIL":
			err = h.mail()
		case "RCPT":
			err = h.recipient()
		case "DATA":
			// NOTE: data will read until finished (or err) within
			// TODO: ability to bail?
			err = h.data()
		case "NOOP":
			err = h.noop()
		case "RSET":
			err = h.reset()
		case "HELP":
			err = h.help()
		case "VRFY":
			err = h.verify()
		case "EXPN":
			err = h.expand()
		case "QUIT":
			err = h.quit()
			// done!
			break RUN
		default:
			err = h.sendCode(500, "command not recognized")
		}

		if err != nil {
			break
		}
	}

	return err
}

func (h *handlerState) read() error {
	// TODO: we may need to return more information about whether to permanently consider bad
	// until quit, or quit right away
	// For now, only return conn erros
	deadline := time.Now().Add(serverTimeout)
	h.conn.SetReadDeadline(deadline)

	// If disconnected, check for errors. Either way, we expect a QUIT.
	if !h.scanner.Scan() {
		if err := h.scanner.Err(); err != nil {
			return err
		} else {
			return errors.New("unexpected client disconnect")
		}
	}

	return nil
}

func (h *handlerState) readCmd() (string, error) {
	err := h.read()
	if err != nil {
		return "", err
	}

	text := h.scanner.Text()

	// Check command length
	if len(text) > (MAX_CMD - len(msg.CRLF)) {
		// TODO: bad client?
		return "", h.sendCode(ERR_CMD_SYNTAX, "command line too long")
	}

	// TODO: optimize
	sp := strings.IndexByte(text, ' ')
	if sp == 0 || len(text) == 0 {
		// starts with space or empty message
		return "", h.sendCode(ERR_CMD_SYNTAX, "commad not recognized")
	} else if sp < 0 {
		// whole line is a command
		return strings.ToUpper(text), nil
	} else {
		// cmd up to space
		return strings.ToUpper(text[0:sp]), nil
	}
}

func (h *handlerState) readData() (string, error) {
	err := h.read() // TODO: do we want bytes instead?
	if err != nil {
		return "", err
	}

	return h.scanner.Text(), nil
}

func (h *handlerState) greet() error {
	return h.sendCode(SRV_READY, fmt.Sprintf("%s is at your service, %s ", h.carrier.Domain, version))
}

func (h *handlerState) hello(cmd string) error {
	// we expect a hello EHLO (extensions, TLS, prefered) or HELO (fallback, may not even want to support...)
	// EHLO SP ( Domain / address-literal ) CRLF
	// HELO SP Domain CRLF

	// TODO: there is more to this: see pg 33
	h.extSupported = cmd == "EHLO"
	//cs.recvHello = true
	log.Printf("RECEIVED %s\n", cmd)

	text := h.scanner.Text()
	sp := strings.IndexByte(text, ' ')
	if sp < 0 || len(text) == sp+1 {
		log.Println("Missing domain")
		return h.sendCode(ERR_MAIL_ACT_NOT_TAKEN_PERM, "missing domain")
	}

	// TODO: validate? addr literal? what if not from us or for us?
	h.ClientDomain = text[sp+1:]
	h.recvEhlo = true

	// TODO: should we even send the exts if not supported?
	// probably not!

	// 250 Domain [ehlo-greet]
	// OR version w/ ehlo keywords... for example
	//S: 250-foo.com greets bar.com
	//S: 250-8BITMIME
	//S: 250-SIZE
	//S: 250-DSN
	//S: 250-VRFY
	//S: 250 HELP
	// TODO: we should use SIZE when we enforce limit on attachments
	// -> parameterize
	ehloGreet := fmt.Sprintf("%s offers a mug of hot cocoa %s", h.carrier.Domain, h.ClientDomain)
	keywords := []string{
		ehloGreet,
		"8BITMIME",
	}

	// RFC 3207:
	// A server MUST NOT return the STARTTLS extension in response
	// to an EHLO command received after a TLS handshake has completed.
	if h.tlsConfig != nil && h.tlsState != tlsGood {
		keywords = append(keywords, "STARTTLS")
	}

	log.Println("sending greet and start")
	var err error
	for i, kw := range keywords {
		op := sendOptNone
		if i == len(keywords)-1 {
			op = sendOptHyphenate
		}
		err = h.sendCode(MAIL_ACT_OK, kw, op)
		if err != nil {
			break
		}
	}

	return err
}

// mail is a command of the form:
// MAIL FROM:<reverse-path> [SP <mail-parameters> ] <CRLF>
// A space after colon is NOT allowed.
// Reverse path can be used to report errors.
func (h *handlerState) mail() error {
	// Clear buffers
	h.FromAddr = ""
	h.ToAddrs = make([]string, 0)

	log.Println("RECEIVED MAIL FROM")

	if !h.recvEhlo {
		return h.sendCode(ERR_BAD_CMD_SEQ, "bad sequence of commands")
	}

	// TODO: could send 530 (where is that from??) and require STARTTLS

	// If we are already mid-mail, we should 503 if we receive this
	if h.processing {
		// TODO: if we clear path, where do we go from here? only accept quit?
		return h.sendCode(ERR_BAD_CMD_SEQ, "bad sequence of commands")
	}

	// TODO: regex instead?
	// Make sure basic syntax is correct.
	text := h.scanner.Text()
	n, rev, err := parsePathSection(text, mailFromCMD)
	if err != nil {
		return h.sendCode(ERR_PARAM_SYNTAX, err.Error())
	}

	// Validate
	// see pg 40 to validate reverse path (and make a func, same for RCPT params)
	// we can return 550 or 553 typically
	parser := NewPathParser(rev)
	if _, err := parser.ValidPath(true); err != nil {
		return h.sendCode(ERR_MAIL_ACT_NOT_TAKEN_MAILBOX_NAME, err.Error())
	}

	// TODO: we SHOULD check SPF HERE for both From and HELO / EHLO
	// err = VerifySPF()
	// if err != nil {
	// 	return
	// }

	// We are processing mail. A further MAIL cmd is bad sequence after this point.
	h.processing = true
	h.FromAddr = rev

	// TODO:
	// check optional params - see http://www.iana.org/assignments/mail-parameters/mail-parameters.xhtml
	// For now, we can look for BODY=7BIT/8BITMIME
	param := ""
	if n < len(text) {
		// first must be a space, we've already proved that in parsing, or else it was the text length
		param = strings.ToUpper(text[n+1:])
	}

	// this also accounts for a trailing space w/ nothing
	if len(param) > 5 && param[:5] == "BODY=" {
		val := param[5:] // TODO: should really read up to space?
		switch val {
		case "7BIT":
		case "8BITMIME":
			h.uses8BITMIME = true
		default:
			return h.sendCode(ERR_PARAM_SYNTAX, "bad BODY value")
		}
	}

	return h.sendCode(MAIL_ACT_OK, "OK")
}

// recipient is a command of the form:
// RCPT TO:<forward-path> [ SP <rcpt-parameters> ] <CRLF>
// We can have multiple of these.
// Specal rules for BCC -
// Must accept but ignore a source routing list (obsolete).
func (h *handlerState) recipient() error {
	// 250 ok
	// 550, "no such user - "
	log.Println("RECEIVED RCPT TO")

	if !(h.recvEhlo && h.processing) {
		return h.sendCode(ERR_BAD_CMD_SEQ, "bad sequence of commands")
	}

	// Check max recipeients (should allow min 100 recipients) or 452 too many recipients
	if len(h.ToAddrs) >= 100 {
		return h.sendCode(MAIL_ACT_NOT_TAKEN_INSUF_STRG, "too many recipients")
	}

	text := h.scanner.Text()
	_, path, err := parsePathSection(text, rcptToCMD)
	if err != nil {
		// TODO: check code
		return h.sendCode(ERR_PARAM_SYNTAX, err.Error())
	}

	parser := NewPathParser(path)
	if _, err := parser.ValidPath(false); err != nil {
		// TODO: check code
		return h.sendCode(ERR_MAIL_ACT_NOT_TAKEN_MAILBOX_NAME, err.Error())
	}

	// TODO: real user lookup, make sure we store somewhere to distribute, if not saving directly here...
	// TODO: reject any IPs with a 550, similar to gmail
	if path != "test@whom.email" {
		return h.sendCode(ERR_MAIL_ACT_NOT_TAKEN_PERM, "no such user here")
	}

	// TODO: deal w/ remaining params?

	// stay in this state, appending receives
	h.ToAddrs = append(h.ToAddrs, path)
	return h.sendCode(MAIL_ACT_OK, "OK")
}

// data handles actual email data from client.
// DATA <CRLF>
// There is a hack for text with '.' which is the end of data indicator:
//
// "When a line of mail text is received by the SMTP server, it checks
// the line.  If the line is composed of a single period, it is
// treated as the end of mail indicator.  If the first character is a
// period and there are other characters on the line, the first
// character is deleted.""
//
// NOTE: when data ends, we can clear buffers, as a subsequent mail cmd is ok
func (h *handlerState) data() error {
	// Any params, we should reject
	text := h.scanner.Text()
	if len(text) > 4 {
		return h.sendCode(ERR_PARAM_SYNTAX, "DATA cmd must not have args")
	}

	// If we get this before RCPT, could say "no valid recipients" (554) or 503 "command out of sequence"
	if len(h.ToAddrs) == 0 {
		return h.sendCode(ERR_TRANSACTION_FAILED, "no valid recipients")
	}

	// Signal to start sending data -> 354 Intermediate
	dataTxt := dataMsg
	if h.uses8BITMIME {
		dataTxt = data8BITMsg
	}
	err := h.sendCode(START_MAIL_INPUT, dataTxt)
	if err != nil {
		return err
	}

	// All next lines are data body, terminated by a line with just a '.'
	// Then, 250 OK.
	// See RFC 5322 ultimately for message formatting and RFC 2045 for MIME:
	// not super clear what's allowed here, or how to handle an empty section.
	inHeaders := true
	isMIME := false
	size := 0
	mimeInfo := mime.ContentInfo{}
	for {
		// NOTE: no new alloc here, underlying bytes will be overwrittens
		text, err = h.readData()
		if err != nil {
			return err
		}

		// TODO: may want to optimize here too
		if len(text) > 0 && text[0] == '.' {
			if len(text) == 1 {
				// done data!
				break
			} else {
				// strip
				text = text[1:]
			}
		}

		// TODO: technically we could (should?) reject any ASCII above 127 if 8BITMIME was not declared,
		// but we could also just accept it...

		// RFC 5322: a line should not be more than 78 chars and MUST not be more than 998 (MAX_TEXT - CRLF)
		if len(text) > MAX_TEXT-2 {
			return h.sendCode(ERR_MAIL_ACT_ABORTED_EXCEED_STRG, "line too long")
		}

		size += len(text)
		if size > MAX_CONTENT_SIZE {
			return h.sendCode(ERR_MAIL_ACT_ABORTED_EXCEED_STRG, "too much data")
		}

		// TODO: decide what we want to do if there are errors here. From RFC 2045>
		// 		Content-type: text/plain; charset=us-ascii

		//    This default is assumed if no Content-Type header field is specified.
		//    It is also recommend that this default be assumed when a
		//    syntactically invalid Content-Type header field is encountered. In
		//    the presence of a MIME-Version header field and the absence of any
		//    Content-Type header field, a receiving User Agent can also assume
		//    that plain US-ASCII text was the sender's intent.  Plain US-ASCII
		//    text may still be assumed in the absence of a MIME-Version or the
		//    presence of an syntactically invalid Content-Type header field, but
		//    the sender's intent might have been otherwise.

		// TODO: parse and verify MIME content, considering dealing w/ size?
		// we could detect here when headers end...
		// TODO: should we just do at end and verify? Probably...
		if inHeaders && text == "" {
			inHeaders = false
			isMIME, _, mimeInfo, err = mime.ParseHeaders(h.Content)
			if err != nil {
				// TODO: figure out what error, consider returning detail
				// TODO; remove log
				log.Printf("err w/ mime headers: %v\n", err)
				return h.sendCode(ERR_MAIL_ACT_NOT_TAKEN_PERM, "bad mime headers")
			}

			// TODO: remove
			log.Printf("IsMime: %v, mimeInfo: %v\n", isMIME, mimeInfo)
		}

		h.Content = append(h.Content, text)

		// TODO: need to decide: read all, then parse MIME? or pass to MIME processing if
		// header checked from here on out...
		// What is simpler to start? all at end. Do it.
		// BUT! Does our stripping of CRLF affect this? Need to figure that out. Shouldn't though!
		// We could also even do all at once at end, rather than in headers...

		// TODO: we may wish to validate extensions vs content-type headers and reject...

		// TODO: validate required headers, from, etc?

		// TODO: probably some DDOS opportunities here, may need to guard slow client

		// TODO: could enforce 7BIT vs 8BITMIME, etc.
		// We probably need to maintain that info too
	}

	// TODO: verify DKIM and SPF or reject

	// TODO: insert Received header and timestamp? only if forwarding? let delivery do that...
	h.Time = time.Now().UTC()
	h.ID = uuid.New().String() // TODO: do we want to parse the internal message id for anything? validate?

	// TODO: lookup based on user it's for, or else leave blank while we save to a temporary location for further processing
	// we don't really want this going right into the user email db yet, more of a queue
	h.Email = "test@whom.email"
	err = h.carrier.MessageDB.Insert(h.Message)
	if err != nil {
		// TODO: unclear how to handle here...
		log.Println(err)
		h.Content = nil
		return h.sendCode(ERR_PROCESSING, "error processing data, please try again")
	}

	// signal the delivery processor
	h.carrier.Incoming() <- h.Message

	// done
	h.clearProcessing()
	return h.sendCode(MAIL_ACT_OK, "OK")
}

func (h *handlerState) clearProcessing() {
	// NOTE: will not force a 2nd EHLO, do we need?
	h.Message = msg.Message{Received: true}
	h.processing = false
	h.uses8BITMIME = false
}

func (h *handlerState) reset() error {
	if len(h.scanner.Text()) > 4 {
		return h.sendCode(ERR_PARAM_SYNTAX, "RSET cmd must not have args")
	}

	h.clearProcessing()
	return h.sendCode(MAIL_ACT_OK, "OK")
}

func (h *handlerState) noop() error {
	// Params are ok but ignored here
	return h.sendCode(MAIL_ACT_OK, "OK")
}

func (h *handlerState) verify() error {
	// Not implementing
	return h.sendCode(CANNOT_VRFY_WILL_ACCEPT, "Have some faith")
}

func (h *handlerState) help() error {
	return h.sendCode(HELP_MSG, fmt.Sprintf("I believe in you: %s", specURL))
}

func (h *handlerState) expand() error {
	return h.sendCode(ERR_CMD_NOT_IMPL, "EXPN cmd not implemented")
}

func (h *handlerState) quit() error {
	// Any params, we should reject w/ error
	if len(h.scanner.Text()) > 4 {
		return h.sendCode(ERR_PARAM_SYNTAX, "QUIT cmd must not have args")
	}

	return h.sendCode(SRV_CLOSING, fmt.Sprintf("%s closing transmission channel", h.carrier.Domain))
}

func (h *handlerState) badSecurity() error {
	// 554 reply code (with a possible text string such as "Command
	// refused due to lack of security").
	return h.sendCode(ERR_TRANSACTION_FAILED, "Very sus")
}

func (h *handlerState) startTLS() error {
	// 220 Ready to start TLS
	// 501 Syntax error (no parameters allowed)
	// 454 TLS not available due to temporary reason
	// If not a public server, could require TLS or: 530 Must issue a STARTTLS command first
	// to every command other than NOOP, EHLO, STARTTLS, or QUIT
	log.Println("RECEIVED STARTTLS")

	// are we handling?
	if h.tlsConfig == nil {
		return h.sendCode(ERR_CMD_NOT_IMPL, "STARTTLS cmd not implemented")
	}

	// Client MUST not reattempt when active (RFC 3207 4.2)
	if h.tlsState == tlsGood {
		h.tlsState = tlsBad
		return h.badSecurity()
	}

	// Client SHOULD send EHLO after this.
	// Either way, we must clearn state.
	h.clearProcessing()
	h.recvEhlo = false
	if len(h.scanner.Text()) > len("STARTTLS") {
		return h.sendCode(ERR_PARAM_SYNTAX, "STARTTLS cmd must not have args")
	}

	// TODO: would this race at all?
	err := h.sendCode(SRV_READY, "Shake on it")
	if err != nil {
		return err
	}

	// TODO: is config correct and shareable???
	tlsConn := tls.Server(h.conn, h.tlsConfig)

	// NOTE: TLS handshake is required after this cmd, or we kill conn
	// this will handshake if it hasn't already happened,
	// but gives us a handy time to check the err

	err = tlsConn.Handshake()
	if err != nil {
		log.Println("bad TLS handshake")
		h.tlsState = tlsBad
	} else {
		//success
		log.Println("successful TLS handshake")
		h.updateConn(tlsConn)
		h.tlsState = tlsGood
	}

	return nil
}

// TODO: clean this up
const (
	sendOptNone      = 0
	sendOptHyphenate = 1
)

func (h *handlerState) sendCode(code int, text string, opts ...int) error {
	// TODO: remove logging
	// TODO: tidy this up
	const format = "%d %s%s"
	const hformat = "%d-%s%s"
	f := format
	for _, o := range opts {
		if o == 1 {
			f = hformat
		}
	}
	_, err := fmt.Fprintf(h.writer, f, code, text, msg.CRLF)
	if err == nil {
		err = h.writer.Flush()
	}

	if err != nil {
		log.Printf("error sending code %d: %v\n", code, err)
	} else {
		log.Printf("sent code "+f, code, text, "\n")
	}

	return err
}
