package smtp

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ambarry/mail/ascii"
)

func TestParsePathSection(t *testing.T) {
	tests := []struct {
		cmd  pathCmd
		text string
		path string
		ok   bool
	}{
		// TODO: param cases?
		{mailFromCMD, "MAIL FROM:<alex@test.com>", "alex@test.com", true},
		{mailFromCMD, "mail from:<alex@test.com> ", "alex@test.com", true}, // is it?
		{mailFromCMD, "MAIL FROM: <alex@test.com>", "", false},
		{mailFromCMD, "MAIL FROM :<alex@test.com>", "", false},
		{rcptToCMD, "RCPT TO:<alex@test.com>", "alex@test.com", true},
		{rcptToCMD, "rcpt to:<alex@test.com>", "alex@test.com", true},
		{rcptToCMD, "RCPT TO: <alex@test.com>", "", false},
		{rcptToCMD, "RCPT TO :<alex@test.com>", "", false},
	}

	for _, test := range tests {
		// TODO: test length
		_, p, err := parsePathSection(test.text, test.cmd)
		if test.ok {
			assert.Nil(t, err, test.text)
			assert.Equalf(t, test.path, p, "expected %s, got %s", test.path, p)
		} else {
			assert.NotNil(t, err)
		}
	}
}

func TestIsDigit(t *testing.T) {
	tests := []struct {
		c       rune
		isDigit bool
	}{
		{'0', true},
		{'1', true},
		{'2', true},
		{'3', true},
		{'4', true},
		{'5', true},
		{'6', true},
		{'7', true},
		{'8', true},
		{'9', true},
		{'A', false},
		{'a', false},
		{'!', false},
		{',', false},
		{'~', false},
	}
	for _, test := range tests {
		b := ascii.IsDigit(test.c)
		assert.Equalf(t, test.isDigit, b, "expected %v, got %v for %s", test.isDigit, b, string(test.c))
	}
}

func TestIsAlpha(t *testing.T) {
	tests := []struct {
		c       rune
		isAlpha bool
	}{
		{'A', true},
		{'a', true},
		{'B', true},
		{'b', true},
		{'M', true},
		{'m', true},
		{'Z', true},
		{'z', true},
		{'1', false},
		{'5', false},
		{'9', false},
		{';', false},
		{'_', false},
		{',', false},
		{'~', false},
	}
	for _, test := range tests {
		b := ascii.IsAlpha(test.c)
		assert.Equalf(t, test.isAlpha, b, "expected %v, got %v for %v", test.isAlpha, b, test.c)
	}
}

func TestIsAText(t *testing.T) {
	tests := []struct {
		c       rune
		isAText bool
	}{
		{'A', true},
		{'a', true},
		{'B', true},
		{'b', true},
		{'M', true},
		{'m', true},
		{'Z', true},
		{'z', true},
		{'1', true},
		{'5', true},
		{'9', true},
	}

	for k := range atextExtras {
		tests = append(tests, struct {
			c       rune
			isAText bool
		}{k, true})
	}

	for _, test := range tests {
		b := isAText(test.c)
		assert.Equalf(t, test.isAText, b, "expected %v, got %v for %v", test.isAText, b, test.c)
	}
}

func TestIsASCIIPrintable(t *testing.T) {
	tests := []struct {
		c       rune
		isASCII bool
	}{
		{'A', true},
		{'a', true},
		{'B', true},
		{'b', true},
		{'M', true},
		{'m', true},
		{'Z', true},
		{'z', true},
		{'1', true},
		{'5', true},
		{'9', true},
		{'(', true},
		{'/', true},
		{'*', true},
		{'&', true},
		{'^', true},
		{'!', true},
		{'?', true},
		{'<', true},
		{'~', true},
		{'+', true},
		{'"', true},
		{'\\', true},
	}

	for _, test := range tests {
		// TODO: double check char range, since we changed this
		b := ascii.IsPrintableChar(test.c)
		assert.Equalf(t, test.isASCII, b, "expected %v, got %v for %s", test.isASCII, b, string(test.c))
	}
}

func TestParseIPV4(t *testing.T) {
	valid := []string{
		"10.2.101.155",
		"127.0.0.1",
		"1.1.1.1",
		"1.2.3.4",
		"0.0.0.0",
		"255.255.255.255",
		"1.23.456.789",
	}

	leading := []string{
		"",
		" ",
		".",
		"<",
		"a",
		"sandwich ",
		"aardvark",
		"\n",
	}

	trailing := []string{
		"",
		" ",
		".",
		">",
		"a",
		" sandwich",
		"\n",
	}

	for i, v := range valid {
		// test by itself
		p := NewPathParser(v)
		n, err := p.parseIPv4(0)
		assert.Nil(t, err, v)
		assert.Lenf(t, v, n, "for %s, expected count %d, got %d", v, len(v), n)

		// test leading
		p = NewPathParser(leading[i] + v)
		n, err = p.parseIPv4(len(leading[i]))
		assert.Nil(t, err, v)
		assert.Lenf(t, v, n, "leading - for %s, expected count %d, got %d", v, len(v), n)

		// test trailing
		p = NewPathParser(v + trailing[i])
		n, err = p.parseIPv4(0)
		assert.Nil(t, err, v)
		assert.Lenf(t, v, n, "trailing - for %s, expected count %d, got %d", v, len(v), n)

		// test both
		p = NewPathParser(leading[i] + v + trailing[i])
		n, err = p.parseIPv4(len(leading[i]))
		assert.Nil(t, err, v)
		assert.Lenf(t, v, n, "both - for %s, expected count %d, got %d", v, len(v), n)

	}

	bad := []string{
		"10.2.101",
		"10.2.101.",
		"1270.0.1",
		"1234.5.12.1",
		//"255.255.255.255.255", // technically we allow as trailing
		"-66.77.88.99",
		"1.2",
		"",
		"1",
		"1.",
		".1.2.3.4",
		"10. 2. 8. 19",
		"10 .4.5.6",
	}

	for _, b := range bad {
		p := NewPathParser(b)
		_, err := p.parseIPv4(0)
		assert.NotNilf(t, err, b)
	}
}

func TestParseIPv6(t *testing.T) {
	// TODO
}

func TestParseGeneralAddr(t *testing.T) {
	// TODO:
}

func TestParseAddressLiteral(t *testing.T) {
	// TODO: add ipv6 and general addr cases, leading and trailing, etc
	valid := []string{
		"[10.2.101.155]",
		"[127.0.0.1]",
		"[1.1.1.1]",
		"[1.2.3.4]",
		"[0.0.0.0]",
		"[255.255.255.255]",
		"[1.23.456.789]",
	}

	for _, v := range valid {
		p := NewPathParser(v)
		n, err := p.parseAddressLiteral(0)
		assert.Nilf(t, err, v)
		assert.Lenf(t, v, n, "expected count %d, got %d", len(v), n)
	}

}

func TestParseQuotedString(t *testing.T) {
	// TODO: lead + trail
	valid := []string{
		"\"\"", // empty quotes, in the go string
		"\"a\"",
		"\"hello\"",
		"\"Hello, World!\"",
		"\"This has \\\" nested \\\\ symbols \\n like that \\.\"", // NOTE: double escape for Go string
	}

	for _, v := range valid {
		p := NewPathParser(v)
		n, err := p.parseQuotedString(0)
		assert.Nilf(t, err, "expected nil for '%s'", v)
		assert.Lenf(t, v, n, "for %s, expected count %d, got %d", v, len(v), n)
	}

	// TODO: bad
}

func TestParseDotString(t *testing.T) {
	// TODO: lead + trail
	valid := []string{
		"hello",
		"hello-world!",
		"hello_world!",
		"hello.world???",
		"789This.Is.A+Valid_Message",
		"/Forward/Slash/Is/Okay",
		"'So'Are'Single'Quotes~~~}}}|||***",
	}

	for _, v := range valid {
		p := NewPathParser(v)
		n, err := p.parseDotString(0)
		assert.Nilf(t, err, "expected nil for '%s'", v)
		assert.Lenf(t, v, n, "for %s, expected count %d, got %d", v, len(v), n)
	}

	// NOTE: using invalid chars at start, because our parses
	// won't throw an error, but will shortcut length otherwise
	bad := []string{
		"",
		".can't.start.with.dot",
		",comma_not_either",
		"\\Reverse\\Slash\\Needs\\Quotes",
		" Hello, world! Has spaces, needs quotes",
		"\"YouAlsoCan'tIncludeDoubleQuotes",
	}

	for _, b := range bad {
		p := NewPathParser(b)
		_, err := p.parseDotString(0)
		assert.NotNilf(t, err, b)
	}
}

func TestParseLocalPart(t *testing.T) {
	// TODO: lead + trail

	// local is all up to the '@'
	valid := []string{
		"alex@test.com",
		"***?alex@[127.0.0.1]",
		"!al/ex.b@test.com",
		"123alex.m+-b@test.something.org",
		"\"This is a really st\\range--email\"@somehowvalid.com",
	}

	for _, v := range valid {
		p := NewPathParser(v)
		n, err := p.parseLocalPart(0)
		assert.Nilf(t, err, "expected nil for '%s'", v)
		local := strings.Split(v, "@")
		length := len(local[0])
		assert.Equal(t, length, n, "for %s, expected count %d, got %d", v, length, n)
	}

	bad := []string{
		" space@test.com",
		"\"UnfinishedQuotes@somewhere.org",
		".@justADot.com",
	}

	for _, b := range bad {
		p := NewPathParser(b)
		_, err := p.parseLocalPart(0)
		assert.NotNilf(t, err, b)
	}
}

func TestParseMailbox(t *testing.T) {
	// TODO: lead + trail

	valid := []string{
		"alex@test",
		"alex@test.com",
		"alex@test-test.com",
		"***?alex@[127.0.0.1]",
		"alex@127.0.1", // technically valid domain
		"*@2",          // somehow this is ok???
		"!al/ex.b@test.com",
		"123alex.m+-b@test.something.org",
		"\"This is a really st\\range--email\"@somehowvalid.com",
	}

	for _, v := range valid {
		p := NewPathParser(v)
		t.Logf("Mailbox - parsing valid: %s\n", v)
		n, err := p.parseMailbox(0)
		assert.Nilf(t, err, "expected nil for '%s'", v)
		assert.Lenf(t, v, n, "for %s, expected count %d, got %d", v, len(v), n)
	}

	bad := []string{
		"alex..b@test.com", // double dot
		"alex@[127.0.1]",
		"alex@[127.0.1.2.3]",
		"alex@[127.0.0.1111]",
		"alex@-dash.com",
		"alex@dash-.com",
		"alex@dash.com-",
		"alex@dash.",
		"alex@dash..com",
		"alextest.com",
		"alex",
		"alex @test.com",
		"alex@ test.com",
		//"alex@test_test.com",  // this will survive until validPath
		" space@test.com",
		"\"UnfinishedQuotes@somewhere.org",
		".@justADot.com",
	}

	for _, b := range bad {
		p := NewPathParser(b)
		t.Logf("Mailbox - parsing bad: %s\n", b)
		_, err := p.parseMailbox(0)
		assert.NotNilf(t, err, b)
	}
}

func TestParseDomain(t *testing.T) {
	// TODO: for now, covered by mailbox
}

func TestSkipAtDomains(t *testing.T) {

}

func TestVaildPath(t *testing.T) {
	// TODO:test w/ some @domains
	valid := []string{
		"",
		"alex@test",
		"alex@test.com",
		"alex@test-test.com",
		"***?alex@[127.0.0.1]",
		"alex@127.0.1", // technically valid domain
		"*@2",          // somehow this is ok???
		"!al/ex.b@test.com",
		"123alex.m+-b@test.something.org",
		"\"This is a really st\\range--email\"@somehowvalid.com",
	}

	for _, v := range valid {
		p := NewPathParser(v)
		n, err := p.ValidPath(true)
		assert.Nilf(t, err, "expected nil for '%s'", v)
		assert.Lenf(t, v, n, "for %s, expected count %d, got %d", v, len(v), n)
	}

	bad := []string{
		"alex..b@test.com", // double dot
		"alex@[127.0.1]",
		"alex@[127.0.1.2.3]",
		"alex@[127.0.0.1111]",
		"alex@-dash.com",
		"alex@dash-.com",
		"alex@dash.com-",
		"alex@dash.",
		"alex@dash..com",
		"alextest.com",
		"alex",
		"alex @test.com",
		"alex@ test.com",
		"alex@test_test.com",
		" space@test.com",
		"\"UnfinishedQuotes@somewhere.org",
		".@justADot.com",
	}

	for _, b := range bad {
		p := NewPathParser(b)
		_, err := p.ValidPath(true)
		assert.NotNilf(t, err, b)
	}
}
