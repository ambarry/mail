package smtp

import (
	"context"
	"crypto/rsa"
	"crypto/tls"
	"log"
	"net"
	netsmtp "net/smtp"
	"sort"
	"strings"
	"time"

	"gitlab.com/ambarry/mail/dkim"
	"gitlab.com/ambarry/mail/msg"
)

// TODO: make these configurable
const MaxSendTryDuration = time.Hour * 24 // Supposed to be 4-5 days, but come on..
const SendTryInterval = time.Hour * 1     // NOTE: can vary based on type of failure eventually, min 30 mins
const DefaultBufSize = 100

type byPref []*net.MX

func (a byPref) Len() int           { return len(a) }
func (a byPref) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byPref) Less(i, j int) bool { return a[i].Pref < a[j].Pref }

// Carrier queues up mail for delivery
type Carrier struct {
	outgoing     chan msg.Message
	incoming     chan msg.Message
	resolver     *net.Resolver
	Domain       string
	MessageDB    msg.MessageDB
	PrivateKey   *rsa.PrivateKey
	UseTLS       bool
	DKIMSelector string
	AllowList    []string

	// TODO: pass in resolver!
}

// CarrierOpt is an option function for Carrier
type CarrierOpt func(c *Carrier) *Carrier

// WithDomain configures the domain of the Carrier
func WithDomain(domain string) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.Domain = domain
		return c
	}
}

// WithBufferSize configures the incoming and outgoing queue channel sizes
func WithBufferSize(bufSize int) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.incoming = make(chan msg.Message, bufSize)
		c.outgoing = make(chan msg.Message, bufSize)
		return c
	}
}

// WithStorage configures the Carrier storage backend
func WithStorage(store msg.MessageDB) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.MessageDB = store
		return c
	}
}

// WithDKIM configures PrivateKey and selector for DKIM signing
func WithDKIM(key *rsa.PrivateKey, selector string) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.PrivateKey = key
		c.DKIMSelector = selector
		return c
	}
}

// UseTLS configures carrier to use StartTLS command when extension is supported when sending email.
// This defaults to true.
func UseTLS(useTLS bool) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.UseTLS = useTLS
		return c
	}
}

// WithAllowList configures allowList
func WithAllowList(allowList []string) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.AllowList = allowList
		return c
	}
}

// WithResolver overrides the DNS resolver for the Carrier.
// This is useful for testing.
func WithResolver(res *net.Resolver) CarrierOpt {
	return func(c *Carrier) *Carrier {
		c.resolver = res
		return c
	}
}

// NewCarrier creats a new Carrier with defaults and applies any options
func NewCarrier(opts ...CarrierOpt) *Carrier {
	// defaults
	c := Carrier{
		incoming:  make(chan msg.Message, DefaultBufSize),
		outgoing:  make(chan msg.Message, DefaultBufSize),
		Domain:    "localhost",
		MessageDB: msg.NewFSMessageDB("."),
		UseTLS:    true,
		resolver:  net.DefaultResolver,
	}

	for _, opt := range opts {
		opt(&c)
	}
	return &c
}

// Incoming returns incoming message channel
func (c *Carrier) Incoming() chan<- msg.Message {
	return c.incoming
}

// Outgoing returns outgoing message channel
func (c *Carrier) Outgoing() chan<- msg.Message {
	return c.outgoing
}

// StartProcessing handles messages as they are created
// Directs incoming and outgoing messages
func (c *Carrier) StartProcessing(stop <-chan bool) {
	go c.processIn(stop)
	go c.processOut(stop)
}

func (c *Carrier) validRecv(email string) bool {
	// TODO: lookup our users using a user db
	email = strings.ToLower(email)
	return email == "test@whom.email" || email == "postmaster@whom.email"
}

func (c *Carrier) validSend(email string) bool {
	// TODO: not sure why we'd block necessarily, but may need to check, ip, etc.
	// We expect a real domain for now, ability to look up MX record
	if c.AllowList == nil {
		return true
	}

	// white list mode
	for _, e := range c.AllowList {
		if email == e {
			return true
		}
	}

	return false
}

func (c *Carrier) processOut(stop <-chan bool) {
PROC:
	for {
		select {
		case <-stop:
			break PROC
		case m := <-c.outgoing:
			// TODO:!!!

			// look up MX record
			// TODO: may want to group by domain, even map the MX
			// TODO: need to mark for resends
			// TODO: upon failure of last resend attempt, need to report / notify of failure
			// TODO: this means we need tracking per recipient / for path
			log.Printf("[carrier] procesing outgoing msg %s\n", m.ID)

			// Sign our message
			if c.PrivateKey != nil {
				sig := dkim.NewSignature(c.Domain, "test", c.PrivateKey)
				err := sig.Sign(&m)
				if err != nil {
					log.Printf("[carrier] erroring signing DKIM %v\n", err)
					c.updateStatus(m, msg.StatusFail, "signing failed")
					// TODO: need to handle retry, etc
					continue
				}

				sigHd, err := sig.AsHeader()
				if err != nil {
					log.Printf("[carrier] erroring making DKIM header %v\n", err)
					c.updateStatus(m, msg.StatusFail, "signing header failed")
					// TODO: need to handle retry, etc
					return
				}

				// TODO: remove
				log.Printf("[carrier] created header: %s\n", sigHd)

				m.PrependHeader(sigHd)
			}

			for _, fp := range m.ToAddrs {
				// assumes we've validated already
				// TODO: we should also group by the domains?
				if !c.validSend(fp) {
					// TODO: probably need to report a permanent error on this msg
					log.Printf("[carrier] won't send to email '%s'\n", fp)
					c.updateStatus(m, msg.StatusFail, "bad address")
					continue
				}

				parser := NewPathParser(fp)
				_, err := parser.ValidPath(false)
				if err != nil {
					// TODO: probably need to report a permanent error on this msg
					log.Printf("[carrier] won't send to email '%s', error parsing: %v\n", fp, err)
					c.updateStatus(m, msg.StatusFail, "bad address")
					continue
				}

				// TODO: allow localhost testing flag, but nothing else...
				if parser.AddrLit {
					// TODO: send the email early if acceptable
					log.Printf("[carrier] won't send to email '%s', not handling addr literals\n", fp)
					c.updateStatus(m, msg.StatusFail, "bad address")
					continue
				}

				// TODO: if our domain, we can skip this and just handle outside of SMTP

				// find MX record
				// TODO: use a real resolver so we can pass a context with timeouts
				mxs, err := c.resolver.LookupMX(context.Background(), parser.Domain)
				if err != nil {
					// TODO: mark something, need to retry. This is either a bad email (permanent) or network issue (temporary).
					// If domain itself doesn't exist (may need to check err) we could permanently stop, but not sure if that will be indicated.
					log.Printf("[carrier] error getting MX record for domain %s, %v\n", parser.Domain, err)
					c.updateStatus(m, msg.StatusTemporaryFail, "bad address")
					continue
				}

				if len(mxs) == 0 {
					// RFC 5321 Section 5.1
					// If an empty list of MXs is returned,
					// the address is treated as if it was associated with an implicit MX
					// RR, with a preference of 0, pointing to that host.
					err = c.sendEmail(parser.Domain, fp, m)
				} else {
					// TODO: looks like LookupMX sorts by default by pref, we can probably skip unless that's undefined
					sort.Sort(byPref(mxs))
					for _, mx := range mxs {
						err = c.sendEmail(mx.Host, fp, m)
						if err == nil {
							break
						}
					}
				}

				if err != nil {
					// TODO: handle - mark for resend at a time, let sit in queue or get scooped again
					log.Println(err)
					c.updateStatus(m, msg.StatusTemporaryFail, "error sending email")
					continue
				}

				log.Printf("[carrier] successfully sent email %s!\n", m.ID)
			}
		}
	}
}

func (c *Carrier) updateStatus(m msg.Message, stat int, errText string) {
	m.Error = errText
	m.Status = stat
	err := c.MessageDB.Update(m)
	if err != nil {
		// TODO: panic?
		log.Printf("[carrier] error updating msg status %v\n", err)
	}
}

func (c *Carrier) sendEmail(host, fp string, m msg.Message) (err error) {
	// NOTE: trailing '.' is for root zone and should be preserved in mx host
	// NOTE: we assume content, headers, etc. have been formatted correctly by this point,
	// as we no longer have the cc, bcc info, etc.

	// make sure we use our resolver
	conn, err := c.resolver.Dial(context.Background(), "tcp", host+":smtp")
	if err != nil {
		return err
	}

	client, err := netsmtp.NewClient(conn, host)
	if err != nil {
		return
	}

	// modify if we error on close only
	defer func() {
		cerr := client.Close()
		if cerr == nil {
			return
		}

		if err != nil {
			log.Printf("Client close error after other error: %v\n", err)
		}

		err = cerr
	}()

	// do hello, see if extension supports STARTTLS...
	err = client.Hello(c.Domain)
	if err != nil {
		return
	}

	// STARTTLS if we and the server are set to use STARTTLS extension
	startTLS := c.UseTLS
	if startTLS {
		startTLS, _ = client.Extension("STARTTLS")
	}
	if startTLS {
		cfg := tls.Config{
			ServerName: host,
		}
		log.Printf("[carrier] attempting to start TLS with %s\n", host)
		err = client.StartTLS(&cfg)
		if err != nil {
			return
		}

		// NOTE: we have to call EHLO again after the STARTTLS, but the client
		// here oh so hopefully does this for us. In fact, it throws an error if
		// we try again...
		log.Println("[carrier] started TLS handshake")
	}

	err = client.Mail(m.FromAddr)
	if err != nil {
		return
	}

	// Only send for this rcpt, not all in message
	// TODO; consider all in domain
	err = client.Rcpt(fp)
	if err != nil {
		return
	}

	w, err := client.Data()
	if err != nil {
		return
	}

	// NOTE: Close will insert the final .CRLF
	// This will also prefix any '.' lines so we don't have to.
	for _, s := range m.Content {
		_, err = w.Write([]byte(s + msg.CRLF))
		if err != nil {
			return
		}
	}

	// NOTE: must close if we intent to call anything else on client
	err = w.Close()
	if err != nil {
		return
	}

	return nil
}

func (c *Carrier) processIn(stop <-chan bool) {
PROC:
	for {
		select {
		case <-stop:
			break PROC
		case msg := <-c.incoming:
			// TODO:!!!
			// Right now, we've already stored it. In theory, we'd be moving things around to user area, but
			// at the moment not sure we need to do anything?

			// TOOD: check forPaths against (c.validRecv)
			log.Printf("[carrier] procesing incoming msg %s\n", msg.ID)
		}
	}
}
