package smtp

import "bytes"

// SplitCRLF scans lines until CRLF. It is similar to bufio.ScanLines, except the carriage return
// is _not_ optional.
// The last non-empty line is returned regardless.
func SplitCRLF(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	i := 0
	search := data
	for len(search) > 0 {
		j := bytes.IndexByte(search, '\n')
		if j < 0 {
			// No more new lines
			break
		}

		// Is this a CRLF?
		if j > 0 && search[j-1] == '\r' {
			fin := i + j
			return fin + 1, data[0 : fin-1], nil
		}

		j += 1
		i += j              // track total offset
		search = search[j:] // keep searching -> this may return an empty slice, at which point loop exits
	}

	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		// drop final \r if we ended on that for some reason
		if data[len(data)-1] == '\r' {
			return len(data), data[0 : len(data)-1], nil
		}
		return len(data), data, nil
	}

	// Request more data.
	return 0, nil, nil

}
