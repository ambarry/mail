package smtp

import (
	"bufio"
	"strings"
	"testing"
)

func crlfScan(s string) *bufio.Scanner {
	scan := bufio.NewScanner(strings.NewReader((s)))
	scan.Split(SplitCRLF)
	return scan
}

func appendCRLF(exp []string) string {
	test := ""
	for _, s := range exp {
		test += s + "\r\n"
	}

	return test
}

// testLines tests the scanner lines against expected.
// Returns number of lines read / splits.
func testLines(t *testing.T, scanner *bufio.Scanner, expected []string) int {
	i := 0
	for scanner.Scan() {
		if scanner.Text() != expected[i] {
			t.Fatalf("text mismatch case %d, expected '%s', got '%s'", i, expected[i], scanner.Text())
		}
		i++
	}

	if scanner.Err() != nil {
		t.Fatalf("scanner err %s", scanner.Err().Error())
	}

	return i
}

func assertDone(t *testing.T, scanner *bufio.Scanner) {
	if scanner.Scan() {
		t.Fatalf("scanner still going")
	}
}

func TestScanSMPTOneLine(t *testing.T) {
	exp := []string{"This is one good line"}
	test := appendCRLF(exp)
	scanner := crlfScan(test)
	testLines(t, scanner, exp)
	assertDone(t, scanner)
}

func TestScanSMTPMultipleLines(t *testing.T) {
	exp := []string{
		"This is one good line",
		"Here is a 2nd line",
		"And a third, why not",
	}
	test := appendCRLF(exp)
	scanner := crlfScan(test)
	testLines(t, scanner, exp)
	assertDone(t, scanner)
}

func TestScanSMTPEmptyLines(t *testing.T) {
	exp := []string{
		"This is one good line",
		"",
		"",
		"And a fourth, why not",
		"",
	}
	test := appendCRLF(exp)
	scanner := crlfScan(test)
	testLines(t, scanner, exp)
	assertDone(t, scanner)
}

func TestScanSMTPIntermediatePartials(t *testing.T) {
	// May need to change this, need to read spec
	exp := []string{
		"This is one good line\nStuff and things\rNo splits yet until now",
		"Why \n not \n\n \r \r\r",
		"And a final",
	}
	test := appendCRLF(exp)
	scanner := crlfScan(test)
	testLines(t, scanner, exp)
	assertDone(t, scanner)
}

func TestScanSMTPEOFNonCRLF(t *testing.T) {
	exp := []string{
		"This is one good line",
		"And a final",
	}
	test := exp[0] + "\r\n" + exp[1]
	scanner := crlfScan(test)
	testLines(t, scanner, exp)
	assertDone(t, scanner)
}
