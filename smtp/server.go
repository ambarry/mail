package smtp

import (
	"crypto/tls"
	"log"
	"net"
)

// TODO: update from args
var (
	version string = "SMTP v0.0.1"
)

// Server listens and handles SMTP connections
type Server struct {
	Listener  *net.TCPListener
	Carrier   *Carrier
	TLSConfig *tls.Config
}

// NewServer creates and returns a new server with a TCPListener
func NewServer(addr string, cfg *tls.Config, carry *Carrier) (*Server, error) {
	log.Printf("starting SMTP server on %s\n", addr)

	laddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}

	lstn, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		return nil, err
	}

	srv := Server{
		Listener:  lstn,
		Carrier:   carry,
		TLSConfig: cfg,
	}
	return &srv, nil
}

// Close closes any TCP listener
func (srv *Server) Close() {
	if srv.Listener != nil {
		srv.Listener.Close()
	}
}

// Listen listens for incoming TCP connections and dispatches
// to a handler in a separate goroutine
// TODO: safeguard DDOS, handler queueing?
func (srv *Server) Listen() error {
	for {
		// TODO: see how we did this in orca, there's some fanciness here to avoid ddos, perhaps
		// maybe a timeout?
		conn, err := srv.Listener.AcceptTCP()
		if err != nil {
			return err
		}

		// TODO: blocking loop until sig or whatever
		log.Println("received new connection from: ", conn.RemoteAddr().String())
		go srv.Handle(conn)
	}
}

func (srv *Server) Handle(conn *net.TCPConn) error {
	// We've connected! Send received code and optional server code info
	h := NewHandler(conn, srv.TLSConfig, srv.Carrier)
	defer h.Close()
	err := h.Run()
	if err != nil {
		log.Println("error during SMTP: ", err)
		return err
	}

	log.Println("connection closed: ", conn.RemoteAddr().String())
	return nil
}
