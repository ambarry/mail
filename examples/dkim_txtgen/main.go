package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/ambarry/mail/dkim"
)

// this program reads a public key and generates the DKIM TXT RR string
// for use in DNS TXT RR
func main() {
	var (
		path = flag.String("path", "", "path to public key file")
	)

	flag.Parse()

	if *path == "" {
		log.Fatalln("no path supplied")
	}

	s, err := dkim.GenerateRRString(*path)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(s)

}
