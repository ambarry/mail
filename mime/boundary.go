package mime

import (
	"errors"
	"fmt"

	"gitlab.com/ambarry/mail/ascii"
)

// EarlyBoundaryError signals an error of an outer boundary encountered when parsing a nested
// multipart message. See RFC 2046. It is clear we need to detect, though not yet clear how to
// handle...
type EarlyBoundaryError struct {
	Depth    int
	Boundary string
}

// NewEarlyBoundaryError creates a new error instance.
func NewEarlyBoundaryError(depth int, boundary string) *EarlyBoundaryError {
	eb := EarlyBoundaryError{
		Depth:    depth,
		Boundary: boundary,
	}
	return &eb
}

// Error implements error interface with a message.
func (eb *EarlyBoundaryError) Error() string {
	return fmt.Sprintf("early boundary detected, boundary %s for depth %d",
		eb.Boundary, eb.Depth)
}

func getBoundary(ci ContentInfo) (string, error) {
	// validate boundary
	boundary, ok := ci.Type.Parameters["boundary"]
	if !ok || len(boundary) == 0 {
		return "", errors.New("boundary is required")
	}

	if boundary[len(boundary)-1] == ' ' {
		return "", errors.New("boundary invalid, cannot end with space")
	}

	for _, r := range boundary {
		_, ok = boundaryChars[r]
		if !ok {
			ok = ascii.IsDigit(r) || ascii.IsAlpha(r)
		}

		if !ok {
			return "", fmt.Errorf("invalid boundary char %c", r)
		}
	}

	boundary = "--" + boundary
	return boundary, nil
}

// isBound returns whether this is a boundary match, and whether it is a closing boundary
func isBound(line, bound string) (bool, bool) {
	i := len(bound)
	match := len(line) >= i && line[:i] == bound
	if !match {
		return false, false
	}

	closed := len(line) >= i+2 && line[i:i+2] == "--"
	return true, closed
}

func checkBounds(line string, bounds []string) (bool, bool, error) {
	if len(line) <= 2 || line[:2] != "--" {
		// not a boundary
		return false, false, nil
	}

	// see dashes: is it our boundary?
	b := len(bounds) - 1
	if match, closed := isBound(line, bounds[b]); match {
		return true, closed, nil
	}

	// is it an incorrect outer boundary?
	for j := b - 1; j >= 0; j-- {
		if match, _ := isBound(line, bounds[j]); match {
			return false, false, NewEarlyBoundaryError(j, bounds[j])
		}
	}

	return false, false, nil
}
