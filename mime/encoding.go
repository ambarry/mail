package mime

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"mime/quotedprintable"
	"path/filepath"
	"strings"

	"gitlab.com/ambarry/mail/msg"
)

// NOTES:
// 1. Binary is not actually supported, though should recognze.
// 2. 8bit, 7bit, and binary are really "identity" operations.
// 3. When decoding:
//   RFC2049:
//   Must treat any unrecognized Content-Transfer-Encoding
//   as if it had a Content-Type of "application/octet-
//   stream", regardless of whether or not the actual
//   Content-Type is recognized.
var mimeTypes []string
var typeByExt map[string]string

// Mechanism is a type for encoding mechanism values
type Mechanism string

// Encoding Mechanisms - binary is not really supported
const (
	EncodingMechanism7Bit            = Mechanism("7bit")
	EncodingMechanism8Bit            = Mechanism("8bit")
	EncodingMechanismQuotedPrintable = Mechanism("quoted-printable")
	EncodingMechanismBase64          = Mechanism("base64")
)

// Encoding contains the encoded string as well as meta information.
type Encoding struct {
	// TODO: possibly merge w/ ContentTypeHeader stuff in header.go
	ContentType string
	FileName    string
	Parameters  map[string]string // e.g. charset
	Body        string            // could do []byte, but binary is never actually allowed
	Mechanism   Mechanism         // NOTE: binary is never actually allowed
}

// EncodingOpt for configuring encodings
type EncodingOpt func(*Encoding)

// NewEncoding creates a new encoding.
// The default encoding mechanism is 8bit.
// If the ContentType is not recognizable from the file extension, we default to
// "application/octet-stream"
// This method lets us potentially override ContentType or specify restrictions on the encoding
// mechanism prior to performing the actual encoding, via the EncodingOpts.
func NewEncoding(path string, opts ...EncodingOpt) *Encoding {
	ext := filepath.Ext(path)
	typ, ok := typeByExt[ext]
	if !ok {
		typ = "application/octet-stream"
	}

	enc := Encoding{
		ContentType: typ,
		FileName:    filepath.Base(path),
		Mechanism:   EncodingMechanism8Bit,
	}

	for _, opt := range opts {
		opt(&enc)
	}
	return &enc
}

// WithMechanism generates an option that sets the Mechanism for encoding
func WithMechanism(mech Mechanism) EncodingOpt {
	return func(enc *Encoding) {
		enc.Mechanism = mech
	}
}

// Encode reads a file path and encodes the content into a string suitable for internet messages.
// NOTE: we expect the upload or recv side to validate extensions, not this method.
func (enc *Encoding) Encode(bytes []byte) error {
	// TODO:
	// based on type, decide if text or binary and must be encoding
	// based on 7bit or 8bit, deal with it, error
	switch enc.Mechanism {
	case EncodingMechanism7Bit:
		// TODO: should we trust or validate? e.g. error if > 127?
		// TODO: check column counts?
		enc.Body = string(bytes)
	case EncodingMechanism8Bit:
		// TODO: should we trust or validate?
		// TODO: check column counts!>
		enc.Body = string(bytes)
	case EncodingMechanismQuotedPrintable:
		// TODO: also must be 76 chars
		// Soft line breaks are trailing = signs followed by CRLF
		b := strings.Builder{}
		_, err := quotedprintable.NewWriter(&b).Write(bytes)
		if err != nil {
			return err
		}
		enc.Body = b.String()
		fmt.Println(enc.Body)
	case EncodingMechanismBase64:
		// RFC 2045 Section 6.8 - max 76 chars per line, use padding
		buf := make([]byte, base64.StdEncoding.EncodedLen(len(bytes)))
		base64.StdEncoding.Encode(buf, bytes)
		b := strings.Builder{}
		var i int
		for i = 0; i < len(buf)-76; i += 76 {
			b.Write(buf[i : i+76])
			b.WriteString(msg.CRLF)
		}
		// don't forget end
		b.Write(buf[i:])
		enc.Body = b.String()

	default:
		return fmt.Errorf("unknown mechanism for encoding: %v", enc.Mechanism)
	}

	return nil
}

func (enc *Encoding) Decode(lines []string) ([]byte, error) {
	// TODO:!!! note defaults as well
	// possibly take ContentInfo
	// TODO: use mime lib where you can!
	// TODO: what do we return when a text based type? should we even go here?
	// I suppose, since it's a text file in theory that we've attached? Or is it for display?
	// tricky tricky, think of text/plain and text/html
	switch enc.Mechanism {
	case EncodingMechanism7Bit:
		// TODO: just return? or validate?
		// TODO: is the \n okay? maybe only for text, otherwise join raw?
		// but would that make any sense anyway?
		return []byte(strings.Join(lines, "\n")), nil
	case EncodingMechanism8Bit:
		return []byte(strings.Join(lines, "\n")), nil
	case EncodingMechanismBase64:
		// RFC 2045 Section 6.8 - max 76 chars per line, use padding
		var bytes []byte
		for _, line := range lines {
			b, err := base64.StdEncoding.DecodeString(line)
			if err != nil {
				return nil, err
			}
			bytes = append(bytes, b...)
		}
		return bytes, nil
	case EncodingMechanismQuotedPrintable:
		// Also must be 76 chars? validate?
		// Soft line breaks are trailing = signs followed by CRLF
		var bytes []byte
		for _, line := range lines {
			if len(line) > 76 {
				return nil, errors.New("MIME - quotedprintable exceeded 76 chars")
			}
			sr := strings.NewReader(line)
			qr := quotedprintable.NewReader(sr)
			b, err := ioutil.ReadAll(qr) // TODO: should we buffer this better and read directly into bytes?
			if err != nil {
				return nil, err
			}
			bytes = append(bytes, b...)
		}
		return bytes, nil
	default:
		return nil, errors.New("MIME - bad content encoding for decode")
	}
}
