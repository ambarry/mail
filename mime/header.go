package mime

import (
	"errors"
	"fmt"
	"strings"
)

// RFCs 2045-2049
// TODO: use the golang mime package?? has media type parsing!

// Lowercased header constants for parsing
const (
	headerPrefixContentType             = "content-type"
	headerPrefixContentTransferEncoding = "content-transfer-encoding"
	headerPrefixContentDescription      = "content-description"
	headerPrefixContentID               = "content-id"
	headerPrefixMIMEVersion             = "mime-version"
)

// Top Level content types
const (
	DiscreteTopLevelText        = "text"
	DiscreteTopLevelImage       = "image"
	DiscreteTopLevelAudio       = "audio"
	DiscreteTopLevelVideo       = "video"
	DiscreteTopLevelApplication = "application"
	// TODO: "font" may now be acceptable, but "example" is an error and should be reported
	CompositeTopLevelMessage   = "message"
	CompositeTopLevelMultipart = "multipart"
)

// Multipart sub types
// Unknown must be treated as Mixed
const (
	MulitpartSubMixed       = "mixed"
	MultipartSubAlternative = "alternative"
	MultipartSubParallel    = "parallel"
	MultipartSubDigest      = "digest"
)

// Message sub types
// NOTE: it is very doubtful we'll want to support stuff like this.
// Both partial and external-body present security concerns.
const (
	MessageSubRFC822       = "rfc822"
	MessageSubPartial      = "partial"
	MessageSubExternalBody = "external-body"
)

type header struct {
	key        string
	value      string
	parameters map[string]string
}

// ContentTypeHeader contains the top and sub types, as well as any parameters
type ContentTypeHeader struct {
	Type       string
	SubType    string
	Parameters map[string]string
}

// ContentInfo contains the parsed content information from the MIME headers
type ContentInfo struct {
	Type        ContentTypeHeader
	Encoding    Mechanism
	Description string
	ID          string
}

// Content is an Entity with info and bytes
type Content struct {
	Info       ContentInfo
	Bytes      []byte    // if single entity
	SubContent []Content // if multipart
}

// ParseHeaders parses ContentInfo from header lines
func ParseHeaders(lines []string) (bool, int, ContentInfo, error) {
	// loop the headers, if starts w/ Content- or MIME?
	foundVer := false
	info := ContentInfo{}
	i := 0
	for i < len(lines) {
		line := lines[i]

		// Empty is not expected as that signifies end of headers
		if line == "" {
			break
		}

		// Collapse any folding whitespace
		j := i + 1
		for j < len(lines) {
			if len(lines[j]) == 0 {
				break
			}

			c := lines[j][0]
			if c == ' ' || c == '\t' {
				// Combine
				line += lines[j]
				i++
			} else {
				break
			}
		}

		// Parse headers
		header, isMIME, err := parseHeader(line)
		if err != nil {
			return false, 0, info, err
		}
		if !isMIME {
			continue
		}

		// Got one
		switch header.key {
		case headerPrefixMIMEVersion:
			foundVer = true
		case headerPrefixContentType:
			// TODO:
			// RFC 2049:
			// Upon encountering any unrecognized Content-Type field,
			// an implementation must treat it as if it had a media
			// type of "application/octet-stream" with no parameter
			// sub-arguments. How such data are handled is up to an
			// implementation
			parts := strings.SplitN(strings.ToLower(header.value), "/", 2)
			if len(parts) != 2 {
				return true, 0, info, errors.New("mime - bad content-type value")
			}
			info.Type = ContentTypeHeader{
				Type:       parts[0],
				SubType:    parts[1],
				Parameters: header.parameters,
			}
		case headerPrefixContentTransferEncoding:
			// TODO: validate here? casing?
			info.Encoding = Mechanism(header.value)
		case headerPrefixContentDescription:
			info.Description = header.value
		case headerPrefixContentID:
			info.ID = header.value
		default:
			// unknown content header, technically allowed in future extensions - > ignore for now
		}

		i++
	}

	// TODO: distinguish missing version from no MIME headers at all (which is ok!)
	if !foundVer {
		return false, 0, info, errors.New("mime - no version specified")
	}

	return true, i + 1, info, nil
}

func parseHeader(line string) (header, bool, error) {
	h := header{}
	c := strings.IndexByte(line, ':')
	if c < 0 {
		// Not a valid header
		return h, true, errors.New("mime - bad header syntax")
	}

	// TODO:! can't do this! screwing up boundary, for example, in multipart.
	//line = strings.ToLower(line)
	h.key = strings.ToLower(line[:c])
	val := line[c+1:]
	// TODO: may need to check whitespace
	if h.key == "mime-version" {
		// TODO: remove comments, check formatting
		v := strings.TrimSpace(val)
		var err error
		if v == "1.0" {
			h.value = v
		} else {
			err = fmt.Errorf("mime - unexpected version %s", v)
		}
		return h, true, err
	}

	if c > 8 && h.key[:8] == "content-" {
		// TODO: remove comments, check formatting, deal w/ quoted, etc.
		// e.g. Content-type: text/plain; charset=us-ascii (Plain text)
		parts := strings.Split(val, ";")
		if len(parts) == 0 {
			return h, true, errors.New("mime - Content-Type missing argument")
		}
		h.value = strings.TrimSpace(parts[0])
		if len(parts) == 1 {
			// just type and subtype
			return h, true, nil
		}

		h.parameters = make(map[string]string)
		for _, p := range parts[1:] {
			// TODO: check for any crazy formatting allowed here, spaces near =, etc.
			kv := strings.SplitN(p, "=", 2)
			if len(kv) != 2 {
				// TODO: error or continue?
				continue
			}
			// attempt to remove any quotes
			k := strings.TrimSpace(kv[0])
			v := strings.TrimSpace(strings.ReplaceAll(kv[1], "\"", ""))
			h.parameters[k] = v
		}
		return h, true, nil
	}

	// Not a MIME header
	return h, false, nil
}

func stripComment(val string) string {
	// TODO:
	// comments: see RFC 5322
	// Have their own syntax, oh boy...
	// can have quotes, fws, nesting...
	// strip anything between parens
	// it's an error if there are other parens
	// b := strings.Builder{}
	// inP := false
	// for i, r := range value {
	// 	if
	// }
	return ""
}
