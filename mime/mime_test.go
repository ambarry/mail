package mime

import (
	"encoding/base64"
	"io/ioutil"
	"path/filepath"
	"strings"
	"testing"
	"text/template"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ambarry/mail/msg"
)

const testMimeTypesPath = "../mime.types"

// TODO: we should make two interfaces: store reader and writer
type testRepo struct {
	file []byte
}

func (tr *testRepo) Init() error                                { return nil }
func (tr *testRepo) Store(email, id string, bytes []byte) error { return nil }
func (tr *testRepo) Get(email, id string) ([]byte, error)       { return tr.file, nil }

func TestParseHeaders(t *testing.T) {
	RegisterTypes(testMimeTypesPath)
	m := crlfify(`MIME-Version: 1.0
Content-Type: text/plain; charset=US-ASCII
Content-Transfer-Encoding: 7bit
Content-ID: 123abc
Content-Description: Just some fun stuff!
From: test@whom.email
To: test@whom.email
Subject: Whatever

Hello, this is a message body!
Cheers!`)
	lines := strings.Split(m, msg.CRLF)
	isMime, n, info, err := ParseHeaders(lines)
	assert.Nil(t, err)
	assert.True(t, isMime)
	assert.Equal(t, 9, n)
	assert.Equalf(t, "123abc", info.ID, "bad id parse")
	assert.Equalf(t, "7bit", info.Encoding, "bad encoding parse")
	assert.Equalf(t, "text", info.Type.Type, "bad type parse")
	assert.Equalf(t, "plain", info.Type.SubType, "bad subtype parse")
	cs, ok := info.Type.Parameters["charset"]
	assert.True(t, ok)
	assert.Equalf(t, "us-ascii", cs, "bad charset parse")
	assert.Equalf(t, "just some fun stuff!", info.Description, "bad description parse")
}

func TestParseMessage(t *testing.T) {
	RegisterTypes(testMimeTypesPath)
	//	m := loadMultipartMessage(t)

	// TODO: simple message
}

func TestParseMultipartMessage(t *testing.T) {

	// From: Nathaniel Borenstein <nsb@bellcore.com>
	// To: Ned Freed <ned@innosoft.com>
	// Date: Sun, 21 Mar 1993 23:56:48 -0800 (PST)
	// Subject: Sample message
	// MIME-Version: 1.0
	// Content-type: multipart/mixed; boundary="simple boundary"

	// This is the preamble.  It is to be ignored, though it
	// is a handy place for composition agents to include an
	// explanatory note to non-MIME conformant readers.

	// --simple boundary

	// This is implicitly typed plain US-ASCII text.
	// It does NOT end with a linebreak.
	// --simple boundary
	// Content-type: text/plain; charset=us-ascii

	// This is explicitly typed plain US-ASCII text.
	// It DOES end with a linebreak.

	// --simple boundary--

	// This is the epilogue.  It is also to be ignored.

	// TODO:
}

func TestComplexMessage(t *testing.T) {
	loadMultipartMessage(t)
	// TODO:!
}

func TestBoundaryWithQuotes(t *testing.T) {
	// TODO: multipart with boundary in quotes, make sure it works... have a ':' in there too
}

func TestComposeTextOnlyMessage(t *testing.T) {
	RegisterTypes(testMimeTypesPath)

	exp := crlfify(`MIME-Version: 1.0
Content-Type: text/plain; charset=US-ASCII
Content-Transfer-Encoding: 7bit
	
Hello, this is a message body!
Cheers!`)

	txt := "Hello, this is a message body!\r\nCheers!"
	mm, err := composeTextOnlyMessage("test@whom.email", txt)
	assert.Nil(t, err)
	assert.Equalf(t, exp, mm, "expected match, wanted %s, got %s", exp, mm)
}

func TestComposeMIMEMessage(t *testing.T) {
	RegisterTypes(testMimeTypesPath)
	ad := Attachment{"id-1", "example.jpg"}
	bytes := loadSampleFile(t, "jpg")
	repo := testRepo{bytes}
	mm, err := composeMIMEMessage(&repo, "test@whom.email", ad)
	assert.Nil(t, err)
	expBytes, err := ioutil.ReadFile("testdata-b64-img.txt")
	assert.Nil(t, err)
	exp := crlfify(string(expBytes))
	assert.Equal(t, exp, mm, "expected multipart message match")
}

func TestComposeMultipartMessage(t *testing.T) {
	RegisterTypes(testMimeTypesPath)
}

func TestQuotedPrintable76Columns(t *testing.T) {

	bytes := []byte("Here is some text that exceeds 76 chars and has a few crazy chars?")
	enc := NewEncoding("sample.jpg", WithMechanism(EncodingMechanismQuotedPrintable))
	err := enc.Encode(bytes)
	assert.Nil(t, err)
	// TODO: it seems like this does line endings but not 76 chars...
	t.Log(enc.Body)
	//ioutil.WriteFile("qp", []byte(enc.Body), 777)
	t.FailNow()
}

func loadSampleFile(t *testing.T, ext string) []byte {
	ext = strings.Trim(ext, ".")
	name := "example." + ext
	path := filepath.Join("../examples", name)
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("Unable to load sample with with ext: %s, error: %v", ext, err)
	}
	return bytes
}

// TODO: parse and decode this! Need to add the audio but you get it...

// Using the example message, but with supplied bas64 encodings of the files.
// We can then ensure that we both create and parse the example as expected.
// From RFC 2049:
// Appendix A -- A Complex Multipart Example
// "What follows is the outline of a complex multipart message.  This
//  message contains five parts that are to be displayed serially:  two
//  introductory plain text objects, an embedded multipart message, a
//  text/enriched object, and a closing encapsulated text message in a
//  non-ASCII character set.  The embedded multipart message itself
//  contains two objects to be displayed in parallel, a picture and an
//  audio fragment."

func loadMultipartMessage(t *testing.T) string {
	type multi struct {
		AudioData string
		ImageData string
	}

	au := loadSampleFile(t, "au")
	jpg := loadSampleFile(t, "jpg")
	auB64 := base64.StdEncoding.EncodeToString(au)
	jpegB64 := base64.StdEncoding.EncodeToString(jpg)

	// Not really a template, just using for string replace
	example, err := ioutil.ReadFile("testdata-multipart.txt")
	assert.Nil(t, err)
	tmpl, err := template.New("multipart").Parse(string(example))
	if err != nil {
		t.Fatalf("Error parseing multipart template: %v", err)
	}

	m := multi{
		AudioData: auB64,
		ImageData: jpegB64,
	}

	b := strings.Builder{}
	err = tmpl.Execute(&b, m)
	if err != nil {
		t.Fatalf("Error executing multipart template: %v", err)
	}

	msg := b.String()
	t.Log(msg)
	return msg
}

func crlfify(s string) string {
	return strings.ReplaceAll(s, "\n", msg.CRLF)
}
