package mime

import (
	"errors"
	"fmt"

	"gitlab.com/ambarry/mail/msg"
)

var (
	ErrCompositeEncoding = errors.New("composite messages only support 7bit or 8bit encodings")
	ErrMaxDepth          = errors.New("multipart message exceeded max depth")
	ErrNoParts           = errors.New("no part encountered") // believe 1 is required
)

// Reader reads a MIME message.
// TODO: make more go
// Can we switch to io.reader / scanner? sep interfaces?
// Maybe this implements io.Reader?
type Reader struct {
}

// TODO: how do we want this to work with handler?
// either message comes in, we detect mime, parse here, save off files or blobs or whatever,
// and report errors...

// OR, we have the reading go through here if it's MIME, handle it incoming, and report errors live
// so we can abandon... That sounds better, if more complicated...
// Eh, let's just read and report errors, why not...

// ParseMessage reads a MIME message.
// It returns an error if the format is incorrect.
// It returns blobs for attachments? Or a full mail message?
// We should store outside of Cassandra in some (eventually distributed)
// Then we can have refs to it and metadata in Cassandra w/ UUIDs, size, etc.
// We might need to denormalize so we can query the metadata effectively with the message
// TODO: what are we returning here? do we store attached files now? or only on success?
// probably on success...
func (r *Reader) ParseMessage(email string, m msg.Message) (bool, Content, error) {
	var c Content
	ok, n, ci, err := ParseHeaders(m.Content)
	if err != nil {
		// bad MIME
		return false, c, err
	}

	if !ok {
		// not MIME
		return false, c, nil
	}

	// This is our entry point in the recursion.
	// We can use the length of bounds to establish depth,
	// multipart vs regular, then go off encoding for each, create blob objs in format we want, etc.
	// Boundary is requried, and we expect any quotes to be trimmed at this point
	if ci.Type.Type == CompositeTopLevelMultipart {
		if !isValidCompositeEncoding(ci) {
			return true, c, ErrCompositeEncoding
		}

		// TODO: make sure w didn't lowercase the boundary value!
		bounds := make([]string, 0)
		_, closed, c, err := readMultipart(m.Content[n:], ci, bounds)

		// We expect the top level to have closed, otherwise, improper message. Should we handle?
		if !closed {
			return true, c, fmt.Errorf("MIME - unclosed multipart message")
		}

		return true, c, err
	}

	// TODO: regular message (check type) - want a func to read by type and enc, should
	// also be callable (and recursive) from within multipart
	bytes, err := processEntity(m.Content[n:], ci)
	if err != nil {
		return true, c, err
	}

	c = Content{Info: ci, Bytes: bytes}
	// TODO: how do we want to deal w/ content?

	return true, c, nil
}

// isValidCompositeEncoding checks content encoding.
// RFC 2045: a content encoding other than 7bit, 8bit, or binary is forbidden on composites.
// This is to prevent any double encoding. Therefore, anything w/ composite can't be extra encoded,
// just 7 or 8 bit in practice.
// Outer 7 inner 8 is wonky and indicates an error, but may be ok to allow.
func isValidCompositeEncoding(ci ContentInfo) bool {
	return ci.Encoding == EncodingMechanism7Bit || ci.Encoding == EncodingMechanism8Bit
}

// readMulipart reads a multipart message. It is possibly recursive with other multiparts within.
// multipart/alternative is same a mixed but all messages are sort of the same, they increase
// in order of fanciness, last being best... pick and display last format capable of displaying
// multipart/digest is same but default (and intended ct) becomes message/rfc822
// multipart/parallel is same as mixed but it's intended that order isn't important, so a fancy
// display could show each in parallel... what were they thinking??
// message/partial will be its own beast...
// We may find bad messages where inner boundaries are not properly closed before outer boundaries
// are encountered. For now, we're treating as an error.
func readMultipart(lines []string, ci ContentInfo, bounds []string) ([]string, bool, Content, error) {
	c := Content{Info: ci}
	b, err := getBoundary(ci)
	if err != nil {
		return nil, false, c, err
	}

	bounds = append(bounds, b)

	// TODO: do we want this?
	if len(bounds) > maxDepth {
		return nil, false, c, ErrMaxDepth
	}

	// read the preamble
	i, closed, err := readUntilBounds(lines, bounds)
	if err != nil {
		return nil, false, c, err
	}

	if closed {
		return nil, false, c, ErrNoParts
	}

	lines = lines[i:]

	// parts loop
	partFound := false
	for {
		_, i, cii, err := ParseHeaders(lines)
		if err != nil {
			return nil, false, c, err
		}

		// RFC2046: If no Content-Type field is present it is assumed to be "message/rfc822" in a
		// "multipart/digest" and "text/plain" otherwise.
		if cii.Type.Type == "" {
			cii.Type = ContentTypeHeader{DiscreteTopLevelText, "plain", nil}
			if ci.Type.SubType == MultipartSubDigest {
				cii.Type.Type = CompositeTopLevelMessage
				cii.Type.SubType = MessageSubRFC822
			}
		}

		lines = lines[i:]

		// recursion for nested multiparts
		partContent := Content{}
		if cii.Type.Type == CompositeTopLevelMultipart || cii.Type.Type == CompositeTopLevelMessage {
			if !isValidCompositeEncoding(ci) {
				return nil, false, c, ErrCompositeEncoding
			}

			// recursively read the multipart: should include the inner epilogue discard text
			lines, closed, partContent, err = readMultipart(lines, cii, bounds)
		} else {
			lines, closed, partContent, err = readPart(lines, cii, bounds)
		}

		if err != nil {
			return nil, false, c, err
		}

		c.SubContent = append(c.SubContent, partContent)

		// move on to next part
		if !closed {
			continue
		}

		// Read epilogue, as this boundary is finished, and we can exit parts loop.
		// We need to read until message is over, or we encounter the next boundary from parent,
		// which may or may not be a closing boundary.
		// Distinguish, so parent knows to continue processing vs. reading its own epilogue.
		bounds = bounds[:len(bounds)-1] // will be empty at outermost
		if len(bounds) == 0 {
			// we've closed the last part: the entire rest is epilogue, which we can ignore
			// TODO: do we need to keep reading for SMTP's sake? Depends on when we call this
			break
		}

		i, closed, err = readUntilBounds(lines, bounds)
		if err != nil {
			return nil, false, c, err
		}

		lines = lines[i:]
		break
	}

	if !partFound {
		return nil, false, c, ErrNoParts
	}

	return lines, closed, c, nil
}

func readUntilBounds(body []string, bounds []string) (int, bool, error) {
	i := 0
	for i < len(body) {
		// 1. MUST have boundary starting a line (WS or junk after is somewhat ok)
		line := body[i]
		i++

		atBounds, closed, err := checkBounds(line, bounds)
		if err != nil {
			return 0, false, err
		}

		if atBounds {
			return i, closed, nil
		}
	}

	return 0, false, errors.New("boundary not encountered")
}

// readPart reads a single, not-multipart entity within a multipart message.
// It may be a "message" type.
func readPart(body []string, ci ContentInfo, bounds []string) ([]string, bool, Content, error) {
	// TODO: here!!
	// TODO: deal with "message" type
	c := Content{Info: ci}
	n, closed, err := readUntilBounds(body, bounds)
	if err != nil {
		return nil, false, c, err
	}

	// get part's content
	part := body[:n]
	c.Bytes, err = processEntity(part, ci)

	return body[n:], closed, c, err
}

// TODO: do we save here? return a file desc?
// TODO: what has the file name here? ID? Description?
// FOr now, maybe dump to std out, make sure we can test this all? decide on formats, etc?
// Do we even want to save things like text/plain as attachments or what?
// Is the concept of an attachment totally artificial? Can we think of something else?
func processEntity(lines []string, ci ContentInfo) ([]byte, error) {
	// TODO: we probably want to validate ext, supported first, etc.
	enc := NewEncoding(ci.ID, WithMechanism(ci.Encoding))
	bytes, err := enc.Decode(lines) // decode into the raw format in which we wish to save
	return bytes, err
}
