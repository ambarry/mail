package mime

import (
	"errors"
	"mime"
	"strings"

	"gitlab.com/ambarry/mail/msg"
	store "gitlab.com/ambarry/mail/storage"
)

// Writer writes MIME messages.
// TODO:
type Writer struct {
}

// ComposeMessage composes a MIME message.
// The message will begin with the mime headers, such that any SMTP headers an be prepended.
// The msg parameter represents the text for the main message body.
// TODO: return Message vs string?
// TODO: HTML for msg body?
// TODO: attachment ids really need name too for ext check, cdesc or id?
func ComposeMessage(repo store.Repo, email, msg string, ads ...Attachment) (string, error) {
	switch len(ads) {
	case 0:
		if msg == "" {
			return "", errors.New("empty message")
		}
		return composeTextOnlyMessage(email, msg)
	case 1:
		return composeMIMEMessage(repo, email, ads[0])
	default:
		return composeMultipartMessage(repo, email, msg, ads)
	}
}

func composeTextOnlyMessage(email, msg string) (string, error) {
	enc := &Encoding{
		ContentType: "text/plain",
		Parameters:  map[string]string{"charset": "US-ASCII"},
		Body:        msg,
		Mechanism:   EncodingMechanism7Bit,
	}
	return writeEncoding(enc)
}

func composeMIMEMessage(repo store.Repo, email string, ad Attachment) (string, error) {
	// TODO: files w/out extension?
	// TODO: determine here or in next how to encode base on ext.
	enc, err := getEncodedFile(repo, email, ad)
	if err != nil {
		return "", err
	}

	return writeEncoding(enc)
}

func composeMultipartMessage(repo store.Repo, email, msg string, ads []Attachment) (string, error) {

	// TODO: get each part and surround with boundary
	b := strings.Builder{}
	writeMIMEVer(&b)
	// TODO: do we want to use???
	b.WriteString("Content-Type: multipart")
	// w := multipart.NewWriter(&b)
	// w.CreatePart()

	// TODO: finish!
	return "", errors.New("not implemented")
}

func writeMIMEVer(b *strings.Builder) {
	b.WriteString("MIME-Version: 1.0" + msg.CRLF)
}

func writeEncoding(enc *Encoding) (string, error) {
	// NOTE: Builder.WriteString panics if error, returned err is always nil, not worth checking
	b := strings.Builder{}
	writeMIMEVer(&b)
	b.WriteString("Content-Type: ")
	contentType := mime.FormatMediaType(enc.ContentType, enc.Parameters)
	b.WriteString(contentType)
	b.WriteString(msg.CRLF)
	b.WriteString("Content-Transfer-Encoding: ")
	b.WriteString(string(enc.Mechanism))
	b.WriteString(msg.CRLF)
	b.WriteString(msg.CRLF)
	b.WriteString(enc.Body)
	return b.String(), nil
}
