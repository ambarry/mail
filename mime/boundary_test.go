package mime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetBoundary(t *testing.T) {
	// pass in ContentInfo, ensure boundar header is parsed
	ci := ContentInfo{}
	b, err := getBoundary(ci)

	assert := assert.New(t)
	assert.Error(err)

	boundary := "abcd1234"
	ci.Type.Parameters = map[string]string{
		"boundary": boundary,
	}

	b, err = getBoundary(ci)
	assert.Nil(err)
	assert.NotEmpty(b)
	assert.Equal("--"+boundary, b)
}

func TestBoundaryWithQuotes(t *testing.T) {
	// this requires quotes for the :, but we're seeing it post-parse (quotes removed)
	ci := ContentInfo{}
	boundary := "This is a boundary:(it has things)"
	ci.Type.Parameters = map[string]string{
		"boundary": boundary,
	}
	b, err := getBoundary(ci)

	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEmpty(b)
	assert.Equal("--"+boundary, b)
}

func TestInvalidBoundaries(t *testing.T) {
	ci := ContentInfo{}

	// can't end with space
	boundary := "abcd1234 "
	ci.Type.Parameters = map[string]string{
		"boundary": boundary,
	}

	_, err := getBoundary(ci)
	assert := assert.New(t)
	assert.Error(err)

	// invalid chars
	boundary = "abcd***1234"
	ci.Type.Parameters["boundary"] = boundary
	_, err = getBoundary(ci)
	assert.Error(err)

	boundary = "!!!!!1234"
	ci.Type.Parameters["boundary"] = boundary
	_, err = getBoundary(ci)
	assert.Error(err)
}

func TestIsBound(t *testing.T) {
	b := "--helloworld1234"

	assert := assert.New(t)
	match, closed := isBound("hello", b)
	assert.False(match)
	assert.False(closed)

	match, closed = isBound("helloworld1234", b)
	assert.False(match)
	assert.False(closed)

	match, closed = isBound("--helloworld1234", b)
	assert.True(match)
	assert.False(closed)

	match, closed = isBound("--helloworld1234   some extra stuff", b) // TODO: confirm this
	assert.True(match)
	assert.False(closed)

	match, closed = isBound("--helloworld1234--", b)
	assert.True(match)
	assert.True(closed)
}

func TestCheckBounds(t *testing.T) {
	b := "--helloworld1234"
	bounds := []string{
		"--anEarlierBoundary",
		"--reallyNestedNow",
		b,
	}

	m, c, err := checkBounds("hello", bounds)

	assert := assert.New(t)
	assert.False(m)
	assert.False(c)
	assert.Nil(err)

	m, c, err = checkBounds("--helloworld1234", bounds)
	assert.True(m)
	assert.False(c)
	assert.Nil(err)

	m, c, err = checkBounds("--helloworld1234--", bounds)
	assert.True(m)
	assert.True(c)
	assert.Nil(err)

	m, c, err = checkBounds("--anEarlierBoundary--", bounds)
	assert.False(m)
	assert.False(c)
	assert.Error(err)
}
