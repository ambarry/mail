package mime

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strings"
)

func RegisterTypes(path string) {

	// TODO: add them to the golang mime package instead, then we can use that!

	// load mime types
	// TODO: how do we change path for runtime and testing? or just not init...
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	typeByExt = make(map[string]string)
	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if line == "" {
			continue
		}

		parts := strings.Split(line, " ")
		if len(parts) < 1 {
			// < 2 means no registered extensions, is it worth loading though? probably...
			continue
		}

		mimeTypes = append(mimeTypes, parts[0])
		for _, e := range parts[1:] {
			typeByExt[e] = parts[0]
		}
	}

	if scan.Err() != nil {
		log.Fatal(scan.Err())
	}

	// guarantee sorted
	sort.Strings(mimeTypes)
}
