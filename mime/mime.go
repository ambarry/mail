package mime

import (
	"path/filepath"
	"strings"

	store "gitlab.com/ambarry/mail/storage"
)

// TODO: what do we want here? is this in the RFC anywhere? Is there one,
// or is this used during nested replies?
const maxDepth = 10

// 	bcharsnospace := DIGIT / ALPHA / "'" / "(" / ")" /
// 					 "+" / "_" / "," / "-" / "." /
// 					 "/" / ":" / "=" / "?"
var boundaryChars = map[rune]struct{}{
	' ':  {},
	'\'': {},
	'(':  {},
	')':  {},
	'+':  {},
	'_':  {},
	',':  {},
	'-':  {},
	'.':  {},
	'/':  {},
	':':  {},
	'=':  {},
	'?':  {},
}

// TODO: use the golang mime package for writing media type header?

// Attachment contains the name and repo ID of the attachment.
// We use the name both to fetch the MIME type by extension (and thus how to encode),
// and we use the Id to fetch the actual file contents from the Repo.
type Attachment struct {
	ID   string
	Name string
}

// getEncodedFile retrieves a file by id, and returns the MIME content type and encoded file.
func getEncodedFile(repo store.Repo, email string, ad Attachment) (*Encoding, error) {
	blob, err := repo.Get(email, ad.ID)
	if err != nil {
		return nil, err
	}

	// TODO: how should we normally decide how to encode? e.g. what should be 7, 8, or base64?
	// I guess anything binary, base64. Else if text, just 7 or 8?
	enc := NewEncoding(ad.Name, WithMechanism(EncodingMechanismBase64))
	ext := filepath.Ext(ad.Name)
	ext = strings.TrimLeft(ext, ".")
	ct, ok := typeByExt[ext]
	if ok {
		enc.ContentType = ct
	}

	err = enc.Encode(blob)
	return enc, err
}
